﻿namespace WindowsFormsApp1
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea5 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend5 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series7 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.DataPoint dataPoint19 = new System.Windows.Forms.DataVisualization.Charting.DataPoint(0D, 0.75D);
            System.Windows.Forms.DataVisualization.Charting.DataPoint dataPoint20 = new System.Windows.Forms.DataVisualization.Charting.DataPoint(9D, 0.65D);
            System.Windows.Forms.DataVisualization.Charting.DataPoint dataPoint21 = new System.Windows.Forms.DataVisualization.Charting.DataPoint(10.66D, 0D);
            System.Windows.Forms.DataVisualization.Charting.Series series8 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.DataPoint dataPoint22 = new System.Windows.Forms.DataVisualization.Charting.DataPoint(0D, 0D);
            System.Windows.Forms.DataVisualization.Charting.DataPoint dataPoint23 = new System.Windows.Forms.DataVisualization.Charting.DataPoint(9.166D, 8.8D);
            System.Windows.Forms.DataVisualization.Charting.DataPoint dataPoint24 = new System.Windows.Forms.DataVisualization.Charting.DataPoint(10.666D, 0D);
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea6 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend6 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series9 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.DataPoint dataPoint25 = new System.Windows.Forms.DataVisualization.Charting.DataPoint(0.5D, 0.8D);
            System.Windows.Forms.DataVisualization.Charting.DataPoint dataPoint26 = new System.Windows.Forms.DataVisualization.Charting.DataPoint(1.5D, 0.9D);
            System.Windows.Forms.DataVisualization.Charting.DataPoint dataPoint27 = new System.Windows.Forms.DataVisualization.Charting.DataPoint(2.5D, 0.85D);
            this.chartPowerCurve = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.trackBar_Irradiance = new System.Windows.Forms.TrackBar();
            this.label1 = new System.Windows.Forms.Label();
            this.IrradianceValue = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.textBoxIsc = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.textBoxVoc = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.textBoxImpp = new System.Windows.Forms.TextBox();
            this.textBoxVmpp = new System.Windows.Forms.TextBox();
            this.trackBar_Temperature = new System.Windows.Forms.TrackBar();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.textBox_Temperature = new System.Windows.Forms.TextBox();
            this.chartPowerLevels = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.button_RunSimulator = new System.Windows.Forms.Button();
            this.serialPort_GPIB_PowerSupply = new System.IO.Ports.SerialPort(this.components);
            this.backgroundWorker_SolarPanelSimulator = new System.ComponentModel.BackgroundWorker();
            this.label_Amps = new System.Windows.Forms.Label();
            this.label_Volts = new System.Windows.Forms.Label();
            this.label_Watts = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.chartPowerCurve)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.trackBar_Irradiance)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.trackBar_Temperature)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chartPowerLevels)).BeginInit();
            this.SuspendLayout();
            // 
            // chartPowerCurve
            // 
            chartArea5.AxisX.LabelStyle.ForeColor = System.Drawing.Color.Blue;
            chartArea5.AxisX.LabelStyle.Interval = 0D;
            chartArea5.AxisX.LineColor = System.Drawing.Color.LightGray;
            chartArea5.AxisX.MajorGrid.LineColor = System.Drawing.Color.LightGray;
            chartArea5.AxisX.MajorTickMark.LineColor = System.Drawing.Color.LightGray;
            chartArea5.AxisX.Maximum = 12D;
            chartArea5.AxisX.Minimum = 0D;
            chartArea5.AxisX.Title = "Voltage [V]";
            chartArea5.AxisX.TitleFont = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            chartArea5.AxisX.TitleForeColor = System.Drawing.Color.Blue;
            chartArea5.AxisX2.LineColor = System.Drawing.Color.LightGray;
            chartArea5.AxisX2.MajorGrid.LineColor = System.Drawing.Color.LightGray;
            chartArea5.AxisX2.MajorTickMark.LineColor = System.Drawing.Color.LightGray;
            chartArea5.AxisY.LabelStyle.ForeColor = System.Drawing.Color.Blue;
            chartArea5.AxisY.LineColor = System.Drawing.Color.LightGray;
            chartArea5.AxisY.MajorGrid.LineColor = System.Drawing.Color.LightGray;
            chartArea5.AxisY.MajorTickMark.LineColor = System.Drawing.Color.LightGray;
            chartArea5.AxisY.Maximum = 1D;
            chartArea5.AxisY.Minimum = 0D;
            chartArea5.AxisY.Title = "Current [A]";
            chartArea5.AxisY.TitleFont = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            chartArea5.AxisY.TitleForeColor = System.Drawing.Color.Blue;
            chartArea5.AxisY2.Enabled = System.Windows.Forms.DataVisualization.Charting.AxisEnabled.True;
            chartArea5.AxisY2.LabelStyle.ForeColor = System.Drawing.Color.Red;
            chartArea5.AxisY2.LineColor = System.Drawing.Color.LightGray;
            chartArea5.AxisY2.MajorGrid.LineColor = System.Drawing.Color.LightGray;
            chartArea5.AxisY2.MajorTickMark.LineColor = System.Drawing.Color.LightGray;
            chartArea5.AxisY2.Maximum = 10D;
            chartArea5.AxisY2.Minimum = 0D;
            chartArea5.AxisY2.TextOrientation = System.Windows.Forms.DataVisualization.Charting.TextOrientation.Rotated270;
            chartArea5.AxisY2.Title = "Power [W]";
            chartArea5.AxisY2.TitleFont = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            chartArea5.AxisY2.TitleForeColor = System.Drawing.Color.Red;
            chartArea5.Name = "ChartArea1";
            this.chartPowerCurve.ChartAreas.Add(chartArea5);
            legend5.Enabled = false;
            legend5.Name = "Legend1";
            this.chartPowerCurve.Legends.Add(legend5);
            this.chartPowerCurve.Location = new System.Drawing.Point(183, 30);
            this.chartPowerCurve.Name = "chartPowerCurve";
            series7.ChartArea = "ChartArea1";
            series7.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Spline;
            series7.Color = System.Drawing.Color.Blue;
            series7.IsVisibleInLegend = false;
            series7.Legend = "Legend1";
            series7.Name = "CurrentVoltageChart";
            series7.Points.Add(dataPoint19);
            series7.Points.Add(dataPoint20);
            series7.Points.Add(dataPoint21);
            series7.XValueType = System.Windows.Forms.DataVisualization.Charting.ChartValueType.Double;
            series7.YValueType = System.Windows.Forms.DataVisualization.Charting.ChartValueType.Double;
            series8.ChartArea = "ChartArea1";
            series8.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Spline;
            series8.Color = System.Drawing.Color.Red;
            series8.Legend = "Legend1";
            series8.Name = "PowerChart";
            series8.Points.Add(dataPoint22);
            series8.Points.Add(dataPoint23);
            series8.Points.Add(dataPoint24);
            series8.YAxisType = System.Windows.Forms.DataVisualization.Charting.AxisType.Secondary;
            this.chartPowerCurve.Series.Add(series7);
            this.chartPowerCurve.Series.Add(series8);
            this.chartPowerCurve.Size = new System.Drawing.Size(444, 269);
            this.chartPowerCurve.TabIndex = 0;
            this.chartPowerCurve.Text = "chart1";
            // 
            // trackBar_Irradiance
            // 
            this.trackBar_Irradiance.AutoSize = false;
            this.trackBar_Irradiance.Location = new System.Drawing.Point(25, 375);
            this.trackBar_Irradiance.Maximum = 1000;
            this.trackBar_Irradiance.Name = "trackBar_Irradiance";
            this.trackBar_Irradiance.Size = new System.Drawing.Size(285, 32);
            this.trackBar_Irradiance.TabIndex = 1;
            this.trackBar_Irradiance.TickFrequency = 0;
            this.trackBar_Irradiance.TickStyle = System.Windows.Forms.TickStyle.Both;
            this.trackBar_Irradiance.Value = 1000;
            this.trackBar_Irradiance.Scroll += new System.EventHandler(this.Irradiance_Scroll);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(31, 338);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(143, 31);
            this.label1.TabIndex = 3;
            this.label1.Text = "Irradiance:";
            // 
            // IrradianceValue
            // 
            this.IrradianceValue.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.IrradianceValue.Location = new System.Drawing.Point(171, 338);
            this.IrradianceValue.Name = "IrradianceValue";
            this.IrradianceValue.Size = new System.Drawing.Size(66, 31);
            this.IrradianceValue.TabIndex = 4;
            this.IrradianceValue.Text = "1000";
            this.IrradianceValue.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.IrradianceValue.TextChanged += new System.EventHandler(this.IrradianceValue_TextChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(243, 341);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(67, 25);
            this.label2.TabIndex = 5;
            this.label2.Text = "W/m2";
            // 
            // textBoxIsc
            // 
            this.textBoxIsc.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxIsc.Location = new System.Drawing.Point(75, 74);
            this.textBoxIsc.Name = "textBoxIsc";
            this.textBoxIsc.Size = new System.Drawing.Size(68, 26);
            this.textBoxIsc.TabIndex = 6;
            this.textBoxIsc.Text = "0.878";
            this.textBoxIsc.TextChanged += new System.EventHandler(this.PV_characteristics_changed);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(40, 74);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(33, 20);
            this.label3.TabIndex = 7;
            this.label3.Text = "Isc";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(33, 105);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(40, 20);
            this.label4.TabIndex = 8;
            this.label4.Text = "Voc";
            // 
            // textBoxVoc
            // 
            this.textBoxVoc.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxVoc.Location = new System.Drawing.Point(75, 105);
            this.textBoxVoc.Name = "textBoxVoc";
            this.textBoxVoc.Size = new System.Drawing.Size(68, 26);
            this.textBoxVoc.TabIndex = 9;
            this.textBoxVoc.Text = "10.666";
            this.textBoxVoc.TextChanged += new System.EventHandler(this.PV_characteristics_changed);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(25, 136);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(49, 20);
            this.label5.TabIndex = 10;
            this.label5.Text = "Impp";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(19, 167);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(55, 20);
            this.label6.TabIndex = 11;
            this.label6.Text = "Vmpp";
            // 
            // textBoxImpp
            // 
            this.textBoxImpp.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxImpp.Location = new System.Drawing.Point(75, 136);
            this.textBoxImpp.Name = "textBoxImpp";
            this.textBoxImpp.Size = new System.Drawing.Size(68, 26);
            this.textBoxImpp.TabIndex = 12;
            this.textBoxImpp.Text = "0.815";
            this.textBoxImpp.TextChanged += new System.EventHandler(this.PV_characteristics_changed);
            // 
            // textBoxVmpp
            // 
            this.textBoxVmpp.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxVmpp.Location = new System.Drawing.Point(76, 167);
            this.textBoxVmpp.Name = "textBoxVmpp";
            this.textBoxVmpp.Size = new System.Drawing.Size(67, 26);
            this.textBoxVmpp.TabIndex = 13;
            this.textBoxVmpp.Text = "9.116";
            this.textBoxVmpp.TextChanged += new System.EventHandler(this.PV_characteristics_changed);
            // 
            // trackBar_Temperature
            // 
            this.trackBar_Temperature.AutoSize = false;
            this.trackBar_Temperature.Location = new System.Drawing.Point(334, 375);
            this.trackBar_Temperature.Maximum = 50;
            this.trackBar_Temperature.Name = "trackBar_Temperature";
            this.trackBar_Temperature.Size = new System.Drawing.Size(306, 32);
            this.trackBar_Temperature.TabIndex = 14;
            this.trackBar_Temperature.TickFrequency = 0;
            this.trackBar_Temperature.TickStyle = System.Windows.Forms.TickStyle.Both;
            this.trackBar_Temperature.Value = 25;
            this.trackBar_Temperature.Scroll += new System.EventHandler(this.trackBar_Temperature_Scroll);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(334, 338);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(177, 31);
            this.label7.TabIndex = 15;
            this.label7.Text = "Temperature:";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(565, 341);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(27, 25);
            this.label8.TabIndex = 16;
            this.label8.Text = "C";
            // 
            // textBox_Temperature
            // 
            this.textBox_Temperature.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox_Temperature.Location = new System.Drawing.Point(507, 338);
            this.textBox_Temperature.Name = "textBox_Temperature";
            this.textBox_Temperature.Size = new System.Drawing.Size(52, 31);
            this.textBox_Temperature.TabIndex = 17;
            this.textBox_Temperature.Text = "25";
            this.textBox_Temperature.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.textBox_Temperature.TextChanged += new System.EventHandler(this.textBox_Temperature_TextChanged);
            // 
            // chartPowerLevels
            // 
            this.chartPowerLevels.BackColor = System.Drawing.Color.Transparent;
            chartArea6.AxisX.Enabled = System.Windows.Forms.DataVisualization.Charting.AxisEnabled.False;
            chartArea6.AxisX.MajorGrid.Enabled = false;
            chartArea6.AxisX.MajorTickMark.Enabled = false;
            chartArea6.AxisX.Maximum = 3D;
            chartArea6.AxisX.Minimum = 0D;
            chartArea6.AxisX2.MajorGrid.Enabled = false;
            chartArea6.AxisY.Enabled = System.Windows.Forms.DataVisualization.Charting.AxisEnabled.False;
            chartArea6.AxisY.MajorGrid.Enabled = false;
            chartArea6.AxisY.MajorTickMark.Enabled = false;
            chartArea6.AxisY.Maximum = 1D;
            chartArea6.AxisY.Minimum = 0D;
            chartArea6.AxisY2.MajorGrid.Enabled = false;
            chartArea6.BackColor = System.Drawing.Color.Transparent;
            chartArea6.Name = "ChartArea1";
            this.chartPowerLevels.ChartAreas.Add(chartArea6);
            legend6.Enabled = false;
            legend6.Name = "Legend1";
            this.chartPowerLevels.Legends.Add(legend6);
            this.chartPowerLevels.Location = new System.Drawing.Point(633, 30);
            this.chartPowerLevels.Name = "chartPowerLevels";
            series9.ChartArea = "ChartArea1";
            series9.Legend = "Legend1";
            series9.Name = "MPPT_chart";
            dataPoint25.AxisLabel = "";
            dataPoint25.IsValueShownAsLabel = false;
            dataPoint25.Label = "Amps";
            dataPoint26.Label = "Volts";
            dataPoint27.Label = "Watts";
            series9.Points.Add(dataPoint25);
            series9.Points.Add(dataPoint26);
            series9.Points.Add(dataPoint27);
            this.chartPowerLevels.Series.Add(series9);
            this.chartPowerLevels.Size = new System.Drawing.Size(290, 269);
            this.chartPowerLevels.TabIndex = 18;
            this.chartPowerLevels.Text = "Power Levels";
            // 
            // button_RunSimulator
            // 
            this.button_RunSimulator.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button_RunSimulator.Location = new System.Drawing.Point(732, 351);
            this.button_RunSimulator.Name = "button_RunSimulator";
            this.button_RunSimulator.Size = new System.Drawing.Size(91, 33);
            this.button_RunSimulator.TabIndex = 19;
            this.button_RunSimulator.Text = "Start";
            this.button_RunSimulator.UseVisualStyleBackColor = true;
            this.button_RunSimulator.Click += new System.EventHandler(this.button_RunSimulator_Click);
            // 
            // serialPort_GPIB_PowerSupply
            // 
            this.serialPort_GPIB_PowerSupply.BaudRate = 115200;
            this.serialPort_GPIB_PowerSupply.Handshake = System.IO.Ports.Handshake.RequestToSend;
            this.serialPort_GPIB_PowerSupply.ParityReplace = ((byte)(0));
            this.serialPort_GPIB_PowerSupply.PortName = "COM9";
            // 
            // backgroundWorker_SolarPanelSimulator
            // 
            this.backgroundWorker_SolarPanelSimulator.WorkerSupportsCancellation = true;
            this.backgroundWorker_SolarPanelSimulator.DoWork += new System.ComponentModel.DoWorkEventHandler(this.backgroundWorker_SolarPanelSimulator_DoWork);
            // 
            // label_Amps
            // 
            this.label_Amps.AutoSize = true;
            this.label_Amps.Location = new System.Drawing.Point(656, 302);
            this.label_Amps.Name = "label_Amps";
            this.label_Amps.Size = new System.Drawing.Size(14, 13);
            this.label_Amps.TabIndex = 20;
            this.label_Amps.Text = "A";
            this.label_Amps.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label_Volts
            // 
            this.label_Volts.AutoSize = true;
            this.label_Volts.Location = new System.Drawing.Point(748, 302);
            this.label_Volts.Name = "label_Volts";
            this.label_Volts.Size = new System.Drawing.Size(14, 13);
            this.label_Volts.TabIndex = 21;
            this.label_Volts.Text = "V";
            this.label_Volts.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label_Watts
            // 
            this.label_Watts.AutoSize = true;
            this.label_Watts.Location = new System.Drawing.Point(840, 302);
            this.label_Watts.Name = "label_Watts";
            this.label_Watts.Size = new System.Drawing.Size(18, 13);
            this.label_Watts.TabIndex = 22;
            this.label_Watts.Text = "W";
            this.label_Watts.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.ClientSize = new System.Drawing.Size(953, 450);
            this.Controls.Add(this.label_Watts);
            this.Controls.Add(this.label_Volts);
            this.Controls.Add(this.label_Amps);
            this.Controls.Add(this.button_RunSimulator);
            this.Controls.Add(this.chartPowerLevels);
            this.Controls.Add(this.textBox_Temperature);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.trackBar_Temperature);
            this.Controls.Add(this.textBoxVmpp);
            this.Controls.Add(this.textBoxImpp);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.textBoxVoc);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.textBoxIsc);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.IrradianceValue);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.trackBar_Irradiance);
            this.Controls.Add(this.chartPowerCurve);
            this.Name = "Form1";
            this.Text = "Solar Panel Simulator";
            this.Load += new System.EventHandler(this.Form1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.chartPowerCurve)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.trackBar_Irradiance)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.trackBar_Temperature)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chartPowerLevels)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataVisualization.Charting.Chart chartPowerCurve;
        private System.Windows.Forms.TrackBar trackBar_Irradiance;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox IrradianceValue;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox textBoxIsc;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox textBoxVoc;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox textBoxImpp;
        private System.Windows.Forms.TextBox textBoxVmpp;
        private System.Windows.Forms.TrackBar trackBar_Temperature;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox textBox_Temperature;
        private System.Windows.Forms.DataVisualization.Charting.Chart chartPowerLevels;
        private System.Windows.Forms.Button button_RunSimulator;
        private System.IO.Ports.SerialPort serialPort_GPIB_PowerSupply;
        private System.ComponentModel.BackgroundWorker backgroundWorker_SolarPanelSimulator;
        private System.Windows.Forms.Label label_Amps;
        private System.Windows.Forms.Label label_Volts;
        private System.Windows.Forms.Label label_Watts;
    }
}

