﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Diagnostics;

namespace WindowsFormsApp1
{


    public partial class Form1 : Form
    {
        double Voc;
        double Isc;
        double Impp;
        double Vmpp;
        double IscScaled;
        double TempK;
        double vt;
        double i0;
        double Irradiance;
        double old_IscScaled = 0.0;

        public Form1()
        {
            InitializeComponent();
            UpdateChart();
        }

        private void scpi_write(string str)
        {
            Debug.WriteLine("<<== " + str);
            serialPort_GPIB_PowerSupply.WriteLine(str);
        }

        private string scpi_read()
        {
            string str;

            str = serialPort_GPIB_PowerSupply.ReadLine();
            Debug.WriteLine("==>> " + str);
            return str;
        }

        private void Form1_Load(object sender, EventArgs e)
        {
        }

        private void Irradiance_Scroll(object sender, EventArgs e)
        {
            IrradianceValue.Text = trackBar_Irradiance.Value.ToString();
        }

        private void IrradianceValue_TextChanged(object sender, EventArgs e)
        {
            int i;

            try
            {
                i = Convert.ToInt32(IrradianceValue.Text);
            }
            catch (System.Exception ex)
            {
                i = 0;
            }

            if (i < 0)
                i = 0;
            if (i > trackBar_Irradiance.Maximum)
                i = trackBar_Irradiance.Maximum;
            IrradianceValue.Text = Convert.ToString(i);

            trackBar_Irradiance.Value = i;

            UpdateChart();
        }

        private void trackBar_Temperature_Scroll(object sender, EventArgs e)
        {
            textBox_Temperature.Text = trackBar_Temperature.Value.ToString();
        }

        private void textBox_Temperature_TextChanged(object sender, EventArgs e)
        {
            int i;

            try
            {
                i = Convert.ToInt32(textBox_Temperature.Text);
            }
            catch (System.Exception ex)
            {
                i = 0;
            }

            if (i < 0)
                i = 0;
            if (i > trackBar_Temperature.Maximum)
                i = trackBar_Temperature.Maximum;
            textBox_Temperature.Text = Convert.ToString(i);

            trackBar_Temperature.Value = i;

            UpdateChart();

        }

        private void UpdateChart()
        {
            Voc = Convert.ToDouble(textBoxVoc.Text);
            Isc = Convert.ToDouble(textBoxIsc.Text);
            Impp = Convert.ToDouble(textBoxImpp.Text);
            Vmpp = Convert.ToDouble(textBoxVmpp.Text);
            Irradiance = Convert.ToDouble(trackBar_Irradiance.Value);
            IscScaled = Isc * Irradiance / 1000;
            TempK = trackBar_Temperature.Value * 298.15 / 25.0;
            vt = get_vt(Vmpp, Voc, Isc, Impp, TempK);
            i0 = get_i0(Isc, Impp, Vmpp, vt);


            // Update the chart axis
            chartPowerCurve.ChartAreas[0].AxisX.Maximum = Convert.ToInt32(Voc * 1.1);
            chartPowerCurve.ChartAreas[0].AxisY.Maximum = Convert.ToInt32(Isc + .6);
            chartPowerCurve.ChartAreas[0].AxisY2.Maximum = Convert.ToInt32(Impp * Vmpp + .5);

            chartPowerCurve.Series[0].Points.Clear();
            chartPowerCurve.Series[1].Points.Clear();

            int i;
            int DataPoints = 1000;

            for(i = 0; i <= DataPoints; i++)
            {
                double I_Yaxis = i * IscScaled / DataPoints;
                double V_Xaxis = get_voltage(vt, IscScaled, I_Yaxis, i0);
                chartPowerCurve.Series[0].Points.AddXY(V_Xaxis, I_Yaxis);
                chartPowerCurve.Series[1].Points.AddXY(V_Xaxis, I_Yaxis * V_Xaxis);
            }
        }


        private void PV_characteristics_changed(object sender, EventArgs e)
        {
            UpdateChart();
        }

        private double get_vt(double Vmpp, double Voc, double Isc, double Impp, double temperature_kelvin)
        {
            return ((2 * Vmpp - Voc) / (Math.Log((Isc - Impp) / Isc) + Impp / (Isc - Impp))) * temperature_kelvin / 298.15;
        }//get vt

        private double get_i0(double Isc, double Impp, double Vmpp, double vt)
        {
            return (Isc - Impp) / (Math.Exp(Vmpp / vt) - 1);
        }//get i0 

        private double get_voltage(double vt, double Isc, double current, double i0)
        {
            return vt * Math.Log(1 + (Isc - current) / i0);
        }//get_voltage value

        private double get_current(double vt, double Isc, double i0, double voltage)
        {
            return Isc - i0 * (Math.Exp(voltage / vt) - 1);
        }

        private void button_RunSimulator_Click(object sender, EventArgs e)
        {
            if (backgroundWorker_SolarPanelSimulator.IsBusy == true)
                backgroundWorker_SolarPanelSimulator.CancelAsync();
            else
                backgroundWorker_SolarPanelSimulator.RunWorkerAsync();
        }

        private void backgroundWorker_SolarPanelSimulator_DoWork(object sender, DoWorkEventArgs e)
        {
            BackgroundWorker worker = sender as BackgroundWorker;

            this.Invoke(new MethodInvoker(delegate
            {
                button_RunSimulator.Text = "Stop";
            }));

            try
            {
                serialPort_GPIB_PowerSupply.Open();
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message);
            }

            if (serialPort_GPIB_PowerSupply.IsOpen == true)
            {
                string sp_line_buffer;
                double measured_current, measured_voltage, calculated_voltage;

                // Get the model name from the power supply
                scpi_write("*RST ; *IDN?"); // reset the power supply
                sp_line_buffer = scpi_read();
                Debug.WriteLine("\t\t\t\t\t\t\t\t\t\t\t\tID string: " + sp_line_buffer);

                if (sp_line_buffer != "HEWLETT-PACKARD,E3631A,0,2.1-5.0-1.0")
                {
                    System.Diagnostics.Debug.WriteLine("\t\t\t\t\t\t\t\t\t\t\t\tInvalid device. Halting...");
                    while (true) ;
                }

                // set the power supply to 25v mode
                scpi_write(":inst:sel p25v ; :inst:sel?");
                sp_line_buffer = scpi_read();
                Debug.WriteLine("\t\t\t\t\t\t\t\t\t\t\t\tInstrument selected: " + sp_line_buffer);

                // send the voltage to the Voltage open circuit value
                scpi_write(":volt " + Voc + "; :volt?");
                sp_line_buffer = scpi_read();
                measured_voltage = Convert.ToDouble(sp_line_buffer);
                Debug.WriteLine("\t\t\t\t\t\t\t\t\t\t\t\tVoltage set to: " + sp_line_buffer + " volts");

                // send the current limit to the Current open circuit value
                // send the current limit to the Current open circuit value
                scpi_write(":curr " + IscScaled + "; :curr?");
                sp_line_buffer = scpi_read();
                measured_current = Convert.ToDouble(sp_line_buffer);
                Debug.WriteLine("\t\t\t\t\t\t\t\t\t\t\t\tCurrent set to: " + sp_line_buffer + " amps");

                serialPort_GPIB_PowerSupply.Write("outp on ; outp?\n"); //send the current limit to 0.0 amps
                sp_line_buffer = scpi_read();
                Debug.WriteLine("\t\t\t\t\t\t\t\t\t\t\t\tOutput: " + sp_line_buffer);

                // Measure the current the first time
                scpi_write(":meas:curr?");
                sp_line_buffer = scpi_read();
                measured_current = Convert.ToDouble(sp_line_buffer);
                Debug.WriteLine("\t\t\t\t\t\t\t\t\t\t\t\tCurrent set to: " + measured_current + " amps");


                while (true)
                {

                    if (worker.CancellationPending == true)
                        break;

                    if (old_IscScaled != IscScaled) ;
                    {
                        // send the current limit to the Current open circuit value
                        scpi_write(":curr " + IscScaled + "; :meas:volt?");
                        sp_line_buffer = scpi_read();
                        measured_voltage = Convert.ToDouble(sp_line_buffer);

                        old_IscScaled = IscScaled;
                    }

                    // Calculate the voltage based on the current
                    //                    calculated_voltage = get_voltage(vt, IscScaled, measured_current, i0);
                    if (measured_current < (Isc * .9))
                        calculated_voltage = get_voltage(vt, Isc, measured_current, i0);
                    else
                        calculated_voltage = get_voltage(vt, Isc, Isc * .9, i0);

                    // Measure the current
                    scpi_write(":volt " + calculated_voltage + "; :meas:curr?");
                    sp_line_buffer = scpi_read();
                    measured_current = Convert.ToDouble(sp_line_buffer);
                    Debug.WriteLine("\t\t\t\t\t\t\t\t\t\t\t\t {0,8:F4}  {1,8:F4}  {2,8:F4}   Vmpp {3,8:F4}   Impp {4,8:F4}  MPP {5,8:F4}  IscScaled {6,8:F4}",
                        calculated_voltage,
                        measured_current,
                        calculated_voltage * measured_current,
                        Vmpp,
                        Impp,
                        Vmpp * Impp,
                        IscScaled);

                    this.Invoke(new MethodInvoker(delegate
                    {
                        chartPowerLevels.Series[0].Points.Clear();
                        chartPowerLevels.Series[0].Points.AddXY(0.5, measured_current / Impp * .8);
                        chartPowerLevels.Series[0].Points.AddXY(1.5, measured_voltage / Vmpp * .8);
                        chartPowerLevels.Series[0].Points.AddXY(2.5, (measured_current * measured_voltage) / (Impp * Vmpp) * .8);
                        label_Amps.Text = String.Format("{0,8:F4} A", measured_current);
                        label_Volts.Text = String.Format("{0,8:F4} V", measured_voltage);
                        label_Watts.Text = String.Format("{0,8:F4} W", measured_current * measured_voltage);
                    }));

                }

                try
                {
                    serialPort_GPIB_PowerSupply.Close();
                }
                catch (Exception exception)
                {
                    MessageBox.Show(exception.Message);
                }
            }

            this.Invoke(new MethodInvoker(delegate
            {
                button_RunSimulator.Text = "Start";
            }));

        }
    }
}
