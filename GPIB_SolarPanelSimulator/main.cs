using System;
using System.IO.Ports;


namespace SerialTestCs
    {
    class main
        {
        static void Main(string[] args)
            {


            double temperature = 298.15; //set temperature value to 25C
            double Voc = 10.666; //set Voltage open circuit value
            double Vmpp = 9.116;
            double Isc = 0.978;
            double Impp = 0.915;
            //double vt = ((2 * Vmpp - Voc) / (Math.Log((Isc - Impp) / Isc) + Impp / (Isc - Impp))) * temperature / 298;
            //double i0 = (Isc - Impp) / (Math.Exp(Vmpp / vt) - 1);
            //double v = vt * Math.Log(1 + (Isc - current) / i0); //get the voltage value

            double vt = get_vt(Vmpp, Voc, Isc, Impp, temperature);
            double i0 = get_i0(Isc, Impp, Vmpp, vt);

            string sp_line_buffer;
            double measured_current, calculated_voltage;

            SerialPort sp = new SerialPort();
            // COM port parameters
            sp.PortName = "COM9";
            sp.BaudRate = 115200;
            sp.DataBits = 8;
            sp.Parity = Parity.None;
            sp.StopBits = StopBits.One;
            sp.Handshake = Handshake.RequestToSend;
            sp.DtrEnable = true;

            // Error handling
            sp.DiscardNull = false;
            sp.ParityReplace = 0;

            try
            {
                sp.Open();
            }
            catch (Exception e)
            {
                System.Console.Write(e.Message);
            }


            // Get the model name from the power supply
            sp.WriteLine("*RST ; *IDN?"); // reset the power supply
            sp_line_buffer = sp.ReadLine();
            Console.Write("ID string: " + sp_line_buffer + "\n");

            if (sp_line_buffer != "HEWLETT-PACKARD,E3631A,0,2.1-5.0-1.0")
            {
                Console.Write("Invalid device. Halting...\n");
                while (true) ;
            }

            // set the power supply to 25v mode
            sp.WriteLine(":inst:sel p25v ; :inst:sel?");
            sp_line_buffer = sp.ReadLine();
            Console.WriteLine("Instrument selected: " + sp_line_buffer);

            // send the voltage to the Voltage open circuit value
            sp.WriteLine(":volt " + Voc + "; :volt?");
            sp_line_buffer = sp.ReadLine();
            Console.WriteLine("Voltage set to: " + sp_line_buffer + " volts");

            // send the current limit to the Current open circuit value
            sp.WriteLine(":curr " + Isc + "; :curr?");
            sp_line_buffer = sp.ReadLine();
            Console.WriteLine("Current set to: " + sp_line_buffer + " amps");

            sp.Write("outp on ; outp?\n"); //send the current limit to 0.0 amps
            sp_line_buffer = sp.ReadLine();
            Console.WriteLine("Output: " + sp_line_buffer);

            // Measure the current the first time
            sp.WriteLine(":meas:curr?");
            sp_line_buffer = sp.ReadLine();
            measured_current = Convert.ToDouble(sp_line_buffer);
            Console.WriteLine("Current set to: " + measured_current + " amps");

            while (true)
            {
                // Calculate the voltage based on the current
                calculated_voltage = get_voltage(vt, Isc, measured_current, i0);

                // Measure the current
                sp.WriteLine(":volt " + calculated_voltage + "; :meas:curr?");
                sp_line_buffer = sp.ReadLine();
                measured_current = Convert.ToDouble(sp_line_buffer);
                Console.WriteLine(" {0,8:F4}  {1,8:F4}  {2,8:F4}   Vmpp {3,8:F4}   Impp {4,8:F4}  MPP {5,8:F4}", calculated_voltage, measured_current, calculated_voltage * measured_current, Vmpp, Impp, Vmpp * Impp);


            }
        }//main

        static public double get_vt(double Vmpp, double Voc, double Isc, double Impp, double temperature) {
            return ((2 * Vmpp - Voc) / (Math.Log((Isc - Impp) / Isc) + Impp / (Isc - Impp))) * temperature / 298;
        }//get vt

        static public double get_i0(double Isc, double Impp, double Vmpp, double vt) {
            return (Isc - Impp) / (Math.Exp(Vmpp / vt) - 1);
        }//get i0 

        static public double get_voltage(double vt, double Isc, double current, double i0) {
            return vt * Math.Log(1 + (Isc - current) / i0);
        }//get_voltage value

        static public double get_current(double vt, double Isc, double i0, double voltage) {
            return Isc - i0 * (Math.Exp(voltage / vt) - 1);
        }

        static public void write_output(double Isc, double Voc, double interval, double vt, double i0) {
            Console.WriteLine("Current:--------------------");

            for (double voltage = 0.0; voltage <= Voc; voltage = voltage + interval)
            {
                Console.WriteLine(voltage); //print voltage
            }//for loop

            Console.WriteLine("Current:--------------------");

            for (double voltage = 0.0; voltage <= Voc; voltage = voltage + interval)
            {
                Console.WriteLine(get_current(vt, Isc, i0, voltage)); //print current
            }//for loop

            Console.WriteLine("Power:--------------------");

            for (double voltage = 0.0; voltage <= Voc; voltage = voltage + interval)
            {
                Console.WriteLine(get_current(vt, Isc, i0, voltage) * voltage); //print power
            }//for loop
        }// write_current

    }
    }
