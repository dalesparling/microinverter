﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace DEMO_ISL81X01
{
    public struct Range
    {
        public double max;
        public double min;
    }
    public partial class Configuration : Form
    {
        Demo demo;
        DataTable tblConfig;
        double mvo, vom, mio, iom, mvi, vim, mii, iim, kvo, voo, k1, ix, kvi, vio, k2, iy;
        bool sign_mvo, sign_mio, sign_mvi, sign_mii, sign_kvo, sign_k1, sign_kvi, sign_k2;
        double vout_min, vout_max, iout_min, iout_max, vin_min, vin_max, iin_min, iin_max;
        Range vout_range, iout_max_range, iout_min_range, vin_range, iin_max_range, iin_min_range;

        public class Define
        {
            //for release version: debug = 0; for debug version: debug = 1.
            public const int debug = 0;
            public const double vcc = 3.3;
        }

        #region construct
        public Configuration()
        {
            InitializeComponent();
            this.MaximizeBox = false;

            if (Define.debug == 1)
            {
                lbReverse.Visible = txtReverse.Visible = lbReverseScale.Visible = true;
                lbPWM.Visible = txtPWM.Visible = lbPWMrange.Visible = true;
            }
            else
            {
                lbReverse.Visible = txtReverse.Visible = lbReverseScale.Visible = false;
                lbPWM.Visible = txtPWM.Visible = lbPWMrange.Visible = false;
            }

            tblConfig = new DataTable();
            DataColumn colMvo = tblConfig.Columns.Add("mvo", typeof(double));
            DataColumn colVom = tblConfig.Columns.Add("Vom", typeof(double));
            DataColumn colMio = tblConfig.Columns.Add("mio", typeof(double));
            DataColumn colIom = tblConfig.Columns.Add("Iom", typeof(double));
            DataColumn colMvi = tblConfig.Columns.Add("mvi", typeof(double));
            DataColumn colVim = tblConfig.Columns.Add("Vim", typeof(double));
            DataColumn colMii = tblConfig.Columns.Add("mii", typeof(double));
            DataColumn colIim = tblConfig.Columns.Add("Iim", typeof(double));
            DataColumn colKvo = tblConfig.Columns.Add("kVO", typeof(double));
            DataColumn colVoo = tblConfig.Columns.Add("Voo", typeof(double));
            DataColumn colK1 = tblConfig.Columns.Add("K1", typeof(double));
            DataColumn colIx = tblConfig.Columns.Add("Ix", typeof(double));
            DataColumn colKvi = tblConfig.Columns.Add("kVI", typeof(double));
            DataColumn colVio = tblConfig.Columns.Add("Vio", typeof(double));
            DataColumn colK2 = tblConfig.Columns.Add("K2", typeof(double));
            DataColumn colIy = tblConfig.Columns.Add("Iy", typeof(double));
            DataColumn colVoutMin = tblConfig.Columns.Add("Vout_min", typeof(double));
            DataColumn colVoutMax = tblConfig.Columns.Add("Vout_max", typeof(double));
            DataColumn colIoutMin = tblConfig.Columns.Add("Iout_min", typeof(double));
            DataColumn colIoutMax = tblConfig.Columns.Add("Iout_max", typeof(double));
            DataColumn colVinMin = tblConfig.Columns.Add("Vin_min", typeof(double));
            DataColumn colVinMax = tblConfig.Columns.Add("Vin_max", typeof(double));
            DataColumn colIinMin = tblConfig.Columns.Add("Iin_min", typeof(double));
            DataColumn colIinMax = tblConfig.Columns.Add("Iin_max", typeof(double));
            DataColumn colStepV = tblConfig.Columns.Add("Step_V", typeof(double));
            DataColumn colStepC = tblConfig.Columns.Add("Step_C", typeof(double));
            DataColumn colIinRe = tblConfig.Columns.Add("Iin_Re", typeof(double));
            DataColumn colPWMVal = tblConfig.Columns.Add("PWM_Val", typeof(double));
        }
        #endregion

        #region load
        private void Setting_Load(object sender, EventArgs e)
        {
            demo = (Demo)this.Owner;

            iin_max = demo.max_iinset;
            iout_max = demo.max_ioutset;
            vin_max = demo.max_vinset;
            vout_max = demo.max_voutset;
            iin_min = demo.min_iinset;
            iout_min = demo.min_ioutset;
            vin_min = demo.min_vinset;
            vout_min = demo.min_voutset;

            mvo = demo.mvo;
            vom = demo.vom;
            mio = demo.mio;
            iom = demo.iom;
            mvi = demo.mvi;
            vim = demo.vim;
            mii = demo.mii;
            iim = demo.iim;
            kvo = demo.kvo;
            voo = demo.voo;
            k1 = demo.k1;
            ix = demo.ix;
            kvi = demo.kvi;
            vio = demo.vio;
            k2 = demo.k2;
            iy = demo.iy;

            sign_k1 = Sign_Judge(k1);
            sign_k2 = Sign_Judge(k2);
            sign_kvi = Sign_Judge(kvi);
            sign_kvo = Sign_Judge(kvo);
            sign_mii = Sign_Judge(mii);
            sign_mio = Sign_Judge(mio);
            sign_mvi = Sign_Judge(mvi);
            sign_mvo = Sign_Judge(mvo);

            vout_range = GetRange(mvo, vom, sign_mvo);
            vin_range = GetRange(mvi, vim, sign_mvi);
            iout_max_range = GetRange(mio, iom, sign_mio);
            iout_max_range.min = 0;
            iout_min_range.min = -1 * iout_max_range.max;
            iout_min_range.max = 0;
            iin_min_range = GetRange(mii, iim, sign_mii);
            iin_min_range.max = 0;
            iin_max_range.min = 0;
            iin_max_range.max = -1 * iin_min_range.min;

            txtIim.Text = demo.iim.ToString();
            txtIinMax.Text = demo.max_iinset.ToString();
            txtIinMin.Text = demo.min_iinset.ToString();
            txtIom.Text = demo.iom.ToString();
            txtIoutMax.Text = demo.max_ioutset.ToString();
            txtIoutMin.Text = demo.min_ioutset.ToString();
            txtIx.Text = demo.ix.ToString();
            txtIy.Text = demo.iy.ToString();
            txtK1.Text = demo.k1.ToString();
            txtK2.Text = demo.k2.ToString();
            txtkVI.Text = demo.kvi.ToString();
            txtkVO.Text = demo.kvo.ToString();
            txtmii.Text = demo.mii.ToString();
            txtmio.Text = demo.mio.ToString();
            txtmvi.Text = demo.mvi.ToString();
            txtmvo.Text = demo.mvo.ToString();
            txtReverse.Text = demo.iin_reverse.ToString();
            txtPWM.Text = demo.pwmPeriod.ToString();
            txtStepI.Text = demo.step_i.ToString();
            txtStepV.Text = demo.step_v.ToString();
            txtVim.Text = demo.vim.ToString();
            txtVinMax.Text = demo.max_vinset.ToString();
            txtVinMin.Text = demo.min_vinset.ToString();
            txtVom.Text = demo.vom.ToString();
            txtVoutMax.Text = demo.max_voutset.ToString();
            txtVoutMin.Text = demo.min_voutset.ToString();
            txtVoo.Text = demo.voo.ToString();
            txtVio.Text = demo.vio.ToString();
        }
        #endregion

        #region KeyPress
        /// <summary>
        /// 48~57=0~9, 8=BS, 127=DEL, 45=- ,46=.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void textBox_KeyPress(object sender, KeyPressEventArgs e)
        {
            int a = (int)e.KeyChar;
            if ((a < 48 || a > 57) && a != 8 && a != 127 && a != 46 && a != 45)
            {
                e.Handled = true;
            }
            else if (a == 46 && ((TextBox)sender).Text.IndexOf('.') != -1)
            {
                e.Handled = true;
            }
            else if (a == 46 && (((TextBox)sender).Text == "" || ((TextBox)sender).Text == "-"))
            {
                e.Handled = true;
            }
            else if (a == 45 && ((TextBox)sender).Text != "")
            {
                e.Handled = true;
            }
            else
            {
                ;
            }
        }

        /// <summary>
        /// 48~57=0~9, 8=BS, 127=DEL
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtPWM_KeyPress(object sender, KeyPressEventArgs e)
        {
            int a = (int)e.KeyChar;
            if ((a < 48 || a > 57) && a != 8 && a != 127)
            {
                e.Handled = true;
            }
        }
        #endregion

        #region setting
        protected bool textBox_Check(Control.ControlCollection cc)
        {
            foreach (Control item in cc)
            {
                if (item.HasChildren)
                {
                    if (textBox_Check(item.Controls))
                    {
                        return true;
                    }
                }
                if (item is TextBox)
                {
                    if (((TextBox)item).BackColor == System.Drawing.Color.Red)
                    {
                        return true;
                    }
                }
            }
            return false;
        }

        protected void btnSave_Refresh()
        {
            if (textBox_Check(this.Controls))
            {
                btnSave.Enabled = false;
            }
            else
            {
                btnSave.Enabled = true;
            }
        }
        
        private void textBox_TextChanged(object sender, EventArgs e)
        {
            TextBox tb = sender as TextBox;
            if (!string.IsNullOrEmpty(tb.Text) && tb.Text != "-")
            {
                tb.BackColor = System.Drawing.Color.White;
                if (!DotJudge(sender))
                {
                    tb.Text = CutDecimalWithOne(tb.Text);
                }
            }
            else
            {
                tb.BackColor = System.Drawing.Color.Red;
            }

            btnSave_Refresh();
        }

        private void txtSenseCorrections_TextChanged(object sender, EventArgs e)
        {
            TextBox tb = sender as TextBox;
            if (!string.IsNullOrEmpty(tb.Text) && tb.Text != "-")
            {
                tb.BackColor = System.Drawing.Color.White;
            }
            else
            {
                tb.BackColor = System.Drawing.Color.Red;
            }

            btnSave_Refresh();
        }

        private void txtPWM_TextChanged(object sender, EventArgs e)
        {
            TextBox tb = sender as TextBox;
            if (!string.IsNullOrEmpty(tb.Text))
            {
                double tempValue = System.Convert.ToDouble(tb.Text);
                if (tempValue > 65535)
                {
                    tb.BackColor = System.Drawing.Color.Red;
                }
                else
                {
                    tb.BackColor = System.Drawing.Color.White;
                }
            }
            else
            {
                tb.BackColor = System.Drawing.Color.Red;
            }

            btnSave_Refresh();
        }

        protected void txtRange_TextChanged(object sender, double value, Range rng, TextBox tb_slope, TextBox tb_intercept)
        {
            TextBox tb = sender as TextBox;
            if (!string.IsNullOrEmpty(tb.Text) && tb.Text != "-")
            {
                if (!DotJudge(sender))
                {
                    tb.Text = CutDecimalWithOne(tb.Text);
                }
                value = System.Convert.ToDouble(tb.Text);
                if (!string.IsNullOrEmpty(tb_slope.Text) && tb_slope.Text != "-" && !string.IsNullOrEmpty(tb_intercept.Text) && tb_intercept.Text != "-")
                {
                    if (RangeJudge(value, rng))
                    {
                        tb.BackColor = System.Drawing.Color.White;
                    }
                    else
                    {
                        tb.BackColor = System.Drawing.Color.Red;
                    }
                }
                else
                {
                    tb.BackColor = System.Drawing.Color.White;
                }
            }
            else
            {
                tb.BackColor = System.Drawing.Color.Red;
            }

            btnSave_Refresh();
        }

        private void txtVoutMin_TextChanged(object sender, EventArgs e)
        {
            txtRange_TextChanged(sender, vout_min, vout_range, txtmvo, txtVom);
            if (!string.IsNullOrEmpty(txtVoutMin.Text) && txtVoutMin.Text != "-")
            {
                vout_min = System.Convert.ToDouble(txtVoutMin.Text);
            }
        }

        private void txtVoutMax_TextChanged(object sender, EventArgs e)
        {
            txtRange_TextChanged(sender, vout_max, vout_range, txtmvo, txtVom);
            if (!string.IsNullOrEmpty(txtVoutMax.Text) && txtVoutMax.Text != "-")
            {
                vout_max = System.Convert.ToDouble(txtVoutMax.Text);
            }
        }

        private void txtIoutMin_TextChanged(object sender, EventArgs e)
        {
            txtRange_TextChanged(sender, iout_min, iout_min_range, txtmio, txtIom);
            if (!string.IsNullOrEmpty(txtIoutMin.Text) && txtIoutMin.Text != "-")
            {
                iout_min = System.Convert.ToDouble(txtIoutMin.Text);
            }
        }

        private void txtIoutMax_TextChanged(object sender, EventArgs e)
        {
            txtRange_TextChanged(sender, iout_max, iout_max_range, txtmio, txtIom);
            if (!string.IsNullOrEmpty(txtIoutMax.Text) && txtIoutMax.Text != "-")
            {
                iout_max = System.Convert.ToDouble(txtIoutMax.Text);
            }
        }

        private void txtVinMin_TextChanged(object sender, EventArgs e)
        {
            txtRange_TextChanged(sender, vin_min, vin_range, txtmvi, txtVim);
            if (!string.IsNullOrEmpty(txtVinMin.Text) && txtVinMin.Text != "-")
            {
                vin_min = System.Convert.ToDouble(txtVinMin.Text);
            }
        }

        private void txtVinMax_TextChanged(object sender, EventArgs e)
        {
            txtRange_TextChanged(sender, vin_max, vin_range, txtmvi, txtVim);
            if (!string.IsNullOrEmpty(txtVinMax.Text) && txtVinMax.Text != "-")
            {
                vin_max = System.Convert.ToDouble(txtVinMax.Text);
            }
        }

        private void txtIinMin_TextChanged(object sender, EventArgs e)
        {
            txtRange_TextChanged(sender, iin_min, iin_min_range, txtmii, txtIim);
            if (!string.IsNullOrEmpty(txtIinMin.Text) && txtIinMin.Text != "-")
            {
                iin_min = System.Convert.ToDouble(txtIinMin.Text);
            }
        }

        private void txtIinMax_TextChanged(object sender, EventArgs e)
        {
            txtRange_TextChanged(sender, iin_max, iin_max_range, txtmii, txtIim);
            if (!string.IsNullOrEmpty(txtIinMax.Text) && txtIinMax.Text != "-")
            {
                iin_max = System.Convert.ToDouble(txtIinMax.Text);
            }
        }

        protected void txtSlpCorrections_TextChanged(object sender, double d_slp, double itcp, bool b_slp, Range rng, double max, double min, 
            TextBox tb_itcp, TextBox tb_max, TextBox tb_min)
        {
            TextBox tb = sender as TextBox;
            if (!string.IsNullOrEmpty(tb.Text) && tb.Text != "-")
            {
                tb.BackColor = System.Drawing.Color.White;
                d_slp = System.Convert.ToDouble(tb.Text);
                b_slp = Sign_Judge(d_slp);
                if (!string.IsNullOrEmpty(tb_itcp.Text) && tb_itcp.Text != "-")
                {
                    rng = GetRange(d_slp, itcp, b_slp);
                    if (!string.IsNullOrEmpty(tb_max.Text) && tb_max.Text != "-")
                    {
                        if (RangeJudge(max, rng))
                        {
                            tb_max.BackColor = System.Drawing.Color.White;
                        }
                        else
                        {
                            tb_max.BackColor = System.Drawing.Color.Red;
                        }
                    }
                    if (!string.IsNullOrEmpty(tb_min.Text) && tb_min.Text != "-")
                    {

                        if (RangeJudge(min, rng))
                        {
                            tb_min.BackColor = System.Drawing.Color.White;
                        }
                        else
                        {
                            tb_min.BackColor = System.Drawing.Color.Red;
                        }
                    }
                }
            }
            else
            {
                tb.BackColor = System.Drawing.Color.Red;
            }

            btnSave_Refresh();
        }

        private void txtmvo_TextChanged(object sender, EventArgs e)
        {
            txtSlpCorrections_TextChanged(sender, mvo, vom, sign_mvo, vout_range, vout_max, vout_min, txtVom, txtVoutMax, txtVoutMin);
            if (!string.IsNullOrEmpty(txtmvo.Text) && txtmvo.Text != "-")
            {
                mvo = System.Convert.ToDouble(txtmvo.Text);
                sign_mvo = Sign_Judge(mvo);
                if (!string.IsNullOrEmpty(txtVom.Text) && txtVom.Text != "-")
                {
                    vout_range = GetRange(mvo, vom, sign_mvo);
                }
            }
        }

        private void txtmio_TextChanged(object sender, EventArgs e)
        {
            TextBox tb = sender as TextBox;
            if (!string.IsNullOrEmpty(tb.Text) && tb.Text != "-")
            {
                tb.BackColor = System.Drawing.Color.White;
                mio = System.Convert.ToDouble(tb.Text);
                sign_mio = Sign_Judge(mio);
                if (!string.IsNullOrEmpty(txtIom.Text) && txtIom.Text != "-")
                {
                    iout_max_range = GetRange(mio, iom, sign_mio);
                    iout_max_range.min = 0;
                    iout_min_range.min = -1 * iout_max_range.max;
                    if (!string.IsNullOrEmpty(txtIoutMax.Text) && txtIoutMax.Text != "-")
                    {
                        if (RangeJudge(iout_max, iout_max_range))
                        {
                            txtIoutMax.BackColor = System.Drawing.Color.White;
                        }
                        else
                        {
                            txtIoutMax.BackColor = System.Drawing.Color.Red;
                        }
                    }
                    if (!string.IsNullOrEmpty(txtIoutMin.Text) && txtIoutMin.Text != "-")
                    {

                        if (RangeJudge(iout_min, iout_min_range))
                        {
                            txtIoutMin.BackColor = System.Drawing.Color.White;
                        }
                        else
                        {
                            txtIoutMin.BackColor = System.Drawing.Color.Red;
                        }
                    }
                }
            }
            else
            {
                tb.BackColor = System.Drawing.Color.Red;
            }

            btnSave_Refresh();
        }

        private void txtmvi_TextChanged(object sender, EventArgs e)
        {
            txtSlpCorrections_TextChanged(sender, mvi, vim, sign_mvi, vin_range, vin_max, vin_min, txtVim, txtVinMax, txtVinMin);
            if (!string.IsNullOrEmpty(txtmvi.Text) && txtmvi.Text != "-")
            {
                mvi = System.Convert.ToDouble(txtmvi.Text);
                sign_mvi = Sign_Judge(mvi);
                if (!string.IsNullOrEmpty(txtVim.Text) && txtVim.Text != "-")
                {
                    vin_range = GetRange(mvi, vim, sign_mvi);
                }
            }
        }

        private void txtmii_TextChanged(object sender, EventArgs e)
        {
            TextBox tb = sender as TextBox;
            if (!string.IsNullOrEmpty(tb.Text) && tb.Text != "-")
            {
                tb.BackColor = System.Drawing.Color.White;
                mii = System.Convert.ToDouble(tb.Text);
                sign_mii = Sign_Judge(mii);
                if (!string.IsNullOrEmpty(txtIim.Text) && txtIim.Text != "-")
                {
                    iin_min_range = GetRange(mii, iim, sign_mii);
                    iin_min_range.max = 0;
                    iin_max_range.max = -1 * iin_min_range.min;
                    if (!string.IsNullOrEmpty(txtIinMax.Text) && txtIinMax.Text != "-")
                    {
                        if (RangeJudge(iin_max, iin_max_range))
                        {
                            txtIinMax.BackColor = System.Drawing.Color.White;
                        }
                        else
                        {
                            txtIinMax.BackColor = System.Drawing.Color.Red;
                        }
                    }
                    if (!string.IsNullOrEmpty(txtIinMin.Text) && txtIinMin.Text != "-")
                    {

                        if (RangeJudge(iin_min, iin_min_range))
                        {
                            txtIinMin.BackColor = System.Drawing.Color.White;
                        }
                        else
                        {
                            txtIinMin.BackColor = System.Drawing.Color.Red;
                        }
                    }
                }
            }
            else
            {
                tb.BackColor = System.Drawing.Color.Red;
            }

            btnSave_Refresh();
        }

        protected void txtItcpCorrections_TextChanged(object sender, double d_slp, double itcp, bool b_slp, Range rng, double max, double min,
            TextBox tb_slp, TextBox tb_max, TextBox tb_min)
        {
            TextBox tb = sender as TextBox;
            if (!string.IsNullOrEmpty(tb.Text) && tb.Text != "-")
            {
                tb.BackColor = System.Drawing.Color.White;
                itcp = System.Convert.ToDouble(tb.Text);
                if (!string.IsNullOrEmpty(tb_slp.Text) && tb_slp.Text != "-")
                {
                    rng = GetRange(d_slp, itcp, b_slp);
                    if (!string.IsNullOrEmpty(tb_max.Text) && tb_max.Text != "-")
                    {
                        if (RangeJudge(max, rng))
                        {
                            tb_max.BackColor = System.Drawing.Color.White;
                        }
                        else
                        {
                            tb_max.BackColor = System.Drawing.Color.Red;
                        }
                    }
                    if (!string.IsNullOrEmpty(tb_min.Text) && tb_min.Text != "-")
                    {
                        if (RangeJudge(min, rng))
                        {
                            tb_min.BackColor = System.Drawing.Color.White;
                        }
                        else
                        {
                            tb_min.BackColor = System.Drawing.Color.Red;
                        }
                    }
                }
            }
            else
            {
                tb.BackColor = System.Drawing.Color.Red;
            }

            btnSave_Refresh();
        }
        
        private void txtVom_TextChanged(object sender, EventArgs e)
        {
            txtItcpCorrections_TextChanged(sender, mvo, vom, sign_mvo, vout_range, vout_max, vout_min, txtmvo, txtVoutMax, txtVoutMin);
            if (!string.IsNullOrEmpty(txtVom.Text) && txtVom.Text != "-")
            {
                vom = System.Convert.ToDouble(txtVom.Text);
                if (!string.IsNullOrEmpty(txtmvo.Text) && txtmvo.Text != "-")
                {
                    vout_range = GetRange(mvo, vom, sign_mvo);
                }
            }
        }
        
        private void txtIom_TextChanged(object sender, EventArgs e)
        {
            TextBox tb = sender as TextBox;
            if (!string.IsNullOrEmpty(tb.Text) && tb.Text != "-")
            {
                tb.BackColor = System.Drawing.Color.White;
                iom = System.Convert.ToDouble(tb.Text);
                if (!string.IsNullOrEmpty(txtmio.Text) && txtmio.Text != "-")
                {
                    iout_max_range = GetRange(mio, iom, sign_mio);
                    iout_max_range.min = 0;
                    iout_min_range.min = -1 * iout_max_range.max;
                    if (!string.IsNullOrEmpty(txtIoutMax.Text) && txtIoutMax.Text != "-")
                    {
                        if (RangeJudge(iout_max, iout_max_range))
                        {
                            txtIoutMax.BackColor = System.Drawing.Color.White;
                        }
                        else
                        {
                            txtIoutMax.BackColor = System.Drawing.Color.Red;
                        }
                    }
                    if (!string.IsNullOrEmpty(txtIoutMin.Text) && txtIoutMin.Text != "-")
                    {

                        if (RangeJudge(iout_min, iout_min_range))
                        {
                            txtIoutMin.BackColor = System.Drawing.Color.White;
                        }
                        else
                        {
                            txtIoutMin.BackColor = System.Drawing.Color.Red;
                        }
                    }
                }
            }
            else
            {
                tb.BackColor = System.Drawing.Color.Red;
            }

            btnSave_Refresh();
        }
        
        private void txtVim_TextChanged(object sender, EventArgs e)
        {
            txtItcpCorrections_TextChanged(sender, mvi, vim, sign_mvi, vin_range, vin_max, vin_min, txtmvi, txtVinMax, txtVinMin);
            if (!string.IsNullOrEmpty(txtVim.Text) && txtVim.Text != "-")
            {
                vim = System.Convert.ToDouble(txtVim.Text);
                if (!string.IsNullOrEmpty(txtmvi.Text) && txtmvi.Text != "-")
                {
                    vin_range = GetRange(mvi, vim, sign_mvi);
                }
            }
        }

        private void txtIim_TextChanged(object sender, EventArgs e)
        {
            TextBox tb = sender as TextBox;
            if (!string.IsNullOrEmpty(tb.Text) && tb.Text != "-")
            {
                tb.BackColor = System.Drawing.Color.White;
                iim = System.Convert.ToDouble(tb.Text);
                if (!string.IsNullOrEmpty(txtmii.Text) && txtmii.Text != "-")
                {
                    iin_min_range = GetRange(mii, iim, sign_mii);
                    iin_min_range.max = 0;
                    iin_max_range.max = -1 * iin_min_range.min;
                    if (!string.IsNullOrEmpty(txtIinMax.Text) && txtIinMax.Text != "-")
                    {
                        if (RangeJudge(iin_max, iin_max_range))
                        {
                            txtIinMax.BackColor = System.Drawing.Color.White;
                        }
                        else
                        {
                            txtIinMax.BackColor = System.Drawing.Color.Red;
                        }
                    }
                    if (!string.IsNullOrEmpty(txtIinMin.Text) && txtIinMin.Text != "-")
                    {

                        if (RangeJudge(iin_min, iin_min_range))
                        {
                            txtIinMin.BackColor = System.Drawing.Color.White;
                        }
                        else
                        {
                            txtIinMin.BackColor = System.Drawing.Color.Red;
                        }
                    }
                }
            }
            else
            {
                tb.BackColor = System.Drawing.Color.Red;
            }

            btnSave_Refresh();
        }

        private void txtVout_DoubleClick(object sender, EventArgs e)
        {
            MessageBox.Show("Range: " + vout_range.min.ToString() + " - " + vout_range.max.ToString(), "Tips", MessageBoxButtons.OK);
        }

        private void txtIoutMax_DoubleClick(object sender, EventArgs e)
        {
            MessageBox.Show("Range: " + iout_max_range.min.ToString() + " - " + iout_max_range.max.ToString(), "Tips", MessageBoxButtons.OK);
        }

        private void txtIoutMin_DoubleClick(object sender, EventArgs e)
        {
            MessageBox.Show("Range: " + iout_min_range.min.ToString() + " - " + iout_min_range.max.ToString(), "Tips", MessageBoxButtons.OK);
        }

        private void txtVin_DoubleClick(object sender, EventArgs e)
        {
            MessageBox.Show("Range: " + vin_range.min.ToString() + " - " + vin_range.max.ToString(), "Tips", MessageBoxButtons.OK);
        }

        private void txtIinMax_DoubleClick(object sender, EventArgs e)
        {
            MessageBox.Show("Range: " + iin_max_range.min.ToString() + " - " + iin_max_range.max.ToString(), "Tips", MessageBoxButtons.OK);
        }

        private void txtIinMin_DoubleClick(object sender, EventArgs e)
        {
            MessageBox.Show("Range: " + iin_min_range.min.ToString() + " - " + iin_min_range.max.ToString(), "Tips", MessageBoxButtons.OK);
        }

        protected Range GetRange(double slope, double intercept, bool sign)
        {
            Range rng = new Range();
            if (sign)
            {
                rng.max = CutDecimalWithOne_max(slope * Define.vcc + intercept);
                rng.min = CutDecimalWithOne_min(intercept);
            }
            else
            {
                rng.max = CutDecimalWithOne_max(intercept);
                rng.min = CutDecimalWithOne_min(slope * Define.vcc + intercept);
            }
            return rng;
        }

        protected bool Sign_Judge(double value)
        {
            if (value < 0)
            {
                return false;
            }
            else
            {
                return true;
            }
        }

        protected bool RangeJudge(double value, Range rng)
        {
            if (value > rng.max || value < rng.min)
            {
                return false;
            }
            else
            {
                return true;
            }
        }

        /// <summary>
        /// 小数点后面数字的个数大于1位返回false
        /// </summary>
        /// <param name="sender"></param>
        /// <returns></returns>
        protected bool DotJudge(object sender)
        {
            int i, j = 0;
            bool dot = false;
            TextBox tb = sender as TextBox;

            if (tb.Text == ".")
            {
                return false;
            }
            for (i = 1; i < tb.Text.Length; i++)
            {
                if (dot)
                {
                    j++;
                }
                if (j > 1)
                {
                    return false;
                }
                if (tb.Text[i] == '.')
                {
                    dot = true;
                }
            }
            
            return true;
        }

        protected double CutDecimalWithOne_max(double value)
        {
            string str = value.ToString();
            int count = str.LastIndexOf(".");
            if ((str.Length - count - 1) > 1 && count != -1)
            {
                if (value > 0)
                {
                    value = Double.Parse(str.Substring(0, count + 2));
                }
                else
                {
                    value = Double.Parse(str.Substring(0, count + 2)) - 0.1;
                }
            }

            return value;
        }

        protected double CutDecimalWithOne_min(double value)
        {
            string str = value.ToString();
            int count = str.LastIndexOf(".");
            if ((str.Length - count - 1) > 1 && count != -1)
            {
                if (value > 0)
                {
                    value = Double.Parse(str.Substring(0, count + 2)) + 0.1;
                }
                else
                {
                    value = Double.Parse(str.Substring(0, count + 2));
                }
            }

            return value;
        }

        protected string CutDecimalWithOne(string str)
        {
            int count = str.LastIndexOf(".");
            if ((str.Length - count - 1) > 1 && count != -1)
            {
                str = Double.Parse(str.Substring(0, count + 2)).ToString();
            }
            return str;
        }
        #endregion

        #region save
        private void btnSave_Click(object sender, EventArgs e)
        {
            bool err_vout = false, err_vin = false, err_iout = false, err_iin = false;
            string err_info = "The max value must be greater than the min value.\nPlease change the range setting:";
            double max_vout, min_vout, max_vin, min_vin, max_iout, min_iout, max_iin, min_iin;
            max_vout = System.Convert.ToDouble(this.txtVoutMax.Text);
            min_vout = System.Convert.ToDouble(this.txtVoutMin.Text);
            max_vin = System.Convert.ToDouble(this.txtVinMax.Text);
            min_vin = System.Convert.ToDouble(this.txtVinMin.Text);
            max_iout = System.Convert.ToDouble(this.txtIoutMax.Text);
            min_iout = System.Convert.ToDouble(this.txtIoutMin.Text);
            max_iin = System.Convert.ToDouble(this.txtIinMax.Text);
            min_iin = System.Convert.ToDouble(this.txtIinMin.Text);
            if (min_vout > max_vout)
            {
                err_vout = true;
                err_info += "\nVout";
            }
            if (min_vin > max_vin)
            {
                err_vin = true;
                err_info += "\nVin";
            }
            if (min_iout > max_iout)
            {
                err_iout = true;
                err_info += "\nIout";
            }
            if (min_iin > max_iin)
            {
                err_iin = true;
                err_info += "\nIin";
            }

            if (err_vout || err_vin || err_iout || err_iin)
            {
                MessageBox.Show(err_info, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else
            {
                if (demo != null)
                {
                    demo.vim = System.Convert.ToDouble(this.txtVim.Text);
                    demo.mvi = System.Convert.ToDouble(this.txtmvi.Text);
                    demo.vom = System.Convert.ToDouble(this.txtVom.Text);
                    demo.mvo = System.Convert.ToDouble(this.txtmvo.Text);
                    demo.iom = System.Convert.ToDouble(this.txtIom.Text);
                    demo.mio = System.Convert.ToDouble(this.txtmio.Text);
                    demo.iim = System.Convert.ToDouble(this.txtIim.Text);
                    demo.mii = System.Convert.ToDouble(this.txtmii.Text);
                    demo.kvi = System.Convert.ToDouble(this.txtkVI.Text);
                    demo.kvo = System.Convert.ToDouble(this.txtkVO.Text);
                    demo.k2 = System.Convert.ToDouble(this.txtK2.Text);
                    demo.iy = System.Convert.ToDouble(this.txtIy.Text);
                    demo.k1 = System.Convert.ToDouble(this.txtK1.Text);
                    demo.ix = System.Convert.ToDouble(this.txtIx.Text);
                    demo.min_voutset = System.Convert.ToDouble(this.txtVoutMin.Text);
                    demo.max_voutset = System.Convert.ToDouble(this.txtVoutMax.Text);
                    demo.min_ioutset = System.Convert.ToDouble(this.txtIoutMin.Text);
                    demo.max_ioutset = System.Convert.ToDouble(this.txtIoutMax.Text);
                    demo.min_vinset = System.Convert.ToDouble(this.txtVinMin.Text);
                    demo.max_vinset = System.Convert.ToDouble(this.txtVinMax.Text);
                    demo.min_iinset = System.Convert.ToDouble(this.txtIinMin.Text);
                    demo.max_iinset = System.Convert.ToDouble(this.txtIinMax.Text);
                    demo.step_i = System.Convert.ToDouble(this.txtStepI.Text);
                    demo.step_v = System.Convert.ToDouble(this.txtStepV.Text);
                    demo.iin_reverse = System.Convert.ToDouble(this.txtReverse.Text);
                    demo.pwmPeriod = System.Convert.ToDouble(this.txtPWM.Text);
                    demo.vio = System.Convert.ToDouble(this.txtVio.Text);
                    demo.voo = System.Convert.ToDouble(this.txtVoo.Text);

                    demo.chart_initial_value = demo.min_iinset - 10;
                    demo.vin_calmax = vin_range.max;

                    demo.vout = "Vout range: " + this.txtVoutMin.Text + "V - " + this.txtVoutMax.Text + "V";
                    demo.vin = "Vin range: " + this.txtVinMin.Text + "V - " + this.txtVinMax.Text + "V";
                    demo.iout = "Iout range: 0A - " + this.txtIoutMax.Text + "A";
                    demo.iin = "Iin range: " + this.txtIinMin.Text + "A - 0A";
                }

                DataRow rowAll = tblConfig.NewRow();
                rowAll["mvo"] = demo.mvo;
                rowAll["Vom"] = demo.vom;
                rowAll["mio"] = demo.mio;
                rowAll["Iom"] = demo.iom;
                rowAll["mvi"] = demo.mvi;
                rowAll["Vim"] = demo.vim;
                rowAll["mii"] = demo.mii;
                rowAll["Iim"] = demo.iim;
                rowAll["kVO"] = demo.kvo;
                rowAll["Voo"] = demo.voo;
                rowAll["K1"] = demo.k1;
                rowAll["Ix"] = demo.ix;
                rowAll["kVI"] = demo.kvi;
                rowAll["Vio"] = demo.vio;
                rowAll["K2"] = demo.k2;
                rowAll["Iy"] = demo.iy;
                rowAll["Vout_min"] = demo.min_voutset;
                rowAll["Vout_max"] = demo.max_voutset;
                rowAll["Iout_min"] = demo.min_ioutset;
                rowAll["Iout_max"] = demo.max_ioutset;
                rowAll["Vin_min"] = demo.min_vinset;
                rowAll["Vin_max"] = demo.max_vinset;
                rowAll["Iin_min"] = demo.min_iinset;
                rowAll["Iin_max"] = demo.max_iinset;
                rowAll["Step_V"] = demo.step_v;
                rowAll["Step_C"] = demo.step_i;
                rowAll["Iin_Re"] = demo.iin_reverse;
                rowAll["PWM_Val"] = demo.pwmPeriod;
                tblConfig.Rows.Add(rowAll);

                SaveCSV(tblConfig, "config.csv");
                tblConfig.Clear();

                this.Close();
            }
        }
        #endregion

        #region file
        public static void SaveCSV(DataTable dt, string fullPath)
        {
            System.IO.FileInfo fi = new System.IO.FileInfo(fullPath);
            if (!fi.Directory.Exists)
            {
                fi.Directory.Create();
            }
            System.IO.FileStream fs = new System.IO.FileStream(fullPath, System.IO.FileMode.Create, System.IO.FileAccess.Write);

            System.IO.StreamWriter sw = new System.IO.StreamWriter(fs, System.Text.Encoding.UTF8);

            string data = "";

            for (int i = 0; i < dt.Columns.Count; i++)
            {
                data += dt.Columns[i].ColumnName.ToString();

                if (i < dt.Columns.Count - 1)
                {
                    data += ",";
                }
            }
            sw.WriteLine(data);

            for (int i = 0; i < dt.Rows.Count; i++)
            {
                data = "";
                for (int j = 0; j < dt.Columns.Count; j++)
                {
                    string str = dt.Rows[i][j].ToString();

                    str = str.Replace("\"", "\"\"");

                    if (str.Contains(',') || str.Contains('"')

                        || str.Contains('\r') || str.Contains('\n'))
                    {
                        str = string.Format("\"{0}\"", str);
                    }

                    data += str;

                    if (j < dt.Columns.Count - 1)
                    {
                        data += ",";
                    }
                }
                sw.WriteLine(data);
            }
            sw.Close();
            fs.Close();
        }
        #endregion
    }
}
