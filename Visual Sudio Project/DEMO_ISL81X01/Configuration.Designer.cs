﻿namespace DEMO_ISL81X01
{
    partial class Configuration
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Configuration));
            this.gpSave = new System.Windows.Forms.GroupBox();
            this.btnSave = new System.Windows.Forms.Button();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.gpSense = new System.Windows.Forms.GroupBox();
            this.txtVoo = new System.Windows.Forms.TextBox();
            this.txtVio = new System.Windows.Forms.TextBox();
            this.lbVoo = new System.Windows.Forms.Label();
            this.lbkVI = new System.Windows.Forms.Label();
            this.lblInter2 = new System.Windows.Forms.Label();
            this.lblVio = new System.Windows.Forms.Label();
            this.lblSlope2 = new System.Windows.Forms.Label();
            this.lbVI_SEN = new System.Windows.Forms.Label();
            this.txtkVI = new System.Windows.Forms.TextBox();
            this.txtIy = new System.Windows.Forms.TextBox();
            this.txtK2 = new System.Windows.Forms.TextBox();
            this.lbIy = new System.Windows.Forms.Label();
            this.lbK2 = new System.Windows.Forms.Label();
            this.txtIx = new System.Windows.Forms.TextBox();
            this.txtK1 = new System.Windows.Forms.TextBox();
            this.lbIx = new System.Windows.Forms.Label();
            this.lbK1 = new System.Windows.Forms.Label();
            this.txtkVO = new System.Windows.Forms.TextBox();
            this.lbkVO = new System.Windows.Forms.Label();
            this.lbIO_SEN = new System.Windows.Forms.Label();
            this.lbII_SEN = new System.Windows.Forms.Label();
            this.lbVO_SEN = new System.Windows.Forms.Label();
            this.lbSense = new System.Windows.Forms.Label();
            this.gpControl = new System.Windows.Forms.GroupBox();
            this.lblInter1 = new System.Windows.Forms.Label();
            this.txtVim = new System.Windows.Forms.TextBox();
            this.lbVim = new System.Windows.Forms.Label();
            this.lbII_CTRL = new System.Windows.Forms.Label();
            this.lbmvi = new System.Windows.Forms.Label();
            this.lbIO_CTRL = new System.Windows.Forms.Label();
            this.lbVI_CTRL = new System.Windows.Forms.Label();
            this.lbVO_CTRL = new System.Windows.Forms.Label();
            this.txtmvi = new System.Windows.Forms.TextBox();
            this.lbControl = new System.Windows.Forms.Label();
            this.lblSlope1 = new System.Windows.Forms.Label();
            this.txtmio = new System.Windows.Forms.TextBox();
            this.txtIom = new System.Windows.Forms.TextBox();
            this.lbmio = new System.Windows.Forms.Label();
            this.lbIom = new System.Windows.Forms.Label();
            this.txtmii = new System.Windows.Forms.TextBox();
            this.txtIim = new System.Windows.Forms.TextBox();
            this.lbmii = new System.Windows.Forms.Label();
            this.lbIim = new System.Windows.Forms.Label();
            this.txtmvo = new System.Windows.Forms.TextBox();
            this.txtVom = new System.Windows.Forms.TextBox();
            this.lbmvo = new System.Windows.Forms.Label();
            this.lbVom = new System.Windows.Forms.Label();
            this.gpRange = new System.Windows.Forms.GroupBox();
            this.lbIinScale = new System.Windows.Forms.Label();
            this.lbVinScale = new System.Windows.Forms.Label();
            this.lbIoutScale = new System.Windows.Forms.Label();
            this.lblVoutScale = new System.Windows.Forms.Label();
            this.lbIin = new System.Windows.Forms.Label();
            this.lbVin = new System.Windows.Forms.Label();
            this.lbIout = new System.Windows.Forms.Label();
            this.lbVout = new System.Windows.Forms.Label();
            this.txtVinMax = new System.Windows.Forms.TextBox();
            this.txtVinMin = new System.Windows.Forms.TextBox();
            this.lbVinMax = new System.Windows.Forms.Label();
            this.lbVinMin = new System.Windows.Forms.Label();
            this.txtIinMax = new System.Windows.Forms.TextBox();
            this.txtIinMin = new System.Windows.Forms.TextBox();
            this.lbIinMax = new System.Windows.Forms.Label();
            this.lbIinMin = new System.Windows.Forms.Label();
            this.txtIoutMax = new System.Windows.Forms.TextBox();
            this.txtIoutMin = new System.Windows.Forms.TextBox();
            this.lbIoutMax = new System.Windows.Forms.Label();
            this.lbIoutMin = new System.Windows.Forms.Label();
            this.txtVoutMax = new System.Windows.Forms.TextBox();
            this.txtVoutMin = new System.Windows.Forms.TextBox();
            this.lbVoutMax = new System.Windows.Forms.Label();
            this.lbVoutMin = new System.Windows.Forms.Label();
            this.gpOther = new System.Windows.Forms.GroupBox();
            this.lbPWMrange = new System.Windows.Forms.Label();
            this.txtPWM = new System.Windows.Forms.TextBox();
            this.lbPWM = new System.Windows.Forms.Label();
            this.lbReverseScale = new System.Windows.Forms.Label();
            this.txtReverse = new System.Windows.Forms.TextBox();
            this.lbReverse = new System.Windows.Forms.Label();
            this.lbStepIScale = new System.Windows.Forms.Label();
            this.txtStepI = new System.Windows.Forms.TextBox();
            this.lbStepI = new System.Windows.Forms.Label();
            this.lbStepVScale = new System.Windows.Forms.Label();
            this.txtStepV = new System.Windows.Forms.TextBox();
            this.lbStepV = new System.Windows.Forms.Label();
            this.gpSave.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            this.gpSense.SuspendLayout();
            this.gpControl.SuspendLayout();
            this.gpRange.SuspendLayout();
            this.gpOther.SuspendLayout();
            this.SuspendLayout();
            // 
            // gpSave
            // 
            this.gpSave.Controls.Add(this.btnSave);
            resources.ApplyResources(this.gpSave, "gpSave");
            this.gpSave.Name = "gpSave";
            this.gpSave.TabStop = false;
            // 
            // btnSave
            // 
            resources.ApplyResources(this.btnSave, "btnSave");
            this.btnSave.Name = "btnSave";
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // tableLayoutPanel1
            // 
            resources.ApplyResources(this.tableLayoutPanel1, "tableLayoutPanel1");
            this.tableLayoutPanel1.Controls.Add(this.gpSense, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.gpControl, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.gpRange, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.gpOther, 1, 1);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            // 
            // gpSense
            // 
            this.gpSense.Controls.Add(this.txtVoo);
            this.gpSense.Controls.Add(this.txtVio);
            this.gpSense.Controls.Add(this.lbVoo);
            this.gpSense.Controls.Add(this.lbkVI);
            this.gpSense.Controls.Add(this.lblInter2);
            this.gpSense.Controls.Add(this.lblVio);
            this.gpSense.Controls.Add(this.lblSlope2);
            this.gpSense.Controls.Add(this.lbVI_SEN);
            this.gpSense.Controls.Add(this.txtkVI);
            this.gpSense.Controls.Add(this.txtIy);
            this.gpSense.Controls.Add(this.txtK2);
            this.gpSense.Controls.Add(this.lbIy);
            this.gpSense.Controls.Add(this.lbK2);
            this.gpSense.Controls.Add(this.txtIx);
            this.gpSense.Controls.Add(this.txtK1);
            this.gpSense.Controls.Add(this.lbIx);
            this.gpSense.Controls.Add(this.lbK1);
            this.gpSense.Controls.Add(this.txtkVO);
            this.gpSense.Controls.Add(this.lbkVO);
            this.gpSense.Controls.Add(this.lbIO_SEN);
            this.gpSense.Controls.Add(this.lbII_SEN);
            this.gpSense.Controls.Add(this.lbVO_SEN);
            this.gpSense.Controls.Add(this.lbSense);
            resources.ApplyResources(this.gpSense, "gpSense");
            this.gpSense.Name = "gpSense";
            this.gpSense.TabStop = false;
            // 
            // txtVoo
            // 
            this.txtVoo.BackColor = System.Drawing.Color.White;
            this.txtVoo.ForeColor = System.Drawing.Color.Black;
            resources.ApplyResources(this.txtVoo, "txtVoo");
            this.txtVoo.Name = "txtVoo";
            this.txtVoo.TextChanged += new System.EventHandler(this.txtSenseCorrections_TextChanged);
            this.txtVoo.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textBox_KeyPress);
            // 
            // txtVio
            // 
            this.txtVio.BackColor = System.Drawing.Color.White;
            this.txtVio.ForeColor = System.Drawing.Color.Black;
            resources.ApplyResources(this.txtVio, "txtVio");
            this.txtVio.Name = "txtVio";
            this.txtVio.TextChanged += new System.EventHandler(this.txtSenseCorrections_TextChanged);
            this.txtVio.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textBox_KeyPress);
            // 
            // lbVoo
            // 
            resources.ApplyResources(this.lbVoo, "lbVoo");
            this.lbVoo.Name = "lbVoo";
            // 
            // lbkVI
            // 
            resources.ApplyResources(this.lbkVI, "lbkVI");
            this.lbkVI.Name = "lbkVI";
            // 
            // lblInter2
            // 
            resources.ApplyResources(this.lblInter2, "lblInter2");
            this.lblInter2.Name = "lblInter2";
            // 
            // lblVio
            // 
            resources.ApplyResources(this.lblVio, "lblVio");
            this.lblVio.Name = "lblVio";
            // 
            // lblSlope2
            // 
            resources.ApplyResources(this.lblSlope2, "lblSlope2");
            this.lblSlope2.Name = "lblSlope2";
            // 
            // lbVI_SEN
            // 
            resources.ApplyResources(this.lbVI_SEN, "lbVI_SEN");
            this.lbVI_SEN.Name = "lbVI_SEN";
            // 
            // txtkVI
            // 
            this.txtkVI.BackColor = System.Drawing.Color.White;
            this.txtkVI.ForeColor = System.Drawing.Color.Black;
            resources.ApplyResources(this.txtkVI, "txtkVI");
            this.txtkVI.Name = "txtkVI";
            this.txtkVI.TextChanged += new System.EventHandler(this.txtSenseCorrections_TextChanged);
            this.txtkVI.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textBox_KeyPress);
            // 
            // txtIy
            // 
            this.txtIy.BackColor = System.Drawing.Color.White;
            this.txtIy.ForeColor = System.Drawing.Color.Black;
            resources.ApplyResources(this.txtIy, "txtIy");
            this.txtIy.Name = "txtIy";
            this.txtIy.TextChanged += new System.EventHandler(this.txtSenseCorrections_TextChanged);
            this.txtIy.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textBox_KeyPress);
            // 
            // txtK2
            // 
            this.txtK2.BackColor = System.Drawing.Color.White;
            this.txtK2.ForeColor = System.Drawing.Color.Black;
            resources.ApplyResources(this.txtK2, "txtK2");
            this.txtK2.Name = "txtK2";
            this.txtK2.TextChanged += new System.EventHandler(this.txtSenseCorrections_TextChanged);
            this.txtK2.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textBox_KeyPress);
            // 
            // lbIy
            // 
            resources.ApplyResources(this.lbIy, "lbIy");
            this.lbIy.Name = "lbIy";
            // 
            // lbK2
            // 
            resources.ApplyResources(this.lbK2, "lbK2");
            this.lbK2.Name = "lbK2";
            // 
            // txtIx
            // 
            this.txtIx.BackColor = System.Drawing.Color.White;
            this.txtIx.ForeColor = System.Drawing.Color.Black;
            resources.ApplyResources(this.txtIx, "txtIx");
            this.txtIx.Name = "txtIx";
            this.txtIx.TextChanged += new System.EventHandler(this.txtSenseCorrections_TextChanged);
            this.txtIx.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textBox_KeyPress);
            // 
            // txtK1
            // 
            this.txtK1.BackColor = System.Drawing.Color.White;
            this.txtK1.ForeColor = System.Drawing.Color.Black;
            resources.ApplyResources(this.txtK1, "txtK1");
            this.txtK1.Name = "txtK1";
            this.txtK1.TextChanged += new System.EventHandler(this.txtSenseCorrections_TextChanged);
            this.txtK1.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textBox_KeyPress);
            // 
            // lbIx
            // 
            resources.ApplyResources(this.lbIx, "lbIx");
            this.lbIx.Name = "lbIx";
            // 
            // lbK1
            // 
            resources.ApplyResources(this.lbK1, "lbK1");
            this.lbK1.Name = "lbK1";
            // 
            // txtkVO
            // 
            this.txtkVO.BackColor = System.Drawing.Color.White;
            this.txtkVO.ForeColor = System.Drawing.Color.Black;
            resources.ApplyResources(this.txtkVO, "txtkVO");
            this.txtkVO.Name = "txtkVO";
            this.txtkVO.TextChanged += new System.EventHandler(this.txtSenseCorrections_TextChanged);
            this.txtkVO.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textBox_KeyPress);
            // 
            // lbkVO
            // 
            resources.ApplyResources(this.lbkVO, "lbkVO");
            this.lbkVO.Name = "lbkVO";
            // 
            // lbIO_SEN
            // 
            resources.ApplyResources(this.lbIO_SEN, "lbIO_SEN");
            this.lbIO_SEN.Name = "lbIO_SEN";
            // 
            // lbII_SEN
            // 
            resources.ApplyResources(this.lbII_SEN, "lbII_SEN");
            this.lbII_SEN.Name = "lbII_SEN";
            // 
            // lbVO_SEN
            // 
            resources.ApplyResources(this.lbVO_SEN, "lbVO_SEN");
            this.lbVO_SEN.Name = "lbVO_SEN";
            // 
            // lbSense
            // 
            resources.ApplyResources(this.lbSense, "lbSense");
            this.lbSense.Name = "lbSense";
            // 
            // gpControl
            // 
            this.gpControl.Controls.Add(this.lblInter1);
            this.gpControl.Controls.Add(this.txtVim);
            this.gpControl.Controls.Add(this.lbVim);
            this.gpControl.Controls.Add(this.lbII_CTRL);
            this.gpControl.Controls.Add(this.lbmvi);
            this.gpControl.Controls.Add(this.lbIO_CTRL);
            this.gpControl.Controls.Add(this.lbVI_CTRL);
            this.gpControl.Controls.Add(this.lbVO_CTRL);
            this.gpControl.Controls.Add(this.txtmvi);
            this.gpControl.Controls.Add(this.lbControl);
            this.gpControl.Controls.Add(this.lblSlope1);
            this.gpControl.Controls.Add(this.txtmio);
            this.gpControl.Controls.Add(this.txtIom);
            this.gpControl.Controls.Add(this.lbmio);
            this.gpControl.Controls.Add(this.lbIom);
            this.gpControl.Controls.Add(this.txtmii);
            this.gpControl.Controls.Add(this.txtIim);
            this.gpControl.Controls.Add(this.lbmii);
            this.gpControl.Controls.Add(this.lbIim);
            this.gpControl.Controls.Add(this.txtmvo);
            this.gpControl.Controls.Add(this.txtVom);
            this.gpControl.Controls.Add(this.lbmvo);
            this.gpControl.Controls.Add(this.lbVom);
            resources.ApplyResources(this.gpControl, "gpControl");
            this.gpControl.Name = "gpControl";
            this.gpControl.TabStop = false;
            // 
            // lblInter1
            // 
            resources.ApplyResources(this.lblInter1, "lblInter1");
            this.lblInter1.Name = "lblInter1";
            // 
            // txtVim
            // 
            this.txtVim.BackColor = System.Drawing.Color.White;
            this.txtVim.ForeColor = System.Drawing.Color.Black;
            resources.ApplyResources(this.txtVim, "txtVim");
            this.txtVim.Name = "txtVim";
            this.txtVim.TextChanged += new System.EventHandler(this.txtVim_TextChanged);
            this.txtVim.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textBox_KeyPress);
            // 
            // lbVim
            // 
            resources.ApplyResources(this.lbVim, "lbVim");
            this.lbVim.Name = "lbVim";
            // 
            // lbII_CTRL
            // 
            resources.ApplyResources(this.lbII_CTRL, "lbII_CTRL");
            this.lbII_CTRL.Name = "lbII_CTRL";
            // 
            // lbmvi
            // 
            resources.ApplyResources(this.lbmvi, "lbmvi");
            this.lbmvi.Name = "lbmvi";
            // 
            // lbIO_CTRL
            // 
            resources.ApplyResources(this.lbIO_CTRL, "lbIO_CTRL");
            this.lbIO_CTRL.Name = "lbIO_CTRL";
            // 
            // lbVI_CTRL
            // 
            resources.ApplyResources(this.lbVI_CTRL, "lbVI_CTRL");
            this.lbVI_CTRL.Name = "lbVI_CTRL";
            // 
            // lbVO_CTRL
            // 
            resources.ApplyResources(this.lbVO_CTRL, "lbVO_CTRL");
            this.lbVO_CTRL.Name = "lbVO_CTRL";
            // 
            // txtmvi
            // 
            this.txtmvi.BackColor = System.Drawing.Color.White;
            this.txtmvi.ForeColor = System.Drawing.Color.Black;
            resources.ApplyResources(this.txtmvi, "txtmvi");
            this.txtmvi.Name = "txtmvi";
            this.txtmvi.TextChanged += new System.EventHandler(this.txtmvi_TextChanged);
            this.txtmvi.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textBox_KeyPress);
            // 
            // lbControl
            // 
            resources.ApplyResources(this.lbControl, "lbControl");
            this.lbControl.Name = "lbControl";
            // 
            // lblSlope1
            // 
            resources.ApplyResources(this.lblSlope1, "lblSlope1");
            this.lblSlope1.Name = "lblSlope1";
            // 
            // txtmio
            // 
            this.txtmio.BackColor = System.Drawing.Color.White;
            this.txtmio.ForeColor = System.Drawing.Color.Black;
            resources.ApplyResources(this.txtmio, "txtmio");
            this.txtmio.Name = "txtmio";
            this.txtmio.TextChanged += new System.EventHandler(this.txtmio_TextChanged);
            this.txtmio.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textBox_KeyPress);
            // 
            // txtIom
            // 
            this.txtIom.BackColor = System.Drawing.Color.White;
            this.txtIom.ForeColor = System.Drawing.Color.Black;
            resources.ApplyResources(this.txtIom, "txtIom");
            this.txtIom.Name = "txtIom";
            this.txtIom.TextChanged += new System.EventHandler(this.txtIom_TextChanged);
            this.txtIom.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textBox_KeyPress);
            // 
            // lbmio
            // 
            resources.ApplyResources(this.lbmio, "lbmio");
            this.lbmio.Name = "lbmio";
            // 
            // lbIom
            // 
            resources.ApplyResources(this.lbIom, "lbIom");
            this.lbIom.Name = "lbIom";
            // 
            // txtmii
            // 
            this.txtmii.BackColor = System.Drawing.Color.White;
            this.txtmii.ForeColor = System.Drawing.Color.Black;
            resources.ApplyResources(this.txtmii, "txtmii");
            this.txtmii.Name = "txtmii";
            this.txtmii.TextChanged += new System.EventHandler(this.txtmii_TextChanged);
            this.txtmii.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textBox_KeyPress);
            // 
            // txtIim
            // 
            this.txtIim.BackColor = System.Drawing.Color.White;
            this.txtIim.ForeColor = System.Drawing.Color.Black;
            resources.ApplyResources(this.txtIim, "txtIim");
            this.txtIim.Name = "txtIim";
            this.txtIim.TextChanged += new System.EventHandler(this.txtIim_TextChanged);
            this.txtIim.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textBox_KeyPress);
            // 
            // lbmii
            // 
            resources.ApplyResources(this.lbmii, "lbmii");
            this.lbmii.Name = "lbmii";
            // 
            // lbIim
            // 
            resources.ApplyResources(this.lbIim, "lbIim");
            this.lbIim.Name = "lbIim";
            // 
            // txtmvo
            // 
            this.txtmvo.BackColor = System.Drawing.Color.White;
            this.txtmvo.ForeColor = System.Drawing.Color.Black;
            resources.ApplyResources(this.txtmvo, "txtmvo");
            this.txtmvo.Name = "txtmvo";
            this.txtmvo.TextChanged += new System.EventHandler(this.txtmvo_TextChanged);
            this.txtmvo.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textBox_KeyPress);
            // 
            // txtVom
            // 
            this.txtVom.BackColor = System.Drawing.Color.White;
            this.txtVom.ForeColor = System.Drawing.Color.Black;
            resources.ApplyResources(this.txtVom, "txtVom");
            this.txtVom.Name = "txtVom";
            this.txtVom.TextChanged += new System.EventHandler(this.txtVom_TextChanged);
            this.txtVom.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textBox_KeyPress);
            // 
            // lbmvo
            // 
            resources.ApplyResources(this.lbmvo, "lbmvo");
            this.lbmvo.Name = "lbmvo";
            // 
            // lbVom
            // 
            resources.ApplyResources(this.lbVom, "lbVom");
            this.lbVom.Name = "lbVom";
            // 
            // gpRange
            // 
            this.gpRange.Controls.Add(this.lbIinScale);
            this.gpRange.Controls.Add(this.lbVinScale);
            this.gpRange.Controls.Add(this.lbIoutScale);
            this.gpRange.Controls.Add(this.lblVoutScale);
            this.gpRange.Controls.Add(this.lbIin);
            this.gpRange.Controls.Add(this.lbVin);
            this.gpRange.Controls.Add(this.lbIout);
            this.gpRange.Controls.Add(this.lbVout);
            this.gpRange.Controls.Add(this.txtVinMax);
            this.gpRange.Controls.Add(this.txtVinMin);
            this.gpRange.Controls.Add(this.lbVinMax);
            this.gpRange.Controls.Add(this.lbVinMin);
            this.gpRange.Controls.Add(this.txtIinMax);
            this.gpRange.Controls.Add(this.txtIinMin);
            this.gpRange.Controls.Add(this.lbIinMax);
            this.gpRange.Controls.Add(this.lbIinMin);
            this.gpRange.Controls.Add(this.txtIoutMax);
            this.gpRange.Controls.Add(this.txtIoutMin);
            this.gpRange.Controls.Add(this.lbIoutMax);
            this.gpRange.Controls.Add(this.lbIoutMin);
            this.gpRange.Controls.Add(this.txtVoutMax);
            this.gpRange.Controls.Add(this.txtVoutMin);
            this.gpRange.Controls.Add(this.lbVoutMax);
            this.gpRange.Controls.Add(this.lbVoutMin);
            resources.ApplyResources(this.gpRange, "gpRange");
            this.gpRange.Name = "gpRange";
            this.gpRange.TabStop = false;
            // 
            // lbIinScale
            // 
            resources.ApplyResources(this.lbIinScale, "lbIinScale");
            this.lbIinScale.Name = "lbIinScale";
            // 
            // lbVinScale
            // 
            resources.ApplyResources(this.lbVinScale, "lbVinScale");
            this.lbVinScale.Name = "lbVinScale";
            // 
            // lbIoutScale
            // 
            resources.ApplyResources(this.lbIoutScale, "lbIoutScale");
            this.lbIoutScale.Name = "lbIoutScale";
            // 
            // lblVoutScale
            // 
            resources.ApplyResources(this.lblVoutScale, "lblVoutScale");
            this.lblVoutScale.Name = "lblVoutScale";
            // 
            // lbIin
            // 
            resources.ApplyResources(this.lbIin, "lbIin");
            this.lbIin.Name = "lbIin";
            // 
            // lbVin
            // 
            resources.ApplyResources(this.lbVin, "lbVin");
            this.lbVin.Name = "lbVin";
            // 
            // lbIout
            // 
            resources.ApplyResources(this.lbIout, "lbIout");
            this.lbIout.Name = "lbIout";
            // 
            // lbVout
            // 
            resources.ApplyResources(this.lbVout, "lbVout");
            this.lbVout.Name = "lbVout";
            // 
            // txtVinMax
            // 
            this.txtVinMax.BackColor = System.Drawing.Color.White;
            this.txtVinMax.ForeColor = System.Drawing.Color.Black;
            resources.ApplyResources(this.txtVinMax, "txtVinMax");
            this.txtVinMax.Name = "txtVinMax";
            this.txtVinMax.TextChanged += new System.EventHandler(this.txtVinMax_TextChanged);
            this.txtVinMax.DoubleClick += new System.EventHandler(this.txtVin_DoubleClick);
            this.txtVinMax.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textBox_KeyPress);
            // 
            // txtVinMin
            // 
            this.txtVinMin.BackColor = System.Drawing.Color.White;
            this.txtVinMin.ForeColor = System.Drawing.Color.Black;
            resources.ApplyResources(this.txtVinMin, "txtVinMin");
            this.txtVinMin.Name = "txtVinMin";
            this.txtVinMin.TextChanged += new System.EventHandler(this.txtVinMin_TextChanged);
            this.txtVinMin.DoubleClick += new System.EventHandler(this.txtVin_DoubleClick);
            this.txtVinMin.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textBox_KeyPress);
            // 
            // lbVinMax
            // 
            resources.ApplyResources(this.lbVinMax, "lbVinMax");
            this.lbVinMax.Name = "lbVinMax";
            // 
            // lbVinMin
            // 
            resources.ApplyResources(this.lbVinMin, "lbVinMin");
            this.lbVinMin.Name = "lbVinMin";
            // 
            // txtIinMax
            // 
            this.txtIinMax.BackColor = System.Drawing.Color.White;
            this.txtIinMax.ForeColor = System.Drawing.Color.Black;
            resources.ApplyResources(this.txtIinMax, "txtIinMax");
            this.txtIinMax.Name = "txtIinMax";
            this.txtIinMax.TextChanged += new System.EventHandler(this.txtIinMax_TextChanged);
            this.txtIinMax.DoubleClick += new System.EventHandler(this.txtIinMax_DoubleClick);
            this.txtIinMax.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textBox_KeyPress);
            // 
            // txtIinMin
            // 
            this.txtIinMin.BackColor = System.Drawing.Color.White;
            this.txtIinMin.ForeColor = System.Drawing.Color.Black;
            resources.ApplyResources(this.txtIinMin, "txtIinMin");
            this.txtIinMin.Name = "txtIinMin";
            this.txtIinMin.TextChanged += new System.EventHandler(this.txtIinMin_TextChanged);
            this.txtIinMin.DoubleClick += new System.EventHandler(this.txtIinMin_DoubleClick);
            this.txtIinMin.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textBox_KeyPress);
            // 
            // lbIinMax
            // 
            resources.ApplyResources(this.lbIinMax, "lbIinMax");
            this.lbIinMax.Name = "lbIinMax";
            // 
            // lbIinMin
            // 
            resources.ApplyResources(this.lbIinMin, "lbIinMin");
            this.lbIinMin.Name = "lbIinMin";
            // 
            // txtIoutMax
            // 
            this.txtIoutMax.BackColor = System.Drawing.Color.White;
            this.txtIoutMax.ForeColor = System.Drawing.Color.Black;
            resources.ApplyResources(this.txtIoutMax, "txtIoutMax");
            this.txtIoutMax.Name = "txtIoutMax";
            this.txtIoutMax.TextChanged += new System.EventHandler(this.txtIoutMax_TextChanged);
            this.txtIoutMax.DoubleClick += new System.EventHandler(this.txtIoutMax_DoubleClick);
            this.txtIoutMax.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textBox_KeyPress);
            // 
            // txtIoutMin
            // 
            this.txtIoutMin.BackColor = System.Drawing.Color.White;
            this.txtIoutMin.ForeColor = System.Drawing.Color.Black;
            resources.ApplyResources(this.txtIoutMin, "txtIoutMin");
            this.txtIoutMin.Name = "txtIoutMin";
            this.txtIoutMin.TextChanged += new System.EventHandler(this.txtIoutMin_TextChanged);
            this.txtIoutMin.DoubleClick += new System.EventHandler(this.txtIoutMin_DoubleClick);
            this.txtIoutMin.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textBox_KeyPress);
            // 
            // lbIoutMax
            // 
            resources.ApplyResources(this.lbIoutMax, "lbIoutMax");
            this.lbIoutMax.Name = "lbIoutMax";
            // 
            // lbIoutMin
            // 
            resources.ApplyResources(this.lbIoutMin, "lbIoutMin");
            this.lbIoutMin.Name = "lbIoutMin";
            // 
            // txtVoutMax
            // 
            this.txtVoutMax.BackColor = System.Drawing.Color.White;
            this.txtVoutMax.ForeColor = System.Drawing.Color.Black;
            resources.ApplyResources(this.txtVoutMax, "txtVoutMax");
            this.txtVoutMax.Name = "txtVoutMax";
            this.txtVoutMax.TextChanged += new System.EventHandler(this.txtVoutMax_TextChanged);
            this.txtVoutMax.DoubleClick += new System.EventHandler(this.txtVout_DoubleClick);
            this.txtVoutMax.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textBox_KeyPress);
            // 
            // txtVoutMin
            // 
            this.txtVoutMin.BackColor = System.Drawing.Color.White;
            this.txtVoutMin.ForeColor = System.Drawing.Color.Black;
            resources.ApplyResources(this.txtVoutMin, "txtVoutMin");
            this.txtVoutMin.Name = "txtVoutMin";
            this.txtVoutMin.TextChanged += new System.EventHandler(this.txtVoutMin_TextChanged);
            this.txtVoutMin.DoubleClick += new System.EventHandler(this.txtVout_DoubleClick);
            this.txtVoutMin.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textBox_KeyPress);
            // 
            // lbVoutMax
            // 
            resources.ApplyResources(this.lbVoutMax, "lbVoutMax");
            this.lbVoutMax.Name = "lbVoutMax";
            // 
            // lbVoutMin
            // 
            resources.ApplyResources(this.lbVoutMin, "lbVoutMin");
            this.lbVoutMin.Name = "lbVoutMin";
            // 
            // gpOther
            // 
            this.gpOther.Controls.Add(this.lbPWMrange);
            this.gpOther.Controls.Add(this.txtPWM);
            this.gpOther.Controls.Add(this.lbPWM);
            this.gpOther.Controls.Add(this.lbReverseScale);
            this.gpOther.Controls.Add(this.txtReverse);
            this.gpOther.Controls.Add(this.lbReverse);
            this.gpOther.Controls.Add(this.lbStepIScale);
            this.gpOther.Controls.Add(this.txtStepI);
            this.gpOther.Controls.Add(this.lbStepI);
            this.gpOther.Controls.Add(this.lbStepVScale);
            this.gpOther.Controls.Add(this.txtStepV);
            this.gpOther.Controls.Add(this.lbStepV);
            resources.ApplyResources(this.gpOther, "gpOther");
            this.gpOther.Name = "gpOther";
            this.gpOther.TabStop = false;
            // 
            // lbPWMrange
            // 
            resources.ApplyResources(this.lbPWMrange, "lbPWMrange");
            this.lbPWMrange.Name = "lbPWMrange";
            // 
            // txtPWM
            // 
            this.txtPWM.BackColor = System.Drawing.Color.White;
            this.txtPWM.ForeColor = System.Drawing.Color.Black;
            resources.ApplyResources(this.txtPWM, "txtPWM");
            this.txtPWM.Name = "txtPWM";
            this.txtPWM.TextChanged += new System.EventHandler(this.txtPWM_TextChanged);
            this.txtPWM.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtPWM_KeyPress);
            // 
            // lbPWM
            // 
            resources.ApplyResources(this.lbPWM, "lbPWM");
            this.lbPWM.Name = "lbPWM";
            // 
            // lbReverseScale
            // 
            resources.ApplyResources(this.lbReverseScale, "lbReverseScale");
            this.lbReverseScale.Name = "lbReverseScale";
            // 
            // txtReverse
            // 
            this.txtReverse.BackColor = System.Drawing.Color.White;
            this.txtReverse.ForeColor = System.Drawing.Color.Black;
            resources.ApplyResources(this.txtReverse, "txtReverse");
            this.txtReverse.Name = "txtReverse";
            this.txtReverse.TextChanged += new System.EventHandler(this.textBox_TextChanged);
            this.txtReverse.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textBox_KeyPress);
            // 
            // lbReverse
            // 
            resources.ApplyResources(this.lbReverse, "lbReverse");
            this.lbReverse.Name = "lbReverse";
            // 
            // lbStepIScale
            // 
            resources.ApplyResources(this.lbStepIScale, "lbStepIScale");
            this.lbStepIScale.Name = "lbStepIScale";
            // 
            // txtStepI
            // 
            this.txtStepI.BackColor = System.Drawing.Color.White;
            this.txtStepI.ForeColor = System.Drawing.Color.Black;
            resources.ApplyResources(this.txtStepI, "txtStepI");
            this.txtStepI.Name = "txtStepI";
            this.txtStepI.TextChanged += new System.EventHandler(this.textBox_TextChanged);
            this.txtStepI.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textBox_KeyPress);
            // 
            // lbStepI
            // 
            resources.ApplyResources(this.lbStepI, "lbStepI");
            this.lbStepI.Name = "lbStepI";
            // 
            // lbStepVScale
            // 
            resources.ApplyResources(this.lbStepVScale, "lbStepVScale");
            this.lbStepVScale.Name = "lbStepVScale";
            // 
            // txtStepV
            // 
            this.txtStepV.BackColor = System.Drawing.Color.White;
            this.txtStepV.ForeColor = System.Drawing.Color.Black;
            resources.ApplyResources(this.txtStepV, "txtStepV");
            this.txtStepV.Name = "txtStepV";
            this.txtStepV.TextChanged += new System.EventHandler(this.textBox_TextChanged);
            this.txtStepV.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textBox_KeyPress);
            // 
            // lbStepV
            // 
            resources.ApplyResources(this.lbStepV, "lbStepV");
            this.lbStepV.Name = "lbStepV";
            // 
            // Configuration
            // 
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.tableLayoutPanel1);
            this.Controls.Add(this.gpSave);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Name = "Configuration";
            this.Load += new System.EventHandler(this.Setting_Load);
            this.gpSave.ResumeLayout(false);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.gpSense.ResumeLayout(false);
            this.gpSense.PerformLayout();
            this.gpControl.ResumeLayout(false);
            this.gpControl.PerformLayout();
            this.gpRange.ResumeLayout(false);
            this.gpRange.PerformLayout();
            this.gpOther.ResumeLayout(false);
            this.gpOther.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox gpSave;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.GroupBox gpSense;
        private System.Windows.Forms.TextBox txtIy;
        private System.Windows.Forms.TextBox txtK2;
        private System.Windows.Forms.Label lbIy;
        private System.Windows.Forms.Label lbK2;
        private System.Windows.Forms.TextBox txtIx;
        private System.Windows.Forms.TextBox txtK1;
        private System.Windows.Forms.Label lbIx;
        private System.Windows.Forms.Label lbK1;
        private System.Windows.Forms.TextBox txtkVO;
        private System.Windows.Forms.Label lbkVO;
        private System.Windows.Forms.TextBox txtkVI;
        private System.Windows.Forms.Label lbIO_SEN;
        private System.Windows.Forms.Label lbII_SEN;
        private System.Windows.Forms.Label lbVO_SEN;
        private System.Windows.Forms.Label lbVI_SEN;
        private System.Windows.Forms.Label lbSense;
        private System.Windows.Forms.Label lbkVI;
        private System.Windows.Forms.GroupBox gpControl;
        private System.Windows.Forms.TextBox txtmio;
        private System.Windows.Forms.TextBox txtIom;
        private System.Windows.Forms.Label lbmio;
        private System.Windows.Forms.Label lbIom;
        private System.Windows.Forms.TextBox txtmii;
        private System.Windows.Forms.TextBox txtIim;
        private System.Windows.Forms.Label lbmii;
        private System.Windows.Forms.Label lbIim;
        private System.Windows.Forms.TextBox txtmvo;
        private System.Windows.Forms.TextBox txtVom;
        private System.Windows.Forms.Label lbmvo;
        private System.Windows.Forms.Label lbVom;
        private System.Windows.Forms.TextBox txtmvi;
        private System.Windows.Forms.TextBox txtVim;
        private System.Windows.Forms.Label lbmvi;
        private System.Windows.Forms.Label lbVim;
        private System.Windows.Forms.GroupBox gpRange;
        private System.Windows.Forms.GroupBox gpOther;
        private System.Windows.Forms.Label lbIinScale;
        private System.Windows.Forms.Label lbVinScale;
        private System.Windows.Forms.Label lbIoutScale;
        private System.Windows.Forms.Label lblVoutScale;
        private System.Windows.Forms.Label lbIin;
        private System.Windows.Forms.Label lbVin;
        private System.Windows.Forms.Label lbIout;
        private System.Windows.Forms.Label lbVout;
        private System.Windows.Forms.TextBox txtVinMax;
        private System.Windows.Forms.TextBox txtVinMin;
        private System.Windows.Forms.Label lbVinMax;
        private System.Windows.Forms.Label lbVinMin;
        private System.Windows.Forms.TextBox txtIinMax;
        private System.Windows.Forms.TextBox txtIinMin;
        private System.Windows.Forms.Label lbIinMax;
        private System.Windows.Forms.Label lbIinMin;
        private System.Windows.Forms.TextBox txtIoutMax;
        private System.Windows.Forms.TextBox txtIoutMin;
        private System.Windows.Forms.Label lbIoutMax;
        private System.Windows.Forms.Label lbIoutMin;
        private System.Windows.Forms.TextBox txtVoutMax;
        private System.Windows.Forms.TextBox txtVoutMin;
        private System.Windows.Forms.Label lbVoutMax;
        private System.Windows.Forms.Label lbVoutMin;
        private System.Windows.Forms.Label lbReverseScale;
        private System.Windows.Forms.TextBox txtReverse;
        private System.Windows.Forms.Label lbReverse;
        private System.Windows.Forms.Label lbStepIScale;
        private System.Windows.Forms.TextBox txtStepI;
        private System.Windows.Forms.Label lbStepI;
        private System.Windows.Forms.Label lbStepVScale;
        private System.Windows.Forms.TextBox txtStepV;
        private System.Windows.Forms.Label lbStepV;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.Label lblSlope1;
        private System.Windows.Forms.Label lblSlope2;
        private System.Windows.Forms.Label lblInter1;
        private System.Windows.Forms.Label lbII_CTRL;
        private System.Windows.Forms.Label lbIO_CTRL;
        private System.Windows.Forms.Label lbVO_CTRL;
        private System.Windows.Forms.Label lbVI_CTRL;
        private System.Windows.Forms.Label lbControl;
        private System.Windows.Forms.Label lblInter2;
        private System.Windows.Forms.TextBox txtVoo;
        private System.Windows.Forms.Label lbVoo;
        private System.Windows.Forms.TextBox txtVio;
        private System.Windows.Forms.Label lblVio;
        private System.Windows.Forms.Label lbPWM;
        private System.Windows.Forms.TextBox txtPWM;
        private System.Windows.Forms.Label lbPWMrange;



    }
}