﻿namespace DEMO_ISL81X01
{
    partial class Demo
    {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Demo));
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea2 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend2 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series5 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.Series series6 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.Series series7 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.Series series8 = new System.Windows.Forms.DataVisualization.Charting.Series();
            this.gpConnect = new System.Windows.Forms.GroupBox();
            this.btnCfg = new VistaButtonTest.VistaButton();
            this.picLogo = new System.Windows.Forms.PictureBox();
            this.btnConnect = new VistaButtonTest.VistaButton();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.toolStripStatusLabel1 = new System.Windows.Forms.ToolStripStatusLabel();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.gpSetting = new System.Windows.Forms.GroupBox();
            this.txtVoutIns = new System.Windows.Forms.TextBox();
            this.lbDither = new System.Windows.Forms.Label();
            this.txtIoutIns = new System.Windows.Forms.TextBox();
            this.txtVinIns = new System.Windows.Forms.TextBox();
            this.txtIinIns = new System.Windows.Forms.TextBox();
            this.chkPWMduty = new System.Windows.Forms.CheckBox();
            this.lbIiVolSca = new System.Windows.Forms.Label();
            this.lbViVolSca = new System.Windows.Forms.Label();
            this.lbIoVolSca = new System.Windows.Forms.Label();
            this.lbVoVolSca = new System.Windows.Forms.Label();
            this.lbIiPerSca = new System.Windows.Forms.Label();
            this.lbViPerSca = new System.Windows.Forms.Label();
            this.lbIoPerSca = new System.Windows.Forms.Label();
            this.lbVoPerSca = new System.Windows.Forms.Label();
            this.txtIinSet = new System.Windows.Forms.TextBox();
            this.txtVinSet = new System.Windows.Forms.TextBox();
            this.txtIoutSet = new System.Windows.Forms.TextBox();
            this.txtVoutSet = new System.Windows.Forms.TextBox();
            this.lblVinSetScale = new System.Windows.Forms.Label();
            this.txtIoutDuty = new System.Windows.Forms.TextBox();
            this.txtVoutDuty = new System.Windows.Forms.TextBox();
            this.txtIoutVol = new System.Windows.Forms.TextBox();
            this.txtVoutVol = new System.Windows.Forms.TextBox();
            this.lbPWMvol = new System.Windows.Forms.Label();
            this.lbPWMduty = new System.Windows.Forms.Label();
            this.lbVoutPWM = new System.Windows.Forms.Label();
            this.lblVoutSet = new System.Windows.Forms.Label();
            this.lbVinPWM = new System.Windows.Forms.Label();
            this.lblVinSet = new System.Windows.Forms.Label();
            this.lbIoutPWM = new System.Windows.Forms.Label();
            this.lblIoutSet = new System.Windows.Forms.Label();
            this.lblVoutSetScale = new System.Windows.Forms.Label();
            this.lbIinPWM = new System.Windows.Forms.Label();
            this.lblIinSet = new System.Windows.Forms.Label();
            this.chkReverse = new System.Windows.Forms.CheckBox();
            this.btnVoutDe = new System.Windows.Forms.Button();
            this.btnVinDe = new System.Windows.Forms.Button();
            this.btnIoutDe = new System.Windows.Forms.Button();
            this.btnIinDe = new System.Windows.Forms.Button();
            this.lblIinSetScale = new System.Windows.Forms.Label();
            this.btnVoutIn = new System.Windows.Forms.Button();
            this.btnVinIn = new System.Windows.Forms.Button();
            this.lblIoutSetScale = new System.Windows.Forms.Label();
            this.btnIoutIn = new System.Windows.Forms.Button();
            this.btnIinIn = new System.Windows.Forms.Button();
            this.btnON = new VistaButtonTest.VistaButton();
            this.btnOFF = new VistaButtonTest.VistaButton();
            this.btnDitherEn = new VistaButtonTest.VistaButton();
            this.btnDitherDis = new VistaButtonTest.VistaButton();
            this.txtVinVol = new System.Windows.Forms.TextBox();
            this.txtIinVol = new System.Windows.Forms.TextBox();
            this.txtVinDuty = new System.Windows.Forms.TextBox();
            this.txtIinDuty = new System.Windows.Forms.TextBox();
            this.gpStatus = new System.Windows.Forms.GroupBox();
            this.lblTempActualScale = new System.Windows.Forms.Label();
            this.lblVinActualScale = new System.Windows.Forms.Label();
            this.lblIinActualScale = new System.Windows.Forms.Label();
            this.lblVoutActualScale = new System.Windows.Forms.Label();
            this.lblIoutActualScale = new System.Windows.Forms.Label();
            this.txtTemp = new System.Windows.Forms.TextBox();
            this.txtIin = new System.Windows.Forms.TextBox();
            this.txtVin = new System.Windows.Forms.TextBox();
            this.txtIout = new System.Windows.Forms.TextBox();
            this.txtVout = new System.Windows.Forms.TextBox();
            this.picVinSta = new System.Windows.Forms.PictureBox();
            this.lblMode = new System.Windows.Forms.Label();
            this.picSysDirection = new System.Windows.Forms.PictureBox();
            this.lblModeSta = new System.Windows.Forms.Label();
            this.picVoutSta = new System.Windows.Forms.PictureBox();
            this.lblTempSta = new System.Windows.Forms.Label();
            this.lblPGSta = new System.Windows.Forms.Label();
            this.picIoutSta = new System.Windows.Forms.PictureBox();
            this.picPGSta = new System.Windows.Forms.PictureBox();
            this.lblIoutSta = new System.Windows.Forms.Label();
            this.lblVoutSta = new System.Windows.Forms.Label();
            this.picIinSta = new System.Windows.Forms.PictureBox();
            this.picTempSta = new System.Windows.Forms.PictureBox();
            this.lblVinSta = new System.Windows.Forms.Label();
            this.lblIinSta = new System.Windows.Forms.Label();
            this.gpMonitor = new System.Windows.Forms.GroupBox();
            this.rdoIn = new System.Windows.Forms.RadioButton();
            this.rdoOut = new System.Windows.Forms.RadioButton();
            this.lblUnitA = new System.Windows.Forms.Label();
            this.lblUintV = new System.Windows.Forms.Label();
            this.chtMonitor = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.serialPort1 = new System.IO.Ports.SerialPort(this.components);
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.dataSet1 = new System.Data.DataSet();
            this.timer2 = new System.Windows.Forms.Timer(this.components);
            this.gpConnect.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picLogo)).BeginInit();
            this.statusStrip1.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            this.gpSetting.SuspendLayout();
            this.gpStatus.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picVinSta)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picSysDirection)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picVoutSta)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picIoutSta)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picPGSta)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picIinSta)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picTempSta)).BeginInit();
            this.gpMonitor.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chtMonitor)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet1)).BeginInit();
            this.SuspendLayout();
            // 
            // gpConnect
            // 
            this.gpConnect.BackColor = System.Drawing.Color.Transparent;
            this.gpConnect.BackgroundImage = global::DEMO_ISL81X01.Properties.Resources.backgroud2;
            this.gpConnect.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.gpConnect.Controls.Add(this.btnCfg);
            this.gpConnect.Controls.Add(this.picLogo);
            this.gpConnect.Controls.Add(this.btnConnect);
            this.gpConnect.Dock = System.Windows.Forms.DockStyle.Top;
            this.gpConnect.Location = new System.Drawing.Point(0, 0);
            this.gpConnect.Margin = new System.Windows.Forms.Padding(4);
            this.gpConnect.Name = "gpConnect";
            this.gpConnect.Padding = new System.Windows.Forms.Padding(4);
            this.gpConnect.Size = new System.Drawing.Size(1274, 100);
            this.gpConnect.TabIndex = 0;
            this.gpConnect.TabStop = false;
            // 
            // btnCfg
            // 
            this.btnCfg.BackColor = System.Drawing.Color.Transparent;
            this.btnCfg.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnCfg.BackgroundImage")));
            this.btnCfg.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.btnCfg.BaseColor = System.Drawing.Color.Transparent;
            this.btnCfg.ButtonColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(211)))), ((int)(((byte)(40)))));
            this.btnCfg.ButtonText = "CONFIGURATION";
            this.btnCfg.CornerRadius = 20;
            this.btnCfg.Font = new System.Drawing.Font("Arial", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCfg.ForeColor = System.Drawing.Color.Black;
            this.btnCfg.GlowColor = System.Drawing.Color.Lime;
            this.btnCfg.Location = new System.Drawing.Point(298, 20);
            this.btnCfg.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnCfg.Name = "btnCfg";
            this.btnCfg.Size = new System.Drawing.Size(224, 70);
            this.btnCfg.TabIndex = 156;
            this.btnCfg.Click += new System.EventHandler(this.btnCfg_Click);
            // 
            // picLogo
            // 
            this.picLogo.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.picLogo.BackColor = System.Drawing.Color.Transparent;
            this.picLogo.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("picLogo.BackgroundImage")));
            this.picLogo.InitialImage = null;
            this.picLogo.Location = new System.Drawing.Point(1032, 20);
            this.picLogo.Name = "picLogo";
            this.picLogo.Size = new System.Drawing.Size(230, 63);
            this.picLogo.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.picLogo.TabIndex = 155;
            this.picLogo.TabStop = false;
            // 
            // btnConnect
            // 
            this.btnConnect.BackColor = System.Drawing.Color.Transparent;
            this.btnConnect.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnConnect.BackgroundImage")));
            this.btnConnect.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.btnConnect.BaseColor = System.Drawing.Color.Transparent;
            this.btnConnect.ButtonColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(211)))), ((int)(((byte)(40)))));
            this.btnConnect.ButtonText = "CONNECT";
            this.btnConnect.CornerRadius = 20;
            this.btnConnect.Font = new System.Drawing.Font("Arial", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnConnect.ForeColor = System.Drawing.Color.Black;
            this.btnConnect.GlowColor = System.Drawing.Color.Lime;
            this.btnConnect.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btnConnect.Location = new System.Drawing.Point(22, 20);
            this.btnConnect.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnConnect.Name = "btnConnect";
            this.btnConnect.Size = new System.Drawing.Size(224, 70);
            this.btnConnect.TabIndex = 125;
            this.btnConnect.Click += new System.EventHandler(this.btnConnect_Click);
            // 
            // statusStrip1
            // 
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripStatusLabel1});
            this.statusStrip1.Location = new System.Drawing.Point(0, 669);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Padding = new System.Windows.Forms.Padding(1, 0, 19, 0);
            this.statusStrip1.Size = new System.Drawing.Size(1274, 22);
            this.statusStrip1.TabIndex = 1;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // toolStripStatusLabel1
            // 
            this.toolStripStatusLabel1.ForeColor = System.Drawing.Color.Red;
            this.toolStripStatusLabel1.Name = "toolStripStatusLabel1";
            this.toolStripStatusLabel1.Size = new System.Drawing.Size(0, 17);
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.BackColor = System.Drawing.Color.Transparent;
            this.tableLayoutPanel1.ColumnCount = 3;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 280F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 350F));
            this.tableLayoutPanel1.Controls.Add(this.gpSetting, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.gpStatus, 2, 0);
            this.tableLayoutPanel1.Controls.Add(this.gpMonitor, 1, 0);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 100);
            this.tableLayoutPanel1.Margin = new System.Windows.Forms.Padding(4);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 1;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(1274, 569);
            this.tableLayoutPanel1.TabIndex = 2;
            // 
            // gpSetting
            // 
            this.gpSetting.Controls.Add(this.txtVoutIns);
            this.gpSetting.Controls.Add(this.lbDither);
            this.gpSetting.Controls.Add(this.txtIoutIns);
            this.gpSetting.Controls.Add(this.txtVinIns);
            this.gpSetting.Controls.Add(this.txtIinIns);
            this.gpSetting.Controls.Add(this.chkPWMduty);
            this.gpSetting.Controls.Add(this.lbIiVolSca);
            this.gpSetting.Controls.Add(this.lbViVolSca);
            this.gpSetting.Controls.Add(this.lbIoVolSca);
            this.gpSetting.Controls.Add(this.lbVoVolSca);
            this.gpSetting.Controls.Add(this.lbIiPerSca);
            this.gpSetting.Controls.Add(this.lbViPerSca);
            this.gpSetting.Controls.Add(this.lbIoPerSca);
            this.gpSetting.Controls.Add(this.lbVoPerSca);
            this.gpSetting.Controls.Add(this.txtIinSet);
            this.gpSetting.Controls.Add(this.txtVinSet);
            this.gpSetting.Controls.Add(this.txtIoutSet);
            this.gpSetting.Controls.Add(this.txtVoutSet);
            this.gpSetting.Controls.Add(this.lblVinSetScale);
            this.gpSetting.Controls.Add(this.txtIoutDuty);
            this.gpSetting.Controls.Add(this.txtVoutDuty);
            this.gpSetting.Controls.Add(this.txtIoutVol);
            this.gpSetting.Controls.Add(this.txtVoutVol);
            this.gpSetting.Controls.Add(this.lbPWMvol);
            this.gpSetting.Controls.Add(this.lbPWMduty);
            this.gpSetting.Controls.Add(this.lbVoutPWM);
            this.gpSetting.Controls.Add(this.lblVoutSet);
            this.gpSetting.Controls.Add(this.lbVinPWM);
            this.gpSetting.Controls.Add(this.lblVinSet);
            this.gpSetting.Controls.Add(this.lbIoutPWM);
            this.gpSetting.Controls.Add(this.lblIoutSet);
            this.gpSetting.Controls.Add(this.lblVoutSetScale);
            this.gpSetting.Controls.Add(this.lbIinPWM);
            this.gpSetting.Controls.Add(this.lblIinSet);
            this.gpSetting.Controls.Add(this.chkReverse);
            this.gpSetting.Controls.Add(this.btnVoutDe);
            this.gpSetting.Controls.Add(this.btnVinDe);
            this.gpSetting.Controls.Add(this.btnIoutDe);
            this.gpSetting.Controls.Add(this.btnIinDe);
            this.gpSetting.Controls.Add(this.lblIinSetScale);
            this.gpSetting.Controls.Add(this.btnVoutIn);
            this.gpSetting.Controls.Add(this.btnVinIn);
            this.gpSetting.Controls.Add(this.lblIoutSetScale);
            this.gpSetting.Controls.Add(this.btnIoutIn);
            this.gpSetting.Controls.Add(this.btnIinIn);
            this.gpSetting.Controls.Add(this.btnON);
            this.gpSetting.Controls.Add(this.btnOFF);
            this.gpSetting.Controls.Add(this.btnDitherEn);
            this.gpSetting.Controls.Add(this.btnDitherDis);
            this.gpSetting.Controls.Add(this.txtVinVol);
            this.gpSetting.Controls.Add(this.txtIinVol);
            this.gpSetting.Controls.Add(this.txtVinDuty);
            this.gpSetting.Controls.Add(this.txtIinDuty);
            this.gpSetting.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gpSetting.Enabled = false;
            this.gpSetting.Location = new System.Drawing.Point(3, 3);
            this.gpSetting.Name = "gpSetting";
            this.gpSetting.Size = new System.Drawing.Size(274, 563);
            this.gpSetting.TabIndex = 3;
            this.gpSetting.TabStop = false;
            this.gpSetting.Text = "Setting";
            // 
            // txtVoutIns
            // 
            this.txtVoutIns.ForeColor = System.Drawing.Color.Black;
            this.txtVoutIns.Location = new System.Drawing.Point(53, 85);
            this.txtVoutIns.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtVoutIns.Multiline = true;
            this.txtVoutIns.Name = "txtVoutIns";
            this.txtVoutIns.Size = new System.Drawing.Size(199, 59);
            this.txtVoutIns.TabIndex = 174;
            this.txtVoutIns.Text = "To set the Object value for Constant-Voltage regulator on J4 (“Vout”) terminal.";
            this.txtVoutIns.Visible = false;
            // 
            // lbDither
            // 
            this.lbDither.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.lbDither.AutoSize = true;
            this.lbDither.Location = new System.Drawing.Point(19, 463);
            this.lbDither.Name = "lbDither";
            this.lbDither.Size = new System.Drawing.Size(122, 16);
            this.lbDither.TabIndex = 181;
            this.lbDither.Text = "Frequency Dither:";
            // 
            // txtIoutIns
            // 
            this.txtIoutIns.ForeColor = System.Drawing.Color.Black;
            this.txtIoutIns.Location = new System.Drawing.Point(53, 115);
            this.txtIoutIns.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtIoutIns.Multiline = true;
            this.txtIoutIns.Name = "txtIoutIns";
            this.txtIoutIns.Size = new System.Drawing.Size(199, 59);
            this.txtIoutIns.TabIndex = 175;
            this.txtIoutIns.Text = "To set the Threshold for Constant-Current OCP on J4 (“Vout”) terminal.";
            this.txtIoutIns.Visible = false;
            // 
            // txtVinIns
            // 
            this.txtVinIns.ForeColor = System.Drawing.Color.Black;
            this.txtVinIns.Location = new System.Drawing.Point(53, 161);
            this.txtVinIns.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtVinIns.Multiline = true;
            this.txtVinIns.Name = "txtVinIns";
            this.txtVinIns.Size = new System.Drawing.Size(199, 59);
            this.txtVinIns.TabIndex = 176;
            this.txtVinIns.Text = "To set the Threshold for Constant-Voltage OVP on J1 (“Vin”) terminal.";
            this.txtVinIns.Visible = false;
            // 
            // txtIinIns
            // 
            this.txtIinIns.ForeColor = System.Drawing.Color.Black;
            this.txtIinIns.Location = new System.Drawing.Point(53, 190);
            this.txtIinIns.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtIinIns.Multiline = true;
            this.txtIinIns.Name = "txtIinIns";
            this.txtIinIns.Size = new System.Drawing.Size(199, 59);
            this.txtIinIns.TabIndex = 177;
            this.txtIinIns.Text = "To set the Threshold for Constant-Current OCP on J1 (“Vin”) terminal.";
            this.txtIinIns.Visible = false;
            // 
            // chkPWMduty
            // 
            this.chkPWMduty.AutoSize = true;
            this.chkPWMduty.Location = new System.Drawing.Point(9, 280);
            this.chkPWMduty.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.chkPWMduty.Name = "chkPWMduty";
            this.chkPWMduty.Size = new System.Drawing.Size(185, 20);
            this.chkPWMduty.TabIndex = 178;
            this.chkPWMduty.Text = "Enable PWM duty setting";
            this.chkPWMduty.UseVisualStyleBackColor = true;
            this.chkPWMduty.CheckedChanged += new System.EventHandler(this.chkPWMduty_CheckedChanged);
            // 
            // lbIiVolSca
            // 
            this.lbIiVolSca.AutoSize = true;
            this.lbIiVolSca.Enabled = false;
            this.lbIiVolSca.Location = new System.Drawing.Point(222, 422);
            this.lbIiVolSca.Name = "lbIiVolSca";
            this.lbIiVolSca.Size = new System.Drawing.Size(27, 16);
            this.lbIiVolSca.TabIndex = 163;
            this.lbIiVolSca.Text = "(V)";
            // 
            // lbViVolSca
            // 
            this.lbViVolSca.AutoSize = true;
            this.lbViVolSca.Enabled = false;
            this.lbViVolSca.Location = new System.Drawing.Point(222, 396);
            this.lbViVolSca.Name = "lbViVolSca";
            this.lbViVolSca.Size = new System.Drawing.Size(27, 16);
            this.lbViVolSca.TabIndex = 163;
            this.lbViVolSca.Text = "(V)";
            // 
            // lbIoVolSca
            // 
            this.lbIoVolSca.AutoSize = true;
            this.lbIoVolSca.Enabled = false;
            this.lbIoVolSca.Location = new System.Drawing.Point(222, 369);
            this.lbIoVolSca.Name = "lbIoVolSca";
            this.lbIoVolSca.Size = new System.Drawing.Size(27, 16);
            this.lbIoVolSca.TabIndex = 163;
            this.lbIoVolSca.Text = "(V)";
            // 
            // lbVoVolSca
            // 
            this.lbVoVolSca.AutoSize = true;
            this.lbVoVolSca.Enabled = false;
            this.lbVoVolSca.Location = new System.Drawing.Point(222, 341);
            this.lbVoVolSca.Name = "lbVoVolSca";
            this.lbVoVolSca.Size = new System.Drawing.Size(27, 16);
            this.lbVoVolSca.TabIndex = 163;
            this.lbVoVolSca.Text = "(V)";
            // 
            // lbIiPerSca
            // 
            this.lbIiPerSca.AutoSize = true;
            this.lbIiPerSca.Enabled = false;
            this.lbIiPerSca.Location = new System.Drawing.Point(128, 422);
            this.lbIiPerSca.Name = "lbIiPerSca";
            this.lbIiPerSca.Size = new System.Drawing.Size(20, 16);
            this.lbIiPerSca.TabIndex = 163;
            this.lbIiPerSca.Text = "%";
            // 
            // lbViPerSca
            // 
            this.lbViPerSca.AutoSize = true;
            this.lbViPerSca.Enabled = false;
            this.lbViPerSca.Location = new System.Drawing.Point(128, 395);
            this.lbViPerSca.Name = "lbViPerSca";
            this.lbViPerSca.Size = new System.Drawing.Size(20, 16);
            this.lbViPerSca.TabIndex = 163;
            this.lbViPerSca.Text = "%";
            // 
            // lbIoPerSca
            // 
            this.lbIoPerSca.AutoSize = true;
            this.lbIoPerSca.Enabled = false;
            this.lbIoPerSca.Location = new System.Drawing.Point(128, 369);
            this.lbIoPerSca.Name = "lbIoPerSca";
            this.lbIoPerSca.Size = new System.Drawing.Size(20, 16);
            this.lbIoPerSca.TabIndex = 163;
            this.lbIoPerSca.Text = "%";
            // 
            // lbVoPerSca
            // 
            this.lbVoPerSca.AutoSize = true;
            this.lbVoPerSca.Enabled = false;
            this.lbVoPerSca.Location = new System.Drawing.Point(128, 341);
            this.lbVoPerSca.Name = "lbVoPerSca";
            this.lbVoPerSca.Size = new System.Drawing.Size(20, 16);
            this.lbVoPerSca.TabIndex = 163;
            this.lbVoPerSca.Text = "%";
            // 
            // txtIinSet
            // 
            this.txtIinSet.Location = new System.Drawing.Point(95, 191);
            this.txtIinSet.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtIinSet.Name = "txtIinSet";
            this.txtIinSet.Size = new System.Drawing.Size(81, 24);
            this.txtIinSet.TabIndex = 173;
            this.txtIinSet.TextChanged += new System.EventHandler(this.txtIinSet_TextChanged);
            this.txtIinSet.DoubleClick += new System.EventHandler(this.txtBox_DoubleClick);
            this.txtIinSet.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtIinSet_KeyPress);
            // 
            // txtVinSet
            // 
            this.txtVinSet.Location = new System.Drawing.Point(95, 161);
            this.txtVinSet.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtVinSet.Name = "txtVinSet";
            this.txtVinSet.Size = new System.Drawing.Size(81, 24);
            this.txtVinSet.TabIndex = 170;
            this.txtVinSet.TextChanged += new System.EventHandler(this.txtVinSet_TextChanged);
            this.txtVinSet.DoubleClick += new System.EventHandler(this.txtBox_DoubleClick);
            this.txtVinSet.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textBox_KeyPress);
            // 
            // txtIoutSet
            // 
            this.txtIoutSet.Location = new System.Drawing.Point(95, 116);
            this.txtIoutSet.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtIoutSet.Name = "txtIoutSet";
            this.txtIoutSet.Size = new System.Drawing.Size(81, 24);
            this.txtIoutSet.TabIndex = 172;
            this.txtIoutSet.TextChanged += new System.EventHandler(this.txtIoutSet_TextChanged);
            this.txtIoutSet.DoubleClick += new System.EventHandler(this.txtBox_DoubleClick);
            this.txtIoutSet.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textBox_KeyPress);
            // 
            // txtVoutSet
            // 
            this.txtVoutSet.ForeColor = System.Drawing.Color.Black;
            this.txtVoutSet.Location = new System.Drawing.Point(95, 86);
            this.txtVoutSet.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtVoutSet.Name = "txtVoutSet";
            this.txtVoutSet.Size = new System.Drawing.Size(81, 24);
            this.txtVoutSet.TabIndex = 171;
            this.txtVoutSet.TextChanged += new System.EventHandler(this.txtVoutSet_TextChanged);
            this.txtVoutSet.DoubleClick += new System.EventHandler(this.txtBox_DoubleClick);
            this.txtVoutSet.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textBox_KeyPress);
            // 
            // lblVinSetScale
            // 
            this.lblVinSetScale.AutoSize = true;
            this.lblVinSetScale.Location = new System.Drawing.Point(225, 165);
            this.lblVinSetScale.Name = "lblVinSetScale";
            this.lblVinSetScale.Size = new System.Drawing.Size(27, 16);
            this.lblVinSetScale.TabIndex = 159;
            this.lblVinSetScale.Text = "(V)";
            // 
            // txtIoutDuty
            // 
            this.txtIoutDuty.BackColor = System.Drawing.SystemColors.Window;
            this.txtIoutDuty.Enabled = false;
            this.txtIoutDuty.Location = new System.Drawing.Point(69, 366);
            this.txtIoutDuty.Name = "txtIoutDuty";
            this.txtIoutDuty.Size = new System.Drawing.Size(58, 24);
            this.txtIoutDuty.TabIndex = 157;
            this.txtIoutDuty.TextChanged += new System.EventHandler(this.Duty_TextChanged);
            this.txtIoutDuty.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textBox_KeyPress);
            // 
            // txtVoutDuty
            // 
            this.txtVoutDuty.BackColor = System.Drawing.SystemColors.Window;
            this.txtVoutDuty.Enabled = false;
            this.txtVoutDuty.ForeColor = System.Drawing.Color.Black;
            this.txtVoutDuty.Location = new System.Drawing.Point(69, 338);
            this.txtVoutDuty.Name = "txtVoutDuty";
            this.txtVoutDuty.Size = new System.Drawing.Size(58, 24);
            this.txtVoutDuty.TabIndex = 157;
            this.txtVoutDuty.TextChanged += new System.EventHandler(this.Duty_TextChanged);
            this.txtVoutDuty.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textBox_KeyPress);
            // 
            // txtIoutVol
            // 
            this.txtIoutVol.BackColor = System.Drawing.SystemColors.Window;
            this.txtIoutVol.Enabled = false;
            this.txtIoutVol.Location = new System.Drawing.Point(158, 366);
            this.txtIoutVol.Name = "txtIoutVol";
            this.txtIoutVol.ReadOnly = true;
            this.txtIoutVol.Size = new System.Drawing.Size(58, 24);
            this.txtIoutVol.TabIndex = 157;
            this.txtIoutVol.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // txtVoutVol
            // 
            this.txtVoutVol.BackColor = System.Drawing.SystemColors.Window;
            this.txtVoutVol.Enabled = false;
            this.txtVoutVol.Location = new System.Drawing.Point(158, 338);
            this.txtVoutVol.Name = "txtVoutVol";
            this.txtVoutVol.ReadOnly = true;
            this.txtVoutVol.Size = new System.Drawing.Size(58, 24);
            this.txtVoutVol.TabIndex = 157;
            this.txtVoutVol.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // lbPWMvol
            // 
            this.lbPWMvol.AutoSize = true;
            this.lbPWMvol.Enabled = false;
            this.lbPWMvol.Location = new System.Drawing.Point(149, 317);
            this.lbPWMvol.Name = "lbPWMvol";
            this.lbPWMvol.Size = new System.Drawing.Size(91, 16);
            this.lbPWMvol.TabIndex = 155;
            this.lbPWMvol.Text = "PWM voltage";
            // 
            // lbPWMduty
            // 
            this.lbPWMduty.AutoSize = true;
            this.lbPWMduty.Enabled = false;
            this.lbPWMduty.Location = new System.Drawing.Point(64, 317);
            this.lbPWMduty.Name = "lbPWMduty";
            this.lbPWMduty.Size = new System.Drawing.Size(72, 16);
            this.lbPWMduty.TabIndex = 155;
            this.lbPWMduty.Text = "PWM duty";
            // 
            // lbVoutPWM
            // 
            this.lbVoutPWM.AutoSize = true;
            this.lbVoutPWM.Enabled = false;
            this.lbVoutPWM.Location = new System.Drawing.Point(19, 342);
            this.lbVoutPWM.Name = "lbVoutPWM";
            this.lbVoutPWM.Size = new System.Drawing.Size(40, 16);
            this.lbVoutPWM.TabIndex = 155;
            this.lbVoutPWM.Text = "Vout:";
            // 
            // lblVoutSet
            // 
            this.lblVoutSet.AutoSize = true;
            this.lblVoutSet.Location = new System.Drawing.Point(9, 90);
            this.lblVoutSet.Name = "lblVoutSet";
            this.lblVoutSet.Size = new System.Drawing.Size(40, 16);
            this.lblVoutSet.TabIndex = 155;
            this.lblVoutSet.Text = "Vout:";
            this.lblVoutSet.MouseEnter += new System.EventHandler(this.lblVoutSet_MouseEnter);
            this.lblVoutSet.MouseLeave += new System.EventHandler(this.lblVoutSet_MouseLeave);
            // 
            // lbVinPWM
            // 
            this.lbVinPWM.AutoSize = true;
            this.lbVinPWM.Enabled = false;
            this.lbVinPWM.Location = new System.Drawing.Point(19, 396);
            this.lbVinPWM.Name = "lbVinPWM";
            this.lbVinPWM.Size = new System.Drawing.Size(32, 16);
            this.lbVinPWM.TabIndex = 156;
            this.lbVinPWM.Text = "Vin:";
            // 
            // lblVinSet
            // 
            this.lblVinSet.AutoSize = true;
            this.lblVinSet.Location = new System.Drawing.Point(13, 165);
            this.lblVinSet.Name = "lblVinSet";
            this.lblVinSet.Size = new System.Drawing.Size(32, 16);
            this.lblVinSet.TabIndex = 156;
            this.lblVinSet.Text = "Vin:";
            this.lblVinSet.MouseEnter += new System.EventHandler(this.lblVinSet_MouseEnter);
            this.lblVinSet.MouseLeave += new System.EventHandler(this.lblVinSet_MouseLeave);
            // 
            // lbIoutPWM
            // 
            this.lbIoutPWM.AutoSize = true;
            this.lbIoutPWM.Enabled = false;
            this.lbIoutPWM.Location = new System.Drawing.Point(19, 369);
            this.lbIoutPWM.Name = "lbIoutPWM";
            this.lbIoutPWM.Size = new System.Drawing.Size(35, 16);
            this.lbIoutPWM.TabIndex = 153;
            this.lbIoutPWM.Text = "Iout:";
            // 
            // lblIoutSet
            // 
            this.lblIoutSet.AutoSize = true;
            this.lblIoutSet.Location = new System.Drawing.Point(12, 120);
            this.lblIoutSet.Name = "lblIoutSet";
            this.lblIoutSet.Size = new System.Drawing.Size(35, 16);
            this.lblIoutSet.TabIndex = 153;
            this.lblIoutSet.Text = "Iout:";
            this.lblIoutSet.MouseEnter += new System.EventHandler(this.lblIoutSet_MouseEnter);
            this.lblIoutSet.MouseLeave += new System.EventHandler(this.lblIoutSet_MouseLeave);
            // 
            // lblVoutSetScale
            // 
            this.lblVoutSetScale.AutoSize = true;
            this.lblVoutSetScale.Location = new System.Drawing.Point(225, 90);
            this.lblVoutSetScale.Name = "lblVoutSetScale";
            this.lblVoutSetScale.Size = new System.Drawing.Size(27, 16);
            this.lblVoutSetScale.TabIndex = 160;
            this.lblVoutSetScale.Text = "(V)";
            // 
            // lbIinPWM
            // 
            this.lbIinPWM.AutoSize = true;
            this.lbIinPWM.Enabled = false;
            this.lbIinPWM.Location = new System.Drawing.Point(19, 423);
            this.lbIinPWM.Name = "lbIinPWM";
            this.lbIinPWM.Size = new System.Drawing.Size(26, 16);
            this.lbIinPWM.TabIndex = 154;
            this.lbIinPWM.Text = "Iin:";
            // 
            // lblIinSet
            // 
            this.lblIinSet.AutoSize = true;
            this.lblIinSet.Location = new System.Drawing.Point(16, 195);
            this.lblIinSet.Name = "lblIinSet";
            this.lblIinSet.Size = new System.Drawing.Size(26, 16);
            this.lblIinSet.TabIndex = 154;
            this.lblIinSet.Text = "Iin:";
            this.lblIinSet.MouseEnter += new System.EventHandler(this.lblIinSet_MouseEnter);
            this.lblIinSet.MouseLeave += new System.EventHandler(this.lblIinSet_MouseLeave);
            // 
            // chkReverse
            // 
            this.chkReverse.AutoSize = true;
            this.chkReverse.Location = new System.Drawing.Point(12, 46);
            this.chkReverse.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.chkReverse.Name = "chkReverse";
            this.chkReverse.Size = new System.Drawing.Size(204, 20);
            this.chkReverse.TabIndex = 152;
            this.chkReverse.Text = "Disable the reverse function";
            this.chkReverse.UseVisualStyleBackColor = true;
            this.chkReverse.CheckedChanged += new System.EventHandler(this.chkReverse_CheckedChanged);
            // 
            // btnVoutDe
            // 
            this.btnVoutDe.Location = new System.Drawing.Point(53, 85);
            this.btnVoutDe.Name = "btnVoutDe";
            this.btnVoutDe.Size = new System.Drawing.Size(44, 26);
            this.btnVoutDe.TabIndex = 163;
            this.btnVoutDe.Text = "🔻";
            this.btnVoutDe.UseVisualStyleBackColor = true;
            this.btnVoutDe.Click += new System.EventHandler(this.btnVoutDe_Click);
            // 
            // btnVinDe
            // 
            this.btnVinDe.Location = new System.Drawing.Point(53, 160);
            this.btnVinDe.Name = "btnVinDe";
            this.btnVinDe.Size = new System.Drawing.Size(44, 26);
            this.btnVinDe.TabIndex = 164;
            this.btnVinDe.Text = "🔻";
            this.btnVinDe.UseVisualStyleBackColor = true;
            this.btnVinDe.Click += new System.EventHandler(this.btnVinDe_Click);
            // 
            // btnIoutDe
            // 
            this.btnIoutDe.Location = new System.Drawing.Point(53, 115);
            this.btnIoutDe.Name = "btnIoutDe";
            this.btnIoutDe.Size = new System.Drawing.Size(44, 26);
            this.btnIoutDe.TabIndex = 165;
            this.btnIoutDe.Text = "🔻";
            this.btnIoutDe.UseVisualStyleBackColor = true;
            this.btnIoutDe.Click += new System.EventHandler(this.btnIoutDe_Click);
            // 
            // btnIinDe
            // 
            this.btnIinDe.Location = new System.Drawing.Point(53, 190);
            this.btnIinDe.Name = "btnIinDe";
            this.btnIinDe.Size = new System.Drawing.Size(44, 26);
            this.btnIinDe.TabIndex = 162;
            this.btnIinDe.Text = "🔻";
            this.btnIinDe.UseVisualStyleBackColor = true;
            this.btnIinDe.Click += new System.EventHandler(this.btnIinDe_Click);
            // 
            // lblIinSetScale
            // 
            this.lblIinSetScale.AutoSize = true;
            this.lblIinSetScale.Location = new System.Drawing.Point(225, 195);
            this.lblIinSetScale.Name = "lblIinSetScale";
            this.lblIinSetScale.Size = new System.Drawing.Size(27, 16);
            this.lblIinSetScale.TabIndex = 158;
            this.lblIinSetScale.Text = "(A)";
            // 
            // btnVoutIn
            // 
            this.btnVoutIn.Location = new System.Drawing.Point(174, 85);
            this.btnVoutIn.Name = "btnVoutIn";
            this.btnVoutIn.Size = new System.Drawing.Size(44, 26);
            this.btnVoutIn.TabIndex = 167;
            this.btnVoutIn.Text = "🔺";
            this.btnVoutIn.UseVisualStyleBackColor = true;
            this.btnVoutIn.Click += new System.EventHandler(this.btnVoutIn_Click);
            // 
            // btnVinIn
            // 
            this.btnVinIn.Location = new System.Drawing.Point(174, 160);
            this.btnVinIn.Name = "btnVinIn";
            this.btnVinIn.Size = new System.Drawing.Size(44, 26);
            this.btnVinIn.TabIndex = 168;
            this.btnVinIn.Text = "🔺";
            this.btnVinIn.UseVisualStyleBackColor = true;
            this.btnVinIn.Click += new System.EventHandler(this.btnVinIn_Click);
            // 
            // lblIoutSetScale
            // 
            this.lblIoutSetScale.AutoSize = true;
            this.lblIoutSetScale.Location = new System.Drawing.Point(225, 120);
            this.lblIoutSetScale.Name = "lblIoutSetScale";
            this.lblIoutSetScale.Size = new System.Drawing.Size(27, 16);
            this.lblIoutSetScale.TabIndex = 157;
            this.lblIoutSetScale.Text = "(A)";
            // 
            // btnIoutIn
            // 
            this.btnIoutIn.Location = new System.Drawing.Point(174, 115);
            this.btnIoutIn.Name = "btnIoutIn";
            this.btnIoutIn.Size = new System.Drawing.Size(44, 26);
            this.btnIoutIn.TabIndex = 161;
            this.btnIoutIn.Text = "🔺";
            this.btnIoutIn.UseVisualStyleBackColor = true;
            this.btnIoutIn.Click += new System.EventHandler(this.btnIoutIn_Click);
            // 
            // btnIinIn
            // 
            this.btnIinIn.Location = new System.Drawing.Point(174, 190);
            this.btnIinIn.Name = "btnIinIn";
            this.btnIinIn.Size = new System.Drawing.Size(44, 26);
            this.btnIinIn.TabIndex = 166;
            this.btnIinIn.Text = "🔺";
            this.btnIinIn.UseVisualStyleBackColor = true;
            this.btnIinIn.Click += new System.EventHandler(this.btnIinIn_Click);
            // 
            // btnON
            // 
            this.btnON.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnON.BackColor = System.Drawing.Color.Transparent;
            this.btnON.BackgroundImage = global::DEMO_ISL81X01.Properties.Resources.button_start;
            this.btnON.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.btnON.BaseColor = System.Drawing.Color.Transparent;
            this.btnON.ButtonColor = System.Drawing.Color.FromArgb(((int)(((byte)(210)))), ((int)(((byte)(202)))), ((int)(((byte)(0)))));
            this.btnON.ButtonText = "   START";
            this.btnON.CornerRadius = 15;
            this.btnON.Font = new System.Drawing.Font("Arial", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnON.ForeColor = System.Drawing.Color.Black;
            this.btnON.GlowColor = System.Drawing.Color.Yellow;
            this.btnON.Location = new System.Drawing.Point(18, 496);
            this.btnON.Margin = new System.Windows.Forms.Padding(2);
            this.btnON.Name = "btnON";
            this.btnON.Size = new System.Drawing.Size(224, 53);
            this.btnON.TabIndex = 147;
            this.btnON.Click += new System.EventHandler(this.btnON_Click);
            // 
            // btnOFF
            // 
            this.btnOFF.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnOFF.BackColor = System.Drawing.Color.Transparent;
            this.btnOFF.BackgroundImage = global::DEMO_ISL81X01.Properties.Resources.button_stop;
            this.btnOFF.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.btnOFF.BaseColor = System.Drawing.Color.Transparent;
            this.btnOFF.ButtonColor = System.Drawing.Color.FromArgb(((int)(((byte)(210)))), ((int)(((byte)(202)))), ((int)(((byte)(0)))));
            this.btnOFF.ButtonText = "  STOP";
            this.btnOFF.CornerRadius = 15;
            this.btnOFF.Font = new System.Drawing.Font("Arial", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnOFF.ForeColor = System.Drawing.Color.Black;
            this.btnOFF.GlowColor = System.Drawing.Color.Yellow;
            this.btnOFF.Location = new System.Drawing.Point(18, 496);
            this.btnOFF.Margin = new System.Windows.Forms.Padding(2);
            this.btnOFF.Name = "btnOFF";
            this.btnOFF.Size = new System.Drawing.Size(224, 53);
            this.btnOFF.TabIndex = 146;
            this.btnOFF.Visible = false;
            this.btnOFF.Click += new System.EventHandler(this.btnOFF_Click);
            // 
            // btnDitherEn
            // 
            this.btnDitherEn.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnDitherEn.BackColor = System.Drawing.Color.Transparent;
            this.btnDitherEn.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.btnDitherEn.BaseColor = System.Drawing.Color.Transparent;
            this.btnDitherEn.ButtonColor = System.Drawing.Color.FromArgb(((int)(((byte)(210)))), ((int)(((byte)(202)))), ((int)(((byte)(0)))));
            this.btnDitherEn.ButtonText = "ENABLE";
            this.btnDitherEn.CornerRadius = 15;
            this.btnDitherEn.Font = new System.Drawing.Font("Arial", 11F);
            this.btnDitherEn.ForeColor = System.Drawing.Color.Black;
            this.btnDitherEn.GlowColor = System.Drawing.Color.Yellow;
            this.btnDitherEn.Location = new System.Drawing.Point(152, 448);
            this.btnDitherEn.Margin = new System.Windows.Forms.Padding(2);
            this.btnDitherEn.Name = "btnDitherEn";
            this.btnDitherEn.Size = new System.Drawing.Size(91, 42);
            this.btnDitherEn.TabIndex = 179;
            this.btnDitherEn.Click += new System.EventHandler(this.btnDitherEn_Click);
            // 
            // btnDitherDis
            // 
            this.btnDitherDis.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnDitherDis.BackColor = System.Drawing.Color.Transparent;
            this.btnDitherDis.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.btnDitherDis.BaseColor = System.Drawing.Color.Transparent;
            this.btnDitherDis.ButtonColor = System.Drawing.Color.FromArgb(((int)(((byte)(210)))), ((int)(((byte)(202)))), ((int)(((byte)(0)))));
            this.btnDitherDis.ButtonText = "DISABLE";
            this.btnDitherDis.CornerRadius = 15;
            this.btnDitherDis.Font = new System.Drawing.Font("Arial", 11F);
            this.btnDitherDis.ForeColor = System.Drawing.Color.Black;
            this.btnDitherDis.GlowColor = System.Drawing.Color.Yellow;
            this.btnDitherDis.Location = new System.Drawing.Point(152, 448);
            this.btnDitherDis.Margin = new System.Windows.Forms.Padding(2);
            this.btnDitherDis.Name = "btnDitherDis";
            this.btnDitherDis.Size = new System.Drawing.Size(90, 42);
            this.btnDitherDis.TabIndex = 180;
            this.btnDitherDis.Click += new System.EventHandler(this.btnDitherDis_Click);
            // 
            // txtVinVol
            // 
            this.txtVinVol.Enabled = false;
            this.txtVinVol.ForeColor = System.Drawing.Color.Black;
            this.txtVinVol.Location = new System.Drawing.Point(158, 392);
            this.txtVinVol.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtVinVol.Name = "txtVinVol";
            this.txtVinVol.Size = new System.Drawing.Size(58, 24);
            this.txtVinVol.TabIndex = 186;
            this.txtVinVol.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // txtIinVol
            // 
            this.txtIinVol.Enabled = false;
            this.txtIinVol.ForeColor = System.Drawing.Color.Black;
            this.txtIinVol.Location = new System.Drawing.Point(158, 419);
            this.txtIinVol.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtIinVol.Name = "txtIinVol";
            this.txtIinVol.Size = new System.Drawing.Size(58, 24);
            this.txtIinVol.TabIndex = 187;
            this.txtIinVol.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // txtVinDuty
            // 
            this.txtVinDuty.Enabled = false;
            this.txtVinDuty.ForeColor = System.Drawing.Color.Black;
            this.txtVinDuty.Location = new System.Drawing.Point(69, 392);
            this.txtVinDuty.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtVinDuty.Name = "txtVinDuty";
            this.txtVinDuty.Size = new System.Drawing.Size(58, 24);
            this.txtVinDuty.TabIndex = 188;
            this.txtVinDuty.TextChanged += new System.EventHandler(this.Duty_TextChanged);
            this.txtVinDuty.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textBox_KeyPress);
            // 
            // txtIinDuty
            // 
            this.txtIinDuty.Enabled = false;
            this.txtIinDuty.ForeColor = System.Drawing.Color.Black;
            this.txtIinDuty.Location = new System.Drawing.Point(69, 419);
            this.txtIinDuty.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtIinDuty.Name = "txtIinDuty";
            this.txtIinDuty.Size = new System.Drawing.Size(58, 24);
            this.txtIinDuty.TabIndex = 189;
            this.txtIinDuty.TextChanged += new System.EventHandler(this.Duty_TextChanged);
            this.txtIinDuty.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textBox_KeyPress);
            // 
            // gpStatus
            // 
            this.gpStatus.Controls.Add(this.lblTempActualScale);
            this.gpStatus.Controls.Add(this.lblVinActualScale);
            this.gpStatus.Controls.Add(this.lblIinActualScale);
            this.gpStatus.Controls.Add(this.lblVoutActualScale);
            this.gpStatus.Controls.Add(this.lblIoutActualScale);
            this.gpStatus.Controls.Add(this.txtTemp);
            this.gpStatus.Controls.Add(this.txtIin);
            this.gpStatus.Controls.Add(this.txtVin);
            this.gpStatus.Controls.Add(this.txtIout);
            this.gpStatus.Controls.Add(this.txtVout);
            this.gpStatus.Controls.Add(this.picVinSta);
            this.gpStatus.Controls.Add(this.lblMode);
            this.gpStatus.Controls.Add(this.picSysDirection);
            this.gpStatus.Controls.Add(this.lblModeSta);
            this.gpStatus.Controls.Add(this.picVoutSta);
            this.gpStatus.Controls.Add(this.lblTempSta);
            this.gpStatus.Controls.Add(this.lblPGSta);
            this.gpStatus.Controls.Add(this.picIoutSta);
            this.gpStatus.Controls.Add(this.picPGSta);
            this.gpStatus.Controls.Add(this.lblIoutSta);
            this.gpStatus.Controls.Add(this.lblVoutSta);
            this.gpStatus.Controls.Add(this.picIinSta);
            this.gpStatus.Controls.Add(this.picTempSta);
            this.gpStatus.Controls.Add(this.lblVinSta);
            this.gpStatus.Controls.Add(this.lblIinSta);
            this.gpStatus.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gpStatus.Enabled = false;
            this.gpStatus.Location = new System.Drawing.Point(927, 3);
            this.gpStatus.Name = "gpStatus";
            this.gpStatus.Size = new System.Drawing.Size(344, 563);
            this.gpStatus.TabIndex = 4;
            this.gpStatus.TabStop = false;
            this.gpStatus.Text = "Status";
            // 
            // lblTempActualScale
            // 
            this.lblTempActualScale.AutoSize = true;
            this.lblTempActualScale.Location = new System.Drawing.Point(290, 519);
            this.lblTempActualScale.Name = "lblTempActualScale";
            this.lblTempActualScale.Size = new System.Drawing.Size(34, 16);
            this.lblTempActualScale.TabIndex = 166;
            this.lblTempActualScale.Text = "(°C)";
            // 
            // lblVinActualScale
            // 
            this.lblVinActualScale.AutoSize = true;
            this.lblVinActualScale.Location = new System.Drawing.Point(290, 428);
            this.lblVinActualScale.Name = "lblVinActualScale";
            this.lblVinActualScale.Size = new System.Drawing.Size(27, 16);
            this.lblVinActualScale.TabIndex = 165;
            this.lblVinActualScale.Text = "(V)";
            // 
            // lblIinActualScale
            // 
            this.lblIinActualScale.AutoSize = true;
            this.lblIinActualScale.Location = new System.Drawing.Point(290, 475);
            this.lblIinActualScale.Name = "lblIinActualScale";
            this.lblIinActualScale.Size = new System.Drawing.Size(27, 16);
            this.lblIinActualScale.TabIndex = 164;
            this.lblIinActualScale.Text = "(A)";
            // 
            // lblVoutActualScale
            // 
            this.lblVoutActualScale.AutoSize = true;
            this.lblVoutActualScale.Location = new System.Drawing.Point(290, 334);
            this.lblVoutActualScale.Name = "lblVoutActualScale";
            this.lblVoutActualScale.Size = new System.Drawing.Size(27, 16);
            this.lblVoutActualScale.TabIndex = 163;
            this.lblVoutActualScale.Text = "(V)";
            // 
            // lblIoutActualScale
            // 
            this.lblIoutActualScale.AutoSize = true;
            this.lblIoutActualScale.Location = new System.Drawing.Point(290, 381);
            this.lblIoutActualScale.Name = "lblIoutActualScale";
            this.lblIoutActualScale.Size = new System.Drawing.Size(27, 16);
            this.lblIoutActualScale.TabIndex = 162;
            this.lblIoutActualScale.Text = "(A)";
            // 
            // txtTemp
            // 
            this.txtTemp.BackColor = System.Drawing.Color.White;
            this.txtTemp.Location = new System.Drawing.Point(226, 516);
            this.txtTemp.Name = "txtTemp";
            this.txtTemp.ReadOnly = true;
            this.txtTemp.Size = new System.Drawing.Size(58, 24);
            this.txtTemp.TabIndex = 161;
            this.txtTemp.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // txtIin
            // 
            this.txtIin.BackColor = System.Drawing.Color.White;
            this.txtIin.Location = new System.Drawing.Point(226, 472);
            this.txtIin.Name = "txtIin";
            this.txtIin.ReadOnly = true;
            this.txtIin.Size = new System.Drawing.Size(58, 24);
            this.txtIin.TabIndex = 160;
            this.txtIin.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // txtVin
            // 
            this.txtVin.BackColor = System.Drawing.Color.White;
            this.txtVin.Location = new System.Drawing.Point(226, 425);
            this.txtVin.Name = "txtVin";
            this.txtVin.ReadOnly = true;
            this.txtVin.Size = new System.Drawing.Size(58, 24);
            this.txtVin.TabIndex = 159;
            this.txtVin.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // txtIout
            // 
            this.txtIout.BackColor = System.Drawing.Color.White;
            this.txtIout.Location = new System.Drawing.Point(226, 378);
            this.txtIout.Name = "txtIout";
            this.txtIout.ReadOnly = true;
            this.txtIout.Size = new System.Drawing.Size(58, 24);
            this.txtIout.TabIndex = 158;
            this.txtIout.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // txtVout
            // 
            this.txtVout.BackColor = System.Drawing.Color.White;
            this.txtVout.Location = new System.Drawing.Point(226, 331);
            this.txtVout.Name = "txtVout";
            this.txtVout.ReadOnly = true;
            this.txtVout.Size = new System.Drawing.Size(58, 24);
            this.txtVout.TabIndex = 157;
            this.txtVout.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // picVinSta
            // 
            this.picVinSta.BackColor = System.Drawing.Color.Transparent;
            this.picVinSta.InitialImage = null;
            this.picVinSta.Location = new System.Drawing.Point(168, 416);
            this.picVinSta.Name = "picVinSta";
            this.picVinSta.Size = new System.Drawing.Size(39, 41);
            this.picVinSta.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.picVinSta.TabIndex = 154;
            this.picVinSta.TabStop = false;
            // 
            // lblMode
            // 
            this.lblMode.AutoSize = true;
            this.lblMode.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblMode.Location = new System.Drawing.Point(173, 235);
            this.lblMode.Name = "lblMode";
            this.lblMode.Size = new System.Drawing.Size(30, 18);
            this.lblMode.TabIndex = 151;
            this.lblMode.Text = "CC";
            this.lblMode.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // picSysDirection
            // 
            this.picSysDirection.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.picSysDirection.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.picSysDirection.Image = global::DEMO_ISL81X01.Properties.Resources.current_initial;
            this.picSysDirection.Location = new System.Drawing.Point(6, 24);
            this.picSysDirection.Name = "picSysDirection";
            this.picSysDirection.Size = new System.Drawing.Size(332, 189);
            this.picSysDirection.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.picSysDirection.TabIndex = 151;
            this.picSysDirection.TabStop = false;
            // 
            // lblModeSta
            // 
            this.lblModeSta.AutoSize = true;
            this.lblModeSta.Location = new System.Drawing.Point(6, 236);
            this.lblModeSta.Name = "lblModeSta";
            this.lblModeSta.Size = new System.Drawing.Size(155, 16);
            this.lblModeSta.TabIndex = 152;
            this.lblModeSta.Text = "Battery charging mode:";
            // 
            // picVoutSta
            // 
            this.picVoutSta.BackColor = System.Drawing.Color.Transparent;
            this.picVoutSta.InitialImage = null;
            this.picVoutSta.Location = new System.Drawing.Point(168, 322);
            this.picVoutSta.Name = "picVoutSta";
            this.picVoutSta.Size = new System.Drawing.Size(39, 41);
            this.picVoutSta.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.picVoutSta.TabIndex = 155;
            this.picVoutSta.TabStop = false;
            // 
            // lblTempSta
            // 
            this.lblTempSta.AutoSize = true;
            this.lblTempSta.Location = new System.Drawing.Point(42, 519);
            this.lblTempSta.Name = "lblTempSta";
            this.lblTempSta.Size = new System.Drawing.Size(92, 16);
            this.lblTempSta.TabIndex = 150;
            this.lblTempSta.Text = "Temperature:";
            // 
            // lblPGSta
            // 
            this.lblPGSta.AutoSize = true;
            this.lblPGSta.Location = new System.Drawing.Point(45, 288);
            this.lblPGSta.Name = "lblPGSta";
            this.lblPGSta.Size = new System.Drawing.Size(87, 16);
            this.lblPGSta.TabIndex = 151;
            this.lblPGSta.Text = "Power good:";
            // 
            // picIoutSta
            // 
            this.picIoutSta.BackColor = System.Drawing.Color.Transparent;
            this.picIoutSta.InitialImage = null;
            this.picIoutSta.Location = new System.Drawing.Point(168, 369);
            this.picIoutSta.Name = "picIoutSta";
            this.picIoutSta.Size = new System.Drawing.Size(39, 41);
            this.picIoutSta.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.picIoutSta.TabIndex = 153;
            this.picIoutSta.TabStop = false;
            // 
            // picPGSta
            // 
            this.picPGSta.BackColor = System.Drawing.Color.Transparent;
            this.picPGSta.InitialImage = null;
            this.picPGSta.Location = new System.Drawing.Point(168, 276);
            this.picPGSta.Name = "picPGSta";
            this.picPGSta.Size = new System.Drawing.Size(39, 41);
            this.picPGSta.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.picPGSta.TabIndex = 154;
            this.picPGSta.TabStop = false;
            // 
            // lblIoutSta
            // 
            this.lblIoutSta.AutoSize = true;
            this.lblIoutSta.Location = new System.Drawing.Point(71, 381);
            this.lblIoutSta.Name = "lblIoutSta";
            this.lblIoutSta.Size = new System.Drawing.Size(35, 16);
            this.lblIoutSta.TabIndex = 152;
            this.lblIoutSta.Text = "Iout:";
            // 
            // lblVoutSta
            // 
            this.lblVoutSta.AutoSize = true;
            this.lblVoutSta.Location = new System.Drawing.Point(68, 334);
            this.lblVoutSta.Name = "lblVoutSta";
            this.lblVoutSta.Size = new System.Drawing.Size(40, 16);
            this.lblVoutSta.TabIndex = 150;
            this.lblVoutSta.Text = "Vout:";
            // 
            // picIinSta
            // 
            this.picIinSta.BackColor = System.Drawing.Color.Transparent;
            this.picIinSta.InitialImage = null;
            this.picIinSta.Location = new System.Drawing.Point(168, 463);
            this.picIinSta.Name = "picIinSta";
            this.picIinSta.Size = new System.Drawing.Size(39, 41);
            this.picIinSta.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.picIinSta.TabIndex = 152;
            this.picIinSta.TabStop = false;
            // 
            // picTempSta
            // 
            this.picTempSta.BackColor = System.Drawing.Color.Transparent;
            this.picTempSta.InitialImage = null;
            this.picTempSta.Location = new System.Drawing.Point(168, 507);
            this.picTempSta.Name = "picTempSta";
            this.picTempSta.Size = new System.Drawing.Size(39, 41);
            this.picTempSta.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.picTempSta.TabIndex = 153;
            this.picTempSta.TabStop = false;
            // 
            // lblVinSta
            // 
            this.lblVinSta.AutoSize = true;
            this.lblVinSta.Location = new System.Drawing.Point(72, 428);
            this.lblVinSta.Name = "lblVinSta";
            this.lblVinSta.Size = new System.Drawing.Size(32, 16);
            this.lblVinSta.TabIndex = 149;
            this.lblVinSta.Text = "Vin:";
            // 
            // lblIinSta
            // 
            this.lblIinSta.AutoSize = true;
            this.lblIinSta.Location = new System.Drawing.Point(75, 475);
            this.lblIinSta.Name = "lblIinSta";
            this.lblIinSta.Size = new System.Drawing.Size(26, 16);
            this.lblIinSta.TabIndex = 151;
            this.lblIinSta.Text = "Iin:";
            // 
            // gpMonitor
            // 
            this.gpMonitor.Controls.Add(this.rdoIn);
            this.gpMonitor.Controls.Add(this.rdoOut);
            this.gpMonitor.Controls.Add(this.lblUnitA);
            this.gpMonitor.Controls.Add(this.lblUintV);
            this.gpMonitor.Controls.Add(this.chtMonitor);
            this.gpMonitor.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gpMonitor.Enabled = false;
            this.gpMonitor.Location = new System.Drawing.Point(283, 3);
            this.gpMonitor.Name = "gpMonitor";
            this.gpMonitor.Size = new System.Drawing.Size(638, 563);
            this.gpMonitor.TabIndex = 5;
            this.gpMonitor.TabStop = false;
            this.gpMonitor.Text = "Monitor";
            // 
            // rdoIn
            // 
            this.rdoIn.AutoSize = true;
            this.rdoIn.Location = new System.Drawing.Point(117, 28);
            this.rdoIn.Name = "rdoIn";
            this.rdoIn.Size = new System.Drawing.Size(68, 20);
            this.rdoIn.TabIndex = 164;
            this.rdoIn.Text = "Vin, Iin";
            this.rdoIn.UseVisualStyleBackColor = true;
            // 
            // rdoOut
            // 
            this.rdoOut.AutoSize = true;
            this.rdoOut.Checked = true;
            this.rdoOut.Location = new System.Drawing.Point(15, 28);
            this.rdoOut.Name = "rdoOut";
            this.rdoOut.Size = new System.Drawing.Size(85, 20);
            this.rdoOut.TabIndex = 162;
            this.rdoOut.TabStop = true;
            this.rdoOut.Text = "Vout, Iout";
            this.rdoOut.UseVisualStyleBackColor = true;
            // 
            // lblUnitA
            // 
            this.lblUnitA.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblUnitA.AutoSize = true;
            this.lblUnitA.BackColor = System.Drawing.Color.White;
            this.lblUnitA.Location = new System.Drawing.Point(567, 100);
            this.lblUnitA.Name = "lblUnitA";
            this.lblUnitA.Size = new System.Drawing.Size(27, 16);
            this.lblUnitA.TabIndex = 161;
            this.lblUnitA.Text = "(A)";
            // 
            // lblUintV
            // 
            this.lblUintV.AutoSize = true;
            this.lblUintV.BackColor = System.Drawing.Color.White;
            this.lblUintV.Location = new System.Drawing.Point(47, 100);
            this.lblUintV.Name = "lblUintV";
            this.lblUintV.Size = new System.Drawing.Size(27, 16);
            this.lblUintV.TabIndex = 160;
            this.lblUintV.Text = "(V)";
            // 
            // chtMonitor
            // 
            this.chtMonitor.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            chartArea2.Name = "ChartArea1";
            this.chtMonitor.ChartAreas.Add(chartArea2);
            legend2.Docking = System.Windows.Forms.DataVisualization.Charting.Docking.Top;
            legend2.Name = "Legend1";
            this.chtMonitor.Legends.Add(legend2);
            this.chtMonitor.Location = new System.Drawing.Point(15, 63);
            this.chtMonitor.Name = "chtMonitor";
            series5.BorderWidth = 2;
            series5.ChartArea = "ChartArea1";
            series5.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Spline;
            series5.Color = System.Drawing.Color.Blue;
            series5.Legend = "Legend1";
            series5.Name = "Vout";
            series6.BorderWidth = 2;
            series6.ChartArea = "ChartArea1";
            series6.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Spline;
            series6.Color = System.Drawing.Color.Red;
            series6.Legend = "Legend1";
            series6.Name = "Iout";
            series6.YAxisType = System.Windows.Forms.DataVisualization.Charting.AxisType.Secondary;
            series7.BorderWidth = 2;
            series7.ChartArea = "ChartArea1";
            series7.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Spline;
            series7.Color = System.Drawing.Color.Lime;
            series7.Legend = "Legend1";
            series7.Name = "Vin";
            series8.BorderWidth = 2;
            series8.ChartArea = "ChartArea1";
            series8.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Spline;
            series8.Color = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            series8.Legend = "Legend1";
            series8.Name = "Iin";
            series8.YAxisType = System.Windows.Forms.DataVisualization.Charting.AxisType.Secondary;
            this.chtMonitor.Series.Add(series5);
            this.chtMonitor.Series.Add(series6);
            this.chtMonitor.Series.Add(series7);
            this.chtMonitor.Series.Add(series8);
            this.chtMonitor.Size = new System.Drawing.Size(608, 486);
            this.chtMonitor.TabIndex = 159;
            this.chtMonitor.Text = "chart1";
            this.chtMonitor.GetToolTipText += new System.EventHandler<System.Windows.Forms.DataVisualization.Charting.ToolTipEventArgs>(this.chtMonitor_GetToolTipText);
            // 
            // timer1
            // 
            this.timer1.Interval = 1000;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // dataSet1
            // 
            this.dataSet1.DataSetName = "NewDataSet";
            // 
            // timer2
            // 
            this.timer2.Interval = 50;
            this.timer2.Tick += new System.EventHandler(this.timer2_Tick);
            // 
            // Demo
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = global::DEMO_ISL81X01.Properties.Resources.backgroud1;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(1274, 691);
            this.Controls.Add(this.tableLayoutPanel1);
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this.gpConnect);
            this.Font = new System.Drawing.Font("Arial", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "Demo";
            this.Text = "Demo for ISL81X01";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.IntersilDemo_FormClosing);
            this.gpConnect.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.picLogo)).EndInit();
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.tableLayoutPanel1.ResumeLayout(false);
            this.gpSetting.ResumeLayout(false);
            this.gpSetting.PerformLayout();
            this.gpStatus.ResumeLayout(false);
            this.gpStatus.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picVinSta)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picSysDirection)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picVoutSta)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picIoutSta)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picPGSta)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picIinSta)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picTempSta)).EndInit();
            this.gpMonitor.ResumeLayout(false);
            this.gpMonitor.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chtMonitor)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox gpConnect;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private VistaButtonTest.VistaButton btnConnect;
        private System.Windows.Forms.Label lblVinSetScale;
        private System.Windows.Forms.Label lblVoutSetScale;
        private System.Windows.Forms.TextBox txtIinSet;
        private System.Windows.Forms.TextBox txtIoutSet;
        private System.Windows.Forms.Label lblIinSetScale;
        private System.Windows.Forms.Label lblIoutSetScale;
        private System.Windows.Forms.TextBox txtVinSet;
        private System.Windows.Forms.TextBox txtVoutSet;
        private System.Windows.Forms.Button btnIinIn;
        private System.Windows.Forms.Button btnIoutIn;
        private System.Windows.Forms.Button btnVinIn;
        private System.Windows.Forms.Button btnVoutIn;
        private System.Windows.Forms.Button btnIinDe;
        private System.Windows.Forms.Button btnIoutDe;
        private System.Windows.Forms.Button btnVinDe;
        private System.Windows.Forms.Button btnVoutDe;
        private System.Windows.Forms.Label lblIinSet;
        private System.Windows.Forms.Label lblIoutSet;
        private System.Windows.Forms.Label lblVinSet;
        private System.Windows.Forms.Label lblVoutSet;
        private System.Windows.Forms.CheckBox chkReverse;
        private System.Windows.Forms.Label lblModeSta;
        private System.Windows.Forms.PictureBox picSysDirection;
        private System.Windows.Forms.DataVisualization.Charting.Chart chtMonitor;
        private VistaButtonTest.VistaButton btnON;
        private VistaButtonTest.VistaButton btnOFF;
        private System.Windows.Forms.PictureBox picVoutSta;
        private System.Windows.Forms.PictureBox picIoutSta;
        private System.Windows.Forms.PictureBox picPGSta;
        private System.Windows.Forms.Label lblIoutSta;
        private System.Windows.Forms.Label lblVoutSta;
        private System.Windows.Forms.Label lblPGSta;
        private System.Windows.Forms.PictureBox picVinSta;
        private System.Windows.Forms.Label lblTempSta;
        private System.Windows.Forms.PictureBox picIinSta;
        private System.Windows.Forms.Label lblVinSta;
        private System.Windows.Forms.Label lblIinSta;
        private System.Windows.Forms.PictureBox picTempSta;
        private System.IO.Ports.SerialPort serialPort1;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.GroupBox gpSetting;
        private System.Windows.Forms.GroupBox gpStatus;
        private System.Windows.Forms.GroupBox gpMonitor;
        private System.Windows.Forms.Label lblUintV;
        private System.Windows.Forms.Label lblUnitA;
        private System.Windows.Forms.RadioButton rdoIn;
        private System.Windows.Forms.RadioButton rdoOut;
        private System.Windows.Forms.TextBox txtTemp;
        private System.Windows.Forms.TextBox txtIin;
        private System.Windows.Forms.TextBox txtVin;
        private System.Windows.Forms.TextBox txtIout;
        private System.Windows.Forms.TextBox txtVout;
        private System.Windows.Forms.Label lblMode;
        private System.Windows.Forms.PictureBox picLogo;
        private System.Data.DataSet dataSet1;
        private System.Windows.Forms.Label lblTempActualScale;
        private System.Windows.Forms.Label lblVinActualScale;
        private System.Windows.Forms.Label lblIinActualScale;
        private System.Windows.Forms.Label lblVoutActualScale;
        private System.Windows.Forms.Label lblIoutActualScale;
        private VistaButtonTest.VistaButton btnCfg;
        private System.Windows.Forms.TextBox txtVoutIns;
        private System.Windows.Forms.TextBox txtIoutIns;
        private System.Windows.Forms.TextBox txtVinIns;
        private System.Windows.Forms.TextBox txtIinIns;
        private System.Windows.Forms.Label lbIiVolSca;
        private System.Windows.Forms.Label lbViVolSca;
        private System.Windows.Forms.Label lbIoVolSca;
        private System.Windows.Forms.Label lbVoVolSca;
        private System.Windows.Forms.Label lbIiPerSca;
        private System.Windows.Forms.Label lbViPerSca;
        private System.Windows.Forms.Label lbIoPerSca;
        private System.Windows.Forms.Label lbVoPerSca;
        private System.Windows.Forms.TextBox txtIoutDuty;
        private System.Windows.Forms.TextBox txtVoutDuty;
        private System.Windows.Forms.TextBox txtIoutVol;
        private System.Windows.Forms.TextBox txtVoutVol;
        private System.Windows.Forms.Label lbPWMvol;
        private System.Windows.Forms.Label lbPWMduty;
        private System.Windows.Forms.Label lbVoutPWM;
        private System.Windows.Forms.Label lbVinPWM;
        private System.Windows.Forms.Label lbIoutPWM;
        private System.Windows.Forms.Label lbIinPWM;
        private System.Windows.Forms.CheckBox chkPWMduty;
        private VistaButtonTest.VistaButton btnDitherEn;
        private System.Windows.Forms.Label lbDither;
        private VistaButtonTest.VistaButton btnDitherDis;
        private System.Windows.Forms.TextBox txtVinVol;
        private System.Windows.Forms.TextBox txtIinVol;
        private System.Windows.Forms.TextBox txtVinDuty;
        private System.Windows.Forms.TextBox txtIinDuty;
        private System.Windows.Forms.Timer timer2;
    }
}

