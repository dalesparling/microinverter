﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using System.Windows.Forms.DataVisualization.Charting;
using System.IO.Ports;
//using Excel = Microsoft.Office.Interop.Excel;
using System.IO;

namespace DEMO_ISL81X01
{
    public partial class Demo : Form
    {
        Configuration configuration;
        public double mvo = -24.818, vom = 83.957, mio = -7.142, iom = 22.436, mvi = -24.72, vim = 80.343, mii = -5.803, iim = 1.879;
        public double kvo = 24.2, voo = 0, k1 = 75.712, ix = 68.844, kvi = 24.2, vio = 0, k2 = 70.145, iy = 54.927;
        public double step_v = 0.1, step_i = 0.5;
        public double min_voutset = 0.8, max_voutset = 60, min_vinset = 9, max_vinset = 60, min_ioutset = 0, max_ioutset = 10, min_iinset = -10, max_iinset = 0;
        public double iin_reverse = 0;
        public double pwmPeriod = 319;
        public double chart_initial_value = -20;
        public string vout, vin, iout, iin;
        public double vin_calmax;

        bool IsOn, PG;
        int temperature = 0, sysDirection, direction_count;
        double VoutSet, VinSet, IoutSet, IinSet, VoutActual = 0, VinActual = 0, IoutActual = 0, IinActual = 0, IoutCon, IinCon;
        bool err_vout, err_iout, err_vin, err_iin;
        double dIsum = 0, dIavg = 0, dI = 0;
        List<double> dIlist;
        int count = 0;

        List<DateTime> xTimeList;
        List<double> yVoutList, yIoutList, yVinList, yIinList;
        int TIME_PERIOD = 60;

        DataTable tblData;
        DataColumn colTime, colVout, colVin, colIout, colIin, colTemp;
        string filepath, mainName;
        DataTable tblConfig;

        int portNum = 0;
        
        public class Define
        {
            public const int max_temperature = 85;
            public const int min_temperature = -40;

            public const double vcc = 3.3;

            //for release version: debug = 0; for debug version: debug = 1.
            public const int debug = 0;

            public const string vout = "vout";
            public const string vin = "vin";
            public const string iout = "iout";
            public const string iin = "iin";
         }

        public class Status
        {
            public const string normal = "";
            public const string button = "Please use '🔼' and '🔽' buttons to change the setting values.";
            public const string key = "Please press valid key.";
            public const string accuracy = "Accuracy: 0.1";
            public const string port = "No available serial port.\nPlease confirm the connection is successful with target board.";
            public const string vin1 = "Please ensure that the Vin input voltage is more than 9V.";
            public const string vin2 = "Please ensure that the Vin setting value is higher than the Vin input voltage.";
        }

        public class CommandCategory
        {
            public const byte START = 0x01;
            public const byte EN = 0x08;
            public const byte STOP = 0x02;
            public const byte VOUTCTRL = 0x03;
            public const byte IOUTCTRL = 0x04;
            public const byte VINCTRL = 0x05;
            public const byte IINCTRL = 0x06;
            public const byte MCUSENSE = 0x07;
            public const byte DITHEREN = 0x09;
            public const byte DITHERDIS = 0x0A;
            public const byte OK = 0x10;
            public const byte ERROR = 0xF1;
        }

        #region construct
        public Demo()
        {
            InitializeComponent();
            btnON.ForeColor = System.Drawing.Color.Gray;
            btnDitherEn.ForeColor = System.Drawing.Color.Gray;

            IsOn = false;
            sysDirection = 0;
            PG = true;
            err_iin = err_iout = err_vin = err_vout = true;

            dIlist = new List<double>();

            xTimeList = new List<DateTime>();
            yVoutList = new List<double>();
            yIoutList = new List<double>();
            yVinList = new List<double>();
            yIinList = new List<double>();

            InitializeChart();

            DataTable tblData = dataSet1.Tables.Add("Data");
            DataColumn colTime = tblData.Columns.Add("Time", typeof(DateTime));
            DataColumn colVoutSet = tblData.Columns.Add("Set Vout(V)", typeof(double));
            DataColumn colVout = tblData.Columns.Add("Actual Vout(V)", typeof(double));
            DataColumn colIoutSet = tblData.Columns.Add("Set Iout(A)", typeof(double));
            DataColumn colIout = tblData.Columns.Add("Actual Iout(A)", typeof(double));
            DataColumn colVinSet = tblData.Columns.Add("Set Vin(V)", typeof(double));
            DataColumn colVin = tblData.Columns.Add("Actual Vin(V)", typeof(double));
            DataColumn colIinSet = tblData.Columns.Add("Set Iin(A)", typeof(double));
            DataColumn colIin = tblData.Columns.Add("Actual Iin(A)", typeof(double));
            DataColumn colTemp = tblData.Columns.Add("Temp(degC)", typeof(int));

            mainName = "DEMO_ISL81X01_";

            if (Define.debug == 1)
            {
                chkPWMduty.Visible = lbPWMduty.Visible = lbPWMvol.Visible =
                    lbVoutPWM.Visible = lbVinPWM.Visible = lbIoutPWM.Visible = lbIinPWM.Visible =
                    txtVoutDuty.Visible = txtVinDuty.Visible = txtIoutDuty.Visible = txtIinDuty.Visible =
                    lbVoPerSca.Visible = lbViPerSca.Visible = lbIoPerSca.Visible = lbIiPerSca.Visible =
                    txtVoutVol.Visible = txtVinVol.Visible = txtIoutVol.Visible = txtIinVol.Visible =
                    lbVoVolSca.Visible = lbViVolSca.Visible = lbIoVolSca.Visible = lbIiVolSca.Visible = true;
            }
            else
            {
                chkPWMduty.Visible = lbPWMduty.Visible = lbPWMvol.Visible =
                    lbVoutPWM.Visible = lbVinPWM.Visible = lbIoutPWM.Visible = lbIinPWM.Visible =
                    txtVoutDuty.Visible = txtVinDuty.Visible = txtIoutDuty.Visible = txtIinDuty.Visible =
                    lbVoPerSca.Visible = lbViPerSca.Visible = lbIoPerSca.Visible = lbIiPerSca.Visible =
                    txtVoutVol.Visible = txtVinVol.Visible = txtIoutVol.Visible = txtIinVol.Visible =
                    lbVoVolSca.Visible = lbViVolSca.Visible = lbIoVolSca.Visible = lbIiVolSca.Visible = false;
            }

            System.IO.FileInfo fi = new System.IO.FileInfo("config.csv");
            if (fi.Directory.Exists)
            {
                tblConfig = OpenCSV("config.csv");
                this.mvo = Convert.ToDouble(tblConfig.Rows[0][0]);
                this.vom = Convert.ToDouble(tblConfig.Rows[0][1]);
                this.mio = Convert.ToDouble(tblConfig.Rows[0][2]);
                this.iom = Convert.ToDouble(tblConfig.Rows[0][3]);
                this.mvi = Convert.ToDouble(tblConfig.Rows[0][4]);
                this.vim = Convert.ToDouble(tblConfig.Rows[0][5]);
                this.mii = Convert.ToDouble(tblConfig.Rows[0][6]);
                this.iim = Convert.ToDouble(tblConfig.Rows[0][7]);
                this.kvo = Convert.ToDouble(tblConfig.Rows[0][8]);
                this.voo = Convert.ToDouble(tblConfig.Rows[0][9]);
                this.k1 = Convert.ToDouble(tblConfig.Rows[0][10]);
                this.ix = Convert.ToDouble(tblConfig.Rows[0][11]);
                this.kvi = Convert.ToDouble(tblConfig.Rows[0][12]);
                this.vio = Convert.ToDouble(tblConfig.Rows[0][13]);
                this.k2 = Convert.ToDouble(tblConfig.Rows[0][14]);
                this.iy = Convert.ToDouble(tblConfig.Rows[0][15]);
                this.min_voutset = Convert.ToDouble(tblConfig.Rows[0][16]);
                this.max_voutset = Convert.ToDouble(tblConfig.Rows[0][17]);
                this.min_ioutset = Convert.ToDouble(tblConfig.Rows[0][18]);
                this.max_ioutset = Convert.ToDouble(tblConfig.Rows[0][19]);
                this.min_vinset = Convert.ToDouble(tblConfig.Rows[0][20]);
                this.max_vinset = Convert.ToDouble(tblConfig.Rows[0][21]);
                this.min_iinset = Convert.ToDouble(tblConfig.Rows[0][22]);
                this.max_iinset = Convert.ToDouble(tblConfig.Rows[0][23]);
                this.step_v = Convert.ToDouble(tblConfig.Rows[0][24]);
                this.step_i = Convert.ToDouble(tblConfig.Rows[0][25]);
                this.iin_reverse = Convert.ToDouble(tblConfig.Rows[0][26]);
                this.pwmPeriod = Convert.ToDouble(tblConfig.Rows[0][27]);

                if (mvi > 0)
                {
                    vin_calmax = CutDecimalWithOne_sign(mvi * Define.vcc + vim);
                }
                else
                {
                    vin_calmax = CutDecimalWithOne_sign(vim);
                }
            }
            this.chart_initial_value = this.min_iinset - 10;
            vout = "Vout range: " + min_voutset.ToString() + "V - " + max_voutset.ToString() + "V";
            vin = "Vin range: " + min_vinset.ToString() + "V - " + max_vinset.ToString() + "V";
            iout = "Iout range: 0A - " + max_ioutset.ToString() + "A";
            iin = "Iin range: " + min_iinset.ToString() + "A - 0A";

            txtVoutSet.Text = "12";
            txtIoutSet.Text = "3";
            txtVinSet.Text = "11.7";
            txtIinSet.Text = "-1";
        }

        protected void InitializeChart()
        {
            chtMonitor.ChartAreas["ChartArea1"].AxisX.LabelStyle.Format = "HH:mm:ss";
            chtMonitor.ChartAreas["ChartArea1"].AxisX.LabelAutoFitStyle = LabelAutoFitStyles.None;
            chtMonitor.ChartAreas["ChartArea1"].AxisX.Interval = 0.0001;

            if ((int)this.min_iinset == this.min_iinset)
            {
                chtMonitor.ChartAreas["ChartArea1"].AxisY2.Minimum = this.min_iinset;
            }
            else
            {
                chtMonitor.ChartAreas["ChartArea1"].AxisY2.Minimum = (int)this.min_iinset - 1;
            }
            chtMonitor.ChartAreas["ChartArea1"].AxisY2.Maximum = (int)this.max_ioutset;
            chtMonitor.ChartAreas["ChartArea1"].AxisY.Minimum = (int)(this.min_vinset > this.min_voutset ? this.min_voutset : this.min_vinset);
            double temp;
            temp = (this.max_vinset > this.max_voutset ? this.max_vinset : this.max_voutset);
            if ((int)temp == temp)
            {
                chtMonitor.ChartAreas["ChartArea1"].AxisY.Maximum = temp;
            }
            else
            {
                chtMonitor.ChartAreas["ChartArea1"].AxisY.Maximum = (int)temp + 1;
            }

            chtMonitor.Series["Vout"].Points.Clear();
            chtMonitor.Series["Vin"].Points.Clear();
            chtMonitor.Series["Iout"].Points.Clear();
            chtMonitor.Series["Iin"].Points.Clear();

            xTimeList.Clear();
            yIinList.Clear();
            yIoutList.Clear();
            yVinList.Clear();
            yVoutList.Clear();

            DateTime dt = DateTime.Now;
            dt = dt.AddSeconds(-TIME_PERIOD);

            for (int i = 0; i <= TIME_PERIOD; i++)
            {
                xTimeList.Add(dt);
                yIinList.Add(chart_initial_value);
                yIoutList.Add(chart_initial_value);
                yVinList.Add(chart_initial_value);
                yVoutList.Add(chart_initial_value);

                chtMonitor.Series["Vout"].Points.AddXY(xTimeList[i], yVoutList[i]);
                chtMonitor.Series["Iout"].Points.AddXY(xTimeList[i], yIoutList[i]);
                chtMonitor.Series["Vin"].Points.AddXY(xTimeList[i], yVinList[i]);
                chtMonitor.Series["Iin"].Points.AddXY(xTimeList[i], yIinList[i]);

                dt = dt.AddSeconds(1);
            }
        }

        private void chtMonitor_GetToolTipText(object sender, ToolTipEventArgs e)
        {
            if (e.HitTestResult.ChartElementType == ChartElementType.DataPoint)
            {
                int i = e.HitTestResult.PointIndex;
                DataPoint dp = e.HitTestResult.Series.Points[i];
                DateTime dt = DateTime.FromOADate(dp.XValue);

                e.Text = string.Format("Time:{0};Value:{1:F1} ", dt, dp.YValues[0]);
            }
        }
        #endregion

        #region close
        private void IntersilDemo_FormClosing(object sender, FormClosingEventArgs e)
        {
            Packet pkt = new Packet(0);
            pkt.command = CommandCategory.STOP;
            pkt.sum = GetCheckSum(pkt);
            Send(pkt);
        }
        #endregion

        #region configuration
        private void btnCfg_Click(object sender, EventArgs e)
        {
            configuration = new Configuration();
            configuration.Owner = this;

            configuration.Show();
        }
        #endregion

        #region connect
        private void btnConnect_Click(object sender, EventArgs e)
        {
            if (btnConnect.ButtonText == "CONNECT")
            {
                string[] portNames = System.IO.Ports.SerialPort.GetPortNames();
                if (portNames.Length == 0)
                {
                    MessageBox.Show(Status.port, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }
                int i;
                for (i = 0; i < portNames.Length; i++)
                {
                    serialPort1.Close();
                    if (!serialPort1.IsOpen)
                    {
                        serialPort1.PortName = portNames[i];
                        serialPort1.BaudRate = 115200;
                        serialPort1.DataBits = 8;
                        serialPort1.StopBits = StopBits.One;
                        serialPort1.Parity = Parity.None;
                        serialPort1.WriteTimeout = 10;
                        serialPort1.ReadTimeout = 10;

                        try
                        {
                            serialPort1.Open();
                        }
                        catch (System.Exception ex)
                        {
                            //MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        }
                    }
                    if (serialPort1.IsOpen)
                    {
                        Packet pkt = new Packet(0);
                        pkt.command = CommandCategory.STOP;
                        pkt.sum = GetCheckSum(pkt);

                        serialPort1.DiscardInBuffer();
                        if (Send(pkt))
                        {
                            System.Threading.Thread.Sleep(20);
                            Packet pktReturn = new Packet();
                            if (Receive(ref pktReturn))
                            {
                                if (IsCorrectCheckSum(pktReturn))
                                {
                                    if (pktReturn.command == CommandCategory.OK)
                                    {
                                        gpSetting.Enabled = true;
                                        btnON.ForeColor = System.Drawing.Color.Black;
                                        btnDitherEn.ForeColor = System.Drawing.Color.Black;
                                        btnCfg.Enabled = false;
                                        btnCfg.ForeColor = System.Drawing.Color.Gray;
                                        btnConnect.ButtonText = "DISCONNECT";
                                        timer1.Enabled = true;

                                        InitializeChart();
                                        chkReverse_CheckedChanged(sender, e);

                                        break;
                                    }
                                }
                            }
                        }
                    }
                }
                if (i == portNames.Length && btnConnect.ButtonText == "CONNECT")
                {
                    MessageBox.Show(Status.port, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }
            }
            else
            {
                if (serialPort1.IsOpen)
                {
                    Packet pkt = new Packet(0);
                    pkt.command = CommandCategory.STOP;
                    pkt.sum = GetCheckSum(pkt);

                    serialPort1.DiscardInBuffer();
                    if (Send(pkt))
                    {
                        System.Threading.Thread.Sleep(20);
                        Packet pktReturn = new Packet();
                        if (Receive(ref pktReturn))
                        {
                            if (IsCorrectCheckSum(pktReturn))
                            {
                                if (pktReturn.command == CommandCategory.OK)
                                {
                                    try
                                    {
                                        serialPort1.Close();
                                    }
                                    catch (System.Exception ex)
                                    {
                                        //MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                                    }

                                    gpMonitor.Enabled = gpSetting.Enabled = gpStatus.Enabled = false;
                                    btnON.ForeColor = System.Drawing.Color.Gray;
                                    btnDitherEn.ForeColor = System.Drawing.Color.Gray;
                                    btnConnect.ButtonText = "CONNECT";
                                    btnCfg.Enabled = true;
                                    btnCfg.ForeColor = System.Drawing.Color.Black;

                                    timer1.Enabled = false;
                                    btnON.Visible = true;
                                    btnDitherEn.Visible = true;
                                    btnOFF.Visible = false;
                                    btnDitherDis.Visible = false;
                                    IsOn = false;
                                    sysDirection = 0;
                                    PG = true;
                                }
                            }
                        }
                    }
                }
            }
        }
        #endregion

        #region setting
        private void chkReverse_CheckedChanged(object sender, EventArgs e)
        {
            if (chkReverse.Checked)
            {
                txtIinSet.ReadOnly = true;
                btnIinDe.Enabled = btnIinIn.Enabled = false;
                txtVinSet.ReadOnly = true;
                btnVinDe.Enabled = btnVinIn.Enabled = false;
                txtVinDuty.ReadOnly = txtVinVol.ReadOnly = txtIinDuty.ReadOnly = txtIinVol.ReadOnly = true;
                lbVinPWM.Enabled = lbViPerSca.Enabled = lbViVolSca.Enabled = lbIinPWM.Enabled = lbIiPerSca.Enabled = lbIiVolSca.Enabled = false;
            }
            else
            {
                txtIinSet.ReadOnly = false;
                txtVinSet.ReadOnly = false;
                txtVinDuty.ReadOnly = txtVinVol.ReadOnly = txtIinDuty.ReadOnly = txtIinVol.ReadOnly = false;
                if (!chkPWMduty.Checked)
                {
                    btnIinDe.Enabled = btnIinIn.Enabled = true;
                    btnVinDe.Enabled = btnVinIn.Enabled = true;
                }
                else
                {
                    lbVinPWM.Enabled = lbViPerSca.Enabled = lbViVolSca.Enabled = lbIinPWM.Enabled = lbIiPerSca.Enabled = lbIiVolSca.Enabled = true;
                }
            }
        }

        private void txtVoutSet_TextChanged(object sender, EventArgs e)
        {
            if (IsOn)
            {
                bool err = true;
                double tempValue = System.Convert.ToDouble(txtVoutSet.Text);
                double duty = GetDutyValue(tempValue, Define.vout);
  
                Packet pkt = new Packet(3);
                pkt.data[0] = (byte)(duty * 100 % 100);
                pkt.data[1] = (byte)(duty * 10000 % 100);
                pkt.data[2] = (byte)(duty * 100000 % 10);
                pkt.command = CommandCategory.VOUTCTRL;
                pkt.sum = GetCheckSum(pkt);

                serialPort1.DiscardInBuffer();
                if (Send(pkt))
                {
                    System.Threading.Thread.Sleep(20);
                    Packet pktReturn = new Packet();
                    if (Receive(ref pktReturn))
                    {
                        if (IsCorrectCheckSum(pktReturn))
                        {
                            if (pktReturn.command == CommandCategory.OK)
                            {
                                VoutSet = tempValue;
                                this.txtVoutDuty.Text = (duty * 100).ToString("F3");
                                err = false;                                
                            }
                        }
                    }
                }
                if (err)
                {
                    txtVoutSet.Text = VoutSet.ToString();
                }
            }
            else
            {
                if (!string.IsNullOrEmpty(txtVoutSet.Text))
                {
                    if (DotJudge(sender))
                    {
                        VoutSet = System.Convert.ToDouble(txtVoutSet.Text);
                        if (!chkPWMduty.Checked)
                        {
                            double duty = GetDutyValue(VoutSet, Define.vout);
                            this.txtVoutDuty.Text = (duty * 100).ToString("F3");
                        }
                        if (RangeJudge(VoutSet, this.max_voutset, this.min_voutset))
                        {
                            err_vout = false;
                            RefreshStatusStrip(Status.normal);
                        }
                        else
                        {
                            RefreshStatusStrip(this.vout);
                            err_vout = true;
                        }
                    }
                    else
                    {
                        txtVoutSet.Text = VoutSet.ToString();
                        RefreshStatusStrip(Status.accuracy);
                    }
                }
                else
                {
                    err_vout = true;
                }
            }
        }

        private void txtIoutSet_TextChanged(object sender, EventArgs e)
        {
            if (IsOn)
            {
                bool err = true;
                double tempValue = System.Convert.ToDouble(txtIoutSet.Text);
                double duty = GetDutyValue(tempValue, Define.iout);

                Packet pkt = new Packet(3);
                pkt.data[0] = (byte)(duty * 100 % 100);
                pkt.data[1] = (byte)(duty * 10000 % 100);
                pkt.data[2] = (byte)(duty * 100000 % 10);
                pkt.command = CommandCategory.IOUTCTRL;
                pkt.sum = GetCheckSum(pkt);

                serialPort1.DiscardInBuffer();
                if (Send(pkt))
                {
                    System.Threading.Thread.Sleep(20);
                    Packet pktReturn = new Packet();
                    if (Receive(ref pktReturn))
                    {
                        if (IsCorrectCheckSum(pktReturn))
                        {
                            if (pktReturn.command == CommandCategory.OK)
                            {
                                IoutSet = tempValue;
                                this.txtIoutDuty.Text = (duty * 100).ToString("F3");
                                err = false;
                            }
                        }
                    }
                }
                if (err)
                {
                    txtIoutSet.Text = IoutSet.ToString();
                }
            }
            else
            {
                if (!string.IsNullOrEmpty(txtIoutSet.Text))
                {
                    if (DotJudge(sender))
                    {
                        IoutSet = System.Convert.ToDouble(txtIoutSet.Text);
                        if (!chkPWMduty.Checked)
                        {
                            double duty = GetDutyValue(IoutSet, Define.iout);
                            this.txtIoutDuty.Text = (duty * 100).ToString("F3");
                        }
                        if (RangeJudge(IoutSet, this.max_ioutset, 0))
                        {
                            err_iout = false;
                            RefreshStatusStrip(Status.normal);
                        }
                        else
                        {
                            RefreshStatusStrip(this.iout);
                            err_iout = true;
                        }
                    }
                    else
                    {
                        txtIoutSet.Text = IoutSet.ToString();
                        RefreshStatusStrip(Status.accuracy);
                    }
                }
                else
                {
                    err_iout = true;
                }
            }
        }

        private void txtVinSet_TextChanged(object sender, EventArgs e)
        {
            if (IsOn)
            {
                bool err = true;
                double tempValue = System.Convert.ToDouble(txtVinSet.Text);
                double duty = GetDutyValue(tempValue, Define.vin);

                Packet pkt = new Packet(3);
                pkt.data[0] = (byte)(duty * 100 % 100);
                pkt.data[1] = (byte)(duty * 10000 % 100);
                pkt.data[2] = (byte)(duty * 100000 % 10);
                pkt.command = CommandCategory.VINCTRL;
                pkt.sum = GetCheckSum(pkt);

                serialPort1.DiscardInBuffer();
                if (Send(pkt))
                {
                    System.Threading.Thread.Sleep(20);
                    Packet pktReturn = new Packet();
                    if (Receive(ref pktReturn))
                    {
                        if (IsCorrectCheckSum(pktReturn))
                        {
                            if (pktReturn.command == CommandCategory.OK)
                            {
                                VinSet = tempValue;
                                this.txtVinDuty.Text = (duty * 100).ToString("F3");
                                err = false;
                            }
                        }
                    }
                }
                if (err)
                {
                    txtVinSet.Text = VinSet.ToString();
                }
            }
            else
            {
                if (!chkReverse.Checked)
                {
                    if (!string.IsNullOrEmpty(txtVinSet.Text))
                    {
                        if (DotJudge(sender))
                        {
                            VinSet = System.Convert.ToDouble(txtVinSet.Text);
                            if (!chkPWMduty.Checked)
                            {
                                double duty = GetDutyValue(VinSet, Define.vin);
                                this.txtVinDuty.Text = (duty * 100).ToString("F3");
                            }
                            if (RangeJudge(VinSet, this.max_vinset, this.min_vinset))
                            {
                                err_vin = false;
                                RefreshStatusStrip(Status.normal);
                            }
                            else
                            {
                                if (chkReverse.Checked)
                                {
                                    err_vin = false;
                                    RefreshStatusStrip(Status.normal);
                                }
                                else
                                {
                                    RefreshStatusStrip(this.vin);
                                    err_vin = true;
                                }
                            }
                        }
                        else
                        {
                            txtVinSet.Text = VinSet.ToString();
                            RefreshStatusStrip(Status.accuracy);
                        }
                    }
                    else
                    {
                        err_vin = true;
                    }
                }
            }
        }

        private void txtIinSet_TextChanged(object sender, EventArgs e)
        {
            if (IsOn)
            {
                bool err = true;
                double tempValue = System.Convert.ToDouble(txtIinSet.Text);
                double duty = GetDutyValue(tempValue, Define.iin);

                Packet pkt = new Packet(3);
                pkt.data[0] = (byte)(duty * 100 % 100);
                pkt.data[1] = (byte)(duty * 10000 % 100);
                pkt.data[2] = (byte)(duty * 100000 % 10);
                pkt.command = CommandCategory.IINCTRL;
                pkt.sum = GetCheckSum(pkt);

                serialPort1.DiscardInBuffer();
                if (Send(pkt))
                {
                    System.Threading.Thread.Sleep(20);
                    Packet pktReturn = new Packet();
                    if (Receive(ref pktReturn))
                    {
                        if (IsCorrectCheckSum(pktReturn))
                        {
                            if (pktReturn.command == CommandCategory.OK)
                            {
                                IinSet = tempValue;
                                this.txtIinDuty.Text = (duty * 100).ToString("F3");
                                err = false;
                            }
                        }
                    }
                }
                if (err)
                {
                    txtIinSet.Text = IinSet.ToString();
                }
            }
            else
            {
                if (!chkReverse.Checked)
                {
                    if (!string.IsNullOrEmpty(txtIinSet.Text))
                    {
                        if (DotJudge(sender))
                        {
                            if ((txtIinSet.Text == "-") || (txtIinSet.Text == "-."))
                            {
                                err_iin = true;
                            }
                            else
                            {
                                IinSet = System.Convert.ToDouble(txtIinSet.Text);
                                if (!chkPWMduty.Checked)
                                {
                                    double duty = GetDutyValue(IinSet, Define.iin);
                                    this.txtIinDuty.Text = (duty * 100).ToString("F3");
                                }
                                if (RangeJudge(IinSet, 0, this.min_iinset))
                                {
                                    err_iin = false;
                                    RefreshStatusStrip(Status.normal);
                                }
                                else
                                {
                                    RefreshStatusStrip(this.iin);
                                    err_iin = true;
                                }
                            }
                        }
                        else
                        {
                            txtIinSet.Text = IinSet.ToString();
                            RefreshStatusStrip(Status.accuracy);
                        }
                    }
                    else
                    {
                        err_iin = true;
                    }
                }
            }
        }
        
        private void btnVoutDe_Click(object sender, EventArgs e)
        {
            double temp_value = VoutSet - this.step_v;
            if (RangeJudge(temp_value, this.max_voutset, this.min_voutset))
            {
                txtVoutSet.Text = temp_value.ToString();
                RefreshStatusStrip(Status.normal);

                if (IsOn)
                {
                    if (VinActual * max_iinset * 1 / temp_value < IoutSet)
                    {
                        txtIoutSet.Text = CutDecimalWithOne(VinActual * max_iinset * 1 / temp_value);
                    }
                    if (!chkReverse.Checked)
                    {
                        if (temp_value * min_ioutset / (VinSet * 0.95) > IinSet)
                        {
                            txtIinSet.Text = CutDecimalWithOne(temp_value * min_ioutset / (VinSet * 0.95));
                        }
                    }
                }
            }
            else
            {
                RefreshStatusStrip(this.vout);
            }
        }

        private void btnVoutIn_Click(object sender, EventArgs e)
        {
            double temp_value = VoutSet + this.step_v;
            if (RangeJudge(temp_value, this.max_voutset, this.min_voutset))
            {
                txtVoutSet.Text = temp_value.ToString();
                RefreshStatusStrip(Status.normal);

                if (IsOn)
                {
                    if (VinActual * max_iinset * 1 / temp_value < IoutSet)
                    {
                        txtIoutSet.Text = CutDecimalWithOne(VinActual * max_iinset * 1 / temp_value);
                    }
                    if (!chkReverse.Checked)
                    {
                        if (temp_value * min_ioutset / (VinSet * 0.95) > IinSet)
                        {
                            txtIinSet.Text = CutDecimalWithOne(temp_value * min_ioutset / (VinSet * 0.95));
                        }
                    }
                }
            }
            else
            {
                RefreshStatusStrip(this.vout);
            }
        }

        private void btnIoutDe_Click(object sender, EventArgs e)
        {
            double temp_value = IoutSet - this.step_i;
            if (RangeJudge(temp_value, this.max_ioutset, 0))
            {
                RefreshStatusStrip(Status.normal);

                if (IsOn && (VinActual * max_iinset * 1 / VoutSet < temp_value))
                {
                    string ErrString = "Please input valid setting values.\n";
                    ErrString += "\n" +
                    "Iout range: 0A - " + CutDecimalWithOne(VinActual * max_iinset * 1 / VoutSet) + "A" +
                    " (when Vin input voltage is " + CutDecimalWithOne(VinActual) + "V and Vout setting value is " + VoutSet.ToString() + "V)";
                    MessageBox.Show(ErrString, "Tips", MessageBoxButtons.OK);
                }
                else
                {
                    txtIoutSet.Text = temp_value.ToString();
                }
            }
            else
            {
                RefreshStatusStrip(this.iout);
            }
        }

        private void btnIoutIn_Click(object sender, EventArgs e)
        {
            double temp_value = IoutSet + this.step_i;
            if (RangeJudge(temp_value, this.max_ioutset, 0))
            {
                RefreshStatusStrip(Status.normal);

                if (IsOn && (VinActual * max_iinset * 1 / VoutSet < temp_value))
                {
                    string ErrString = "Please input valid setting values.\n";
                    ErrString += "\n" +
                    "Iout range: 0A - " + CutDecimalWithOne(VinActual * max_iinset * 1 / VoutSet) + "A" +
                    " (when Vin input voltage is " + CutDecimalWithOne(VinActual) + "V and Vout setting value is " + VoutSet.ToString() + "V)";
                    MessageBox.Show(ErrString, "Tips", MessageBoxButtons.OK);
                }
                else
                {
                    txtIoutSet.Text = temp_value.ToString();
                }
            }
            else
            {
                RefreshStatusStrip(this.iout);
            }
        }

        private void btnVinDe_Click(object sender, EventArgs e)
        {
            double temp_value = VinSet - this.step_v;
            if (RangeJudge(temp_value, this.max_vinset, this.min_vinset))
            {
                RefreshStatusStrip(Status.normal);

                if (IsOn)
                {
                    //if (temp_value >= VinActual)
                    //{
                        txtVinSet.Text = temp_value.ToString();

                        if (!chkReverse.Checked && (VoutSet * min_ioutset / (temp_value * 0.95) > IinSet))
                        {
                            txtIinSet.Text = CutDecimalWithOne(VoutSet * min_ioutset / (temp_value * 0.95));
                        }
                    //}
                    //else
                    //{
                    //    MessageBox.Show(Status.vin2, "Tips", MessageBoxButtons.OK);
                    //}
                }
                else
                {
                    txtVinSet.Text = temp_value.ToString();
                }
            }
            else
            {
                RefreshStatusStrip(this.vin);
            }
            count = 0;
        }

        private void btnVinIn_Click(object sender, EventArgs e)
        {
            double temp_value = VinSet + this.step_v;
            if (RangeJudge(temp_value, this.max_vinset, this.min_vinset))
            {
                txtVinSet.Text = temp_value.ToString();
                RefreshStatusStrip(Status.normal);

                if (!chkReverse.Checked && IsOn && (VoutSet * min_ioutset / (temp_value * 0.95) > IinSet))
                {
                    txtIinSet.Text = CutDecimalWithOne(VoutSet * min_ioutset / (temp_value * 0.95));
                }
            }
            else
            {
                RefreshStatusStrip(this.vin);
            }
            count = 0;
        }

        private void btnIinDe_Click(object sender, EventArgs e)
        {
            double temp_value = IinSet - this.step_i;
            if (RangeJudge(temp_value, 0, this.min_iinset))
            {
                RefreshStatusStrip(Status.normal);

                if (!chkReverse.Checked && IsOn && (VoutSet * min_ioutset / (VinSet * 0.95) > temp_value))
                {
                    string ErrString = "Please input valid setting values.\n";
                    ErrString += "\n" +
                    "Iin range: " + CutDecimalWithOne(VoutSet * min_ioutset / (VinSet * 0.95)) + "A - 0A" +
                    " (when Vin setting value is " + VinSet.ToString() + "V and Vout setting value is " + VoutSet.ToString() + "V)";
                    MessageBox.Show(ErrString, "Tips", MessageBoxButtons.OK);
                }
                else
                {
                    txtIinSet.Text = temp_value.ToString();
                }
            }
            else
            {
                RefreshStatusStrip(this.iin);
            }
            count = 0;
        }

        private void btnIinIn_Click(object sender, EventArgs e)
        {
            double temp_value = IinSet + this.step_i;
            if (RangeJudge(temp_value, 0, this.min_iinset))
            {
                RefreshStatusStrip(Status.normal);

                if (!chkReverse.Checked && IsOn && (VoutSet * min_ioutset / (VinSet * 0.95) > temp_value))
                {
                    string ErrString = "Please input valid setting values.\n";
                    ErrString += "\n" +
                    "Iin range: " + CutDecimalWithOne(VoutSet * min_ioutset / (VinSet * 0.95)) + "A - 0A" +
                    " (when Vin setting value is " + VinSet.ToString() + "V and Vout setting value is " + VoutSet.ToString() + "V)";
                    MessageBox.Show(ErrString, "Tips", MessageBoxButtons.OK);
                }
                else
                {
                    txtIinSet.Text = temp_value.ToString();
                }
            }
            else
            {
                RefreshStatusStrip(this.iin);
            }
            count = 0;
        }

        private void textBox_KeyPress(object sender, KeyPressEventArgs e)
        {
            int a = (int)e.KeyChar;
            if (IsOn)
            {
                e.Handled = true;
                RefreshStatusStrip(Status.button);
                return;
            }
            else if ((a < 48 || a > 57) && a != 8 && a != 127 && a != 46)
            {
                e.Handled = true;
                RefreshStatusStrip(Status.key);
            }
            else if (a == 46 && ((TextBox)sender).Text.IndexOf('.') != -1)
            {
                e.Handled = true;
                RefreshStatusStrip(Status.key);
            }
            else if (a == 46 && ((TextBox)sender).Text == "")
            {
                e.Handled = true;
                RefreshStatusStrip(Status.key);
            }
            else
            {
                RefreshStatusStrip(Status.normal);
            }
        }

        private void txtIinSet_KeyPress(object sender, KeyPressEventArgs e)
        {
            int a = (int)e.KeyChar;
            if (IsOn)
            {
                e.Handled = true;
                RefreshStatusStrip(Status.button);
                return;
            }
            else if ((a < 48 || a > 57) && a != 8 && a != 127 && a != 46 && a != 45)
            {
                e.Handled = true;
                RefreshStatusStrip(Status.key);
            }
            else if (a == 46 && ((TextBox)sender).Text.IndexOf('.') != -1)
            {
                e.Handled = true;
                RefreshStatusStrip(Status.key);
            }
            else if (a == 46 && ((TextBox)sender).Text == "")
            {
                e.Handled = true;
                RefreshStatusStrip(Status.key);
            }
            else if (a == 45 && ((TextBox)sender).Text != "")
            {
                e.Handled = true;
                RefreshStatusStrip(Status.key);
            }
            else
            {
                RefreshStatusStrip(Status.normal);
            }
        }

        private void txtBox_DoubleClick(object sender, EventArgs e)
        {
            MessageBox.Show(this.vout + "\n" + this.iout + "\n" + this.vin + "\n" + this.iin + "\n\n" + Status.accuracy, "Tips", MessageBoxButtons.OK);
        }

        protected bool RangeJudge(double value, double max, double min)
        {
            if (value > max || value < min)
            {
                return false;
            }
            else
            {
                return true;
            }
        }

        protected bool RangeJudge(int value, int max, int min)
        {
            if (value > max || value < min)
            {
                return false;
            }
            else
            {
                return true;
            }
        }

        /// <summary>
        /// 小数点后面数字的个数大于1位返回false
        /// </summary>
        /// <param name="sender"></param>
        /// <returns></returns>
        protected bool DotJudge(object sender)
        {
            int i, j = 0;
            bool dot = false;
            TextBox tb = sender as TextBox;
            if (tb.Text == ".")
            {
                return false;
            }
            for (i = 1; i < tb.Text.Length; i++)
            {
                if (dot)
                {
                    j++;
                }
                if (j > 1)
                {
                    return false;
                }
                if (tb.Text[i] == '.')
                {
                    dot = true;
                }
            }
            return true;
        }

        protected void RefreshStatusStrip(string type)
        {
            toolStripStatusLabel1.Text = type;
        }

        protected double GetDutyValue(double value, string status)
        {
            double duty = 0;
            switch (status)
            {
                case Define.vout:
                    duty = (value - this.vom) / this.mvo / Define.vcc;
                    break;
                case Define.iout:
                    duty = (value - this.iom) / this.mio / Define.vcc;
                    IoutCon = (value - this.iom) / this.mio;
                    break;
                case Define.vin:
                    duty = (value - this.vim) / this.mvi / Define.vcc;
                    break;
                case Define.iin:
                    duty = (value - this.iim) / this.mii / Define.vcc;
                    IinCon = (value - this.iim) / this.mii;
                    break;
                default:
                    break;
            }
            if (duty < 0)
            {
                duty = 0;
            }
            if(duty > 1)
            {
                duty = 1;
            }
            return duty;
        }

        protected string CutDecimalWithOne(double value)
        {
            string str = value.ToString();
            int count = str.LastIndexOf(".");
            if ((str.Length - count - 1) > 1 && count != -1)
            {
                str = Double.Parse(str.Substring(0, count + 2)).ToString();
            }

            return str;
        }

        protected double CutDecimalWithOne_sign(double value)
        {
            string str = value.ToString();
            int count = str.LastIndexOf(".");
            if ((str.Length - count - 1) > 1 && count != -1)
            {
                if (value > 0)
                {
                    value = Double.Parse(str.Substring(0, count + 2));
                }
                else
                {
                    value = Double.Parse(str.Substring(0, count + 2)) - 0.1;
                }
            }

            return value;
        }
        #endregion

        #region ON/OFF
        private void btnON_Click(object sender, EventArgs e)
        {
            string ErrString = "Please input valid setting values.\n";
            if (err_vout || err_vin || err_iout || err_iin)
            {
                if (err_vout)
                {
                    ErrString += "\n" + vout;
                }
                if (err_iout)
                {
                    ErrString += "\n" + iout;
                }
                if (err_vin)
                {
                    ErrString += "\n" + vin;
                }
                if (err_iin)
                {
                    ErrString += "\n" + iin;
                }
                MessageBox.Show(ErrString, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else
            {
                double duty_iin, duty_vin;
                double duty_vout = GetDutyValue(VoutSet, Define.vout);
                double duty_iout = GetDutyValue(IoutSet, Define.iout);
                if (chkReverse.Checked)
                {
                    duty_iin = GetDutyValue(iin_reverse, Define.iin);
                    if ((max_vinset + 5) > vin_calmax)
                    {
                        duty_vin = GetDutyValue(vin_calmax, Define.vin);
                    }
                    else
                    {
                        duty_vin = GetDutyValue(max_vinset + 5, Define.vin);
                    }
                }
                else
                {
                    if (VinSet + 1 > max_vinset)
                    {
                        duty_vin = GetDutyValue(VinSet, Define.vin);
                    }
                    else
                    {
                        duty_vin = GetDutyValue(VinSet + 1, Define.vin);
                    }
                    duty_iin = GetDutyValue(IinSet, Define.iin);
                }

                Packet pkt = new Packet(15);
                pkt.data[0] = (byte)(duty_vout * 100 % 100);
                pkt.data[1] = (byte)(duty_vout * 10000 % 100);
                pkt.data[2] = (byte)(duty_vout * 100000 % 10);
                pkt.data[3] = (byte)(duty_iout * 100 % 100);
                pkt.data[4] = (byte)(duty_iout * 10000 % 100);
                pkt.data[5] = (byte)(duty_iout * 100000 % 10);
                pkt.data[6] = (byte)(duty_vin * 100 % 100);
                pkt.data[7] = (byte)(duty_vin * 10000 % 100);
                pkt.data[8] = (byte)(duty_vin * 100000 % 10);
                pkt.data[9] = (byte)(duty_iin * 100 % 100);
                pkt.data[10] = (byte)(duty_iin * 10000 % 100);
                pkt.data[11] = (byte)(duty_iin * 100000 % 10);
                pkt.data[12] = (byte)(pwmPeriod / 1000);
                pkt.data[13] = (byte)(pwmPeriod % 1000 / 10);
                pkt.data[14] = (byte)(pwmPeriod % 10);
                pkt.command = CommandCategory.START;
                pkt.sum = GetCheckSum(pkt);

                serialPort1.DiscardInBuffer();
                if (Send(pkt))
                {
                    System.Threading.Thread.Sleep(20);
                    Packet pktReturn = new Packet();
                    if (Receive(ref pktReturn))
                    {
                        if (IsCorrectCheckSum(pktReturn))
                        {
                            if (pktReturn.command == CommandCategory.OK)
                            {
                                VinActual = (double)(pktReturn.data[2] * 256 + pktReturn.data[3]) * this.kvi / 1000 + this.vio;
                                if (VinActual >= 8.5)
                                {
                                    if (VinActual <= VinSet || chkReverse.Checked)
                                    {
                                        bool err_ioutsec = false, err_iinsec = false;
                                        if (VinActual * max_iinset * 1 / VoutSet < IoutSet)
                                        {
                                            err_ioutsec = true;
                                            ErrString += "\n" +
                                                "Iout range: 0A - " + CutDecimalWithOne(VinActual * max_iinset * 1 / VoutSet) + "A" +
                                                " (when Vin input voltage is " + CutDecimalWithOne(VinActual) + "V and Vout setting value is " + VoutSet.ToString() + "V)";
                                        }
                                        if (!chkReverse.Checked)
                                        {
                                            if (VoutSet * min_ioutset / (VinSet * 0.95) > IinSet)
                                            {
                                                err_iinsec = true;
                                                ErrString += "\n" +
                                                    "Iin range: " + CutDecimalWithOne(VoutSet * min_ioutset / (VinSet * 0.95)) + "A - 0A" +
                                                    " (when Vin setting value is " + VinSet.ToString() + "V and Vout setting value is " + VoutSet.ToString() + "V)";
                                            }
                                        }
                                        if (err_iinsec || err_ioutsec)
                                        {
                                            MessageBox.Show(ErrString, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                                        }
                                        else
                                        {
                                            Packet pkt1 = new Packet(0);
                                            pkt1.command = CommandCategory.EN;
                                            pkt1.sum = GetCheckSum(pkt1);
                                            serialPort1.DiscardInBuffer();
                                            if (Send(pkt1))
                                            {
                                                System.Threading.Thread.Sleep(20);
                                                Packet pktReturn1 = new Packet();
                                                if (Receive(ref pktReturn1))
                                                {
                                                    if (IsCorrectCheckSum(pktReturn1))
                                                    {
                                                        if (pktReturn1.command == CommandCategory.OK)
                                                        {
                                                            //timer1.Enabled = true;
                                                            timer2.Enabled = true;
                                                            IsOn = true;
                                                            btnON.Visible = false;
                                                            btnOFF.Visible = true;
                                                            btnDitherDis.Enabled = false;
                                                            btnDitherDis.ForeColor = System.Drawing.Color.Gray;
                                                            btnDitherEn.Enabled = false;
                                                            btnDitherEn.ForeColor = System.Drawing.Color.Gray;
                                                            lbDither.Enabled = false;
                                                            gpMonitor.Enabled = gpStatus.Enabled = true;
                                                            InitializeChart();

                                                            lbPWMduty.Enabled = lbPWMvol.Enabled = lbVoutPWM.Enabled = lbVinPWM.Enabled = lbIoutPWM.Enabled = lbIinPWM.Enabled =
                                                                lbIiPerSca.Enabled = lbIoPerSca.Enabled = lbViPerSca.Enabled = lbVoPerSca.Enabled =
                                                                lbViVolSca.Enabled = lbIiVolSca.Enabled = lbVoVolSca.Enabled = lbIoVolSca.Enabled =
                                                                txtVoutDuty.Enabled = txtVinDuty.Enabled = txtIoutDuty.Enabled = txtIinDuty.Enabled =
                                                                txtIinVol.Enabled = txtIoutVol.Enabled = txtVinVol.Enabled = txtVoutVol.Enabled = false;
                                                            chkPWMduty.Enabled = false;
                                                            chkPWMduty.Checked = false;

                                                            chkReverse.Enabled = false;
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                    else
                                    {
                                        MessageBox.Show(Status.vin2, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                                    }
                                }
                                else
                                {
                                    MessageBox.Show(Status.vin1, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                                }
                            }
                        } 
                    }
                }
            }
        }

        private void btnOFF_Click(object sender, EventArgs e)
        {
            Packet pkt = new Packet(0);
            pkt.command = CommandCategory.STOP;
            pkt.sum = GetCheckSum(pkt);

            serialPort1.DiscardInBuffer();
            if (Send(pkt))
            {
                System.Threading.Thread.Sleep(20);
                Packet pktReturn = new Packet();
                if (Receive(ref pktReturn))
                {
                    if (IsCorrectCheckSum(pktReturn))
                    {
                        if (pktReturn.command == CommandCategory.OK)
                        {
                            IsOn = false;
                            btnON.Visible = true;
                            btnOFF.Visible = false;
                            btnDitherDis.Enabled = true;
                            btnDitherDis.ForeColor = System.Drawing.Color.Black;
                            btnDitherEn.Enabled = true;
                            btnDitherEn.ForeColor = System.Drawing.Color.Black;
                            lbDither.Enabled = true;
                            gpMonitor.Enabled = gpStatus.Enabled = false;
                            //timer1.Enabled = false;

                            chkPWMduty.Enabled = true;
                            if (chkPWMduty.Checked)
                            {
                                lbPWMduty.Enabled = lbPWMvol.Enabled = lbVoutPWM.Enabled = lbVinPWM.Enabled = lbIoutPWM.Enabled = lbIinPWM.Enabled =
                                    lbIiPerSca.Enabled = lbIoPerSca.Enabled = lbViPerSca.Enabled = lbVoPerSca.Enabled =
                                    lbViVolSca.Enabled = lbIiVolSca.Enabled = lbVoVolSca.Enabled = lbIoVolSca.Enabled =
                                    txtVoutDuty.Enabled = txtVinDuty.Enabled = txtIoutDuty.Enabled = txtIinDuty.Enabled =
                                    txtIinVol.Enabled = txtIoutVol.Enabled = txtVinVol.Enabled = txtVoutVol.Enabled = true;
                            }

                            chkReverse.Enabled = true;
                        }
                    }
                }
            }

            FileGenerate();
        }
        #endregion

        #region dither
        private void btnDitherEn_Click(object sender, EventArgs e)
        {
            Packet pkt = new Packet(0);
            pkt.command = CommandCategory.DITHEREN;
            pkt.sum = GetCheckSum(pkt);

            serialPort1.DiscardInBuffer();
            if (Send(pkt))
            {
                System.Threading.Thread.Sleep(20);
                Packet pktReturn = new Packet();
                if (Receive(ref pktReturn))
                {
                    if (IsCorrectCheckSum(pktReturn))
                    {
                        if (pktReturn.command == CommandCategory.OK)
                        {
                            btnDitherEn.Visible = false;
                            btnDitherDis.Visible = true;
                        }
                    }
                }
            }
        }
        private void btnDitherDis_Click(object sender, EventArgs e)
        {
            Packet pkt = new Packet(0);
            pkt.command = CommandCategory.DITHERDIS;
            pkt.sum = GetCheckSum(pkt);

            serialPort1.DiscardInBuffer();
            if (Send(pkt))
            {
                System.Threading.Thread.Sleep(20);
                Packet pktReturn = new Packet();
                if (Receive(ref pktReturn))
                {
                    if (IsCorrectCheckSum(pktReturn))
                    {
                        if (pktReturn.command == CommandCategory.OK)
                        {
                            btnDitherEn.Visible = true;
                            btnDitherDis.Visible = false;
                        }
                    }
                }
            }
        }
        #endregion

        #region refresh
        private void timer1_Tick(object sender, EventArgs e)
        {
            if (serialPort1.IsOpen)
            {
                if (IsOn)
                {
                    Packet pkt = new Packet(0);
                    pkt.command = CommandCategory.MCUSENSE;
                    pkt.sum = GetCheckSum(pkt);

                    serialPort1.DiscardInBuffer();
                    if (Send(pkt))
                    {
                        System.Threading.Thread.Sleep(20);
                        Packet pktReturn = new Packet();
                        if (Receive(ref pktReturn))
                        {
                            if (IsCorrectCheckSum(pktReturn))
                            {
                                if (pktReturn.command == CommandCategory.OK)
                                {
                                    if (pktReturn.length == 0)
                                    {
                                        btnOFF_Click(sender, e);
                                        MessageBox.Show(Status.vin1, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                                    }
                                    else
                                    {
                                        VoutActual = (double)(pktReturn.data[0] * 256 + pktReturn.data[1]) * this.kvo / 1000 + this.voo;
                                        VinActual = (double)(pktReturn.data[2] * 256 + pktReturn.data[3]) * this.kvi / 1000 + this.vio;
                                        IoutActual = (double)(pktReturn.data[4] * 256 + pktReturn.data[5]) * this.k1 / 1000 - this.ix + IoutCon * this.mio;
                                        IinActual = (double)(pktReturn.data[6] * 256 + pktReturn.data[7]) * this.k2 / 1000 - this.iy + IinCon * this.mii;
                                        temperature = pktReturn.data[8] * 256 + pktReturn.data[9];
                                        if (pktReturn.data[10] == 1)
                                        {
                                            PG = true;
                                        }
                                        else
                                        {
                                            PG = false;
                                        }
                                        if (IoutActual > 0 && IinActual > 0)
                                        {
                                            sysDirection = 1;
                                        }
                                        else if (IoutActual < -0.5 && IinActual < -0.5)
                                        {
                                            sysDirection = 2;
                                        }
                                        else
                                        {
                                            sysDirection = 0;
                                        }
                                        if (sysDirection == 2)
                                        {
                                            if (count < 3)
                                            {
                                                dIlist.Add(IinSet - IinActual);
                                                if (count >= 2)
                                                {
                                                    dIavg = dIlist[2];
                                                }
                                                count++;
                                            }
                                            else
                                            {
                                                dI = IinSet - IinActual;
                                                if (dIavg - dI > 0.05)
                                                {
                                                    lblMode.Text = "CV";
                                                }
                                                else
                                                {
                                                    lblMode.Text = "CC";
                                                }
                                            }
                                        }
                                        RefreshStatus();

                                        //if (VinActual >= VinSet + 0.5 && !chkReverse.Checked)
                                        //{
                                        //    btnOFF_Click(sender, e);
                                        //    MessageBox.Show(Status.vin2, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                                        //}
                                        //else 
                                        if (VinActual < 8.5)
                                        {
                                            btnOFF_Click(sender, e);
                                            MessageBox.Show(Status.vin1, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                                        }
                                        else if (VinActual * max_iinset * 1 / VoutSet <= IoutSet - 0.5)
                                        {
                                            btnOFF_Click(sender, e);
                                            string ErrString = "Please input valid setting values.\n";
                                            ErrString += "\n" +
                                            "Iout range: 0A - " + CutDecimalWithOne(VinActual * max_iinset * 1 / VoutSet) + "A" +
                                            " (when Vin input voltage is " + CutDecimalWithOne(VinActual) + "V and Vout setting value is " + VoutSet.ToString() + "V)";
                                            MessageBox.Show(ErrString, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                                        }
                                    }
                                }
                            }
                        }
                    }
                    RefreshChartAndData();
                    RefreshDirection();
                }
            }
            else
            {
                timer1.Enabled = false;
                gpSetting.Enabled = gpMonitor.Enabled = gpStatus.Enabled = false;
                btnON.ForeColor = System.Drawing.Color.Gray;
                btnON.Visible = true;
                btnOFF.Visible = false;
                IsOn = false;
                sysDirection = 0;
                PG = true;
                btnConnect.ButtonText = "CONNECT";

                FileGenerate();
            }
        }

        protected void RefreshChartAndData()
        {
            xTimeList.RemoveAt(0);
            yIinList.RemoveAt(0);
            yIoutList.RemoveAt(0);
            yVinList.RemoveAt(0);
            yVoutList.RemoveAt(0);

            DateTime dt = DateTime.Now;

            xTimeList.Add(dt);
            yVoutList.Add(VoutActual);
            yVinList.Add(VinActual);
            yIinList.Add(IinActual);
            yIoutList.Add(IoutActual);

            chtMonitor.Series["Vout"].Points.Clear();
            chtMonitor.Series["Vin"].Points.Clear();
            chtMonitor.Series["Iout"].Points.Clear();
            chtMonitor.Series["Iin"].Points.Clear();

            if (rdoOut.Checked)
            {
                for (int i = 0; i <= TIME_PERIOD; i++)
                {
                    chtMonitor.Series["Iout"].Points.AddXY(xTimeList[i], yIoutList[i]);
                    chtMonitor.Series["Vout"].Points.AddXY(xTimeList[i], yVoutList[i]);
                }
            }
            else
            {
                for (int i = 0; i <= TIME_PERIOD; i++)
                {
                    chtMonitor.Series["Iin"].Points.AddXY(xTimeList[i], yIinList[i]);
                    chtMonitor.Series["Vin"].Points.AddXY(xTimeList[i], yVinList[i]);
                }
            }

            DataRow rowAll = dataSet1.Tables["Data"].NewRow();
            rowAll["Time"] = dt;
            rowAll["Set Vout(V)"] = VoutSet;
            rowAll["Actual Vout(V)"] = Math.Round(VoutActual, 2);
            rowAll["Set Iout(A)"] = IoutSet;
            rowAll["Actual Iout(A)"] = Math.Round(IoutActual, 2);
            rowAll["Set Vin(V)"] = VinSet;
            rowAll["Actual Vin(V)"] = Math.Round(VinActual, 2);
            rowAll["Set Iin(A)"] = IinSet;
            rowAll["Actual Iin(A)"] = Math.Round(IinActual, 2);
            rowAll["Temp(degC)"] = temperature;
            dataSet1.Tables["Data"].Rows.Add(rowAll);
        }

        protected void RefreshStatus()
        {
            if (PG)
            {
                picPGSta.Image = Properties.Resources.status_good;
            }
            else
            {
                picPGSta.Image = Properties.Resources.status_bad;
            }
            if (RangeJudge(VoutActual, max_voutset, min_voutset))
            {
                picVoutSta.Image = Properties.Resources.status_good;
            }
            else
            {
                picVoutSta.Image = Properties.Resources.status_bad;
            }
            if (RangeJudge(IoutActual, max_ioutset, min_iinset))
            {
                picIoutSta.Image = Properties.Resources.status_good;
            }
            else
            {
                picIoutSta.Image = Properties.Resources.status_bad;
            }
            if (RangeJudge(VinActual, max_vinset, min_vinset))
            {
                picVinSta.Image = Properties.Resources.status_good;
            }
            else
            {
                picVinSta.Image = Properties.Resources.status_bad;
            }
            if (RangeJudge(IinActual, max_ioutset, min_iinset))
            {
                picIinSta.Image = Properties.Resources.status_good;
            }
            else
            {
                picIinSta.Image = Properties.Resources.status_bad;
            }
            if (RangeJudge(temperature, Define.max_temperature, Define.min_temperature))
            {
                picTempSta.Image = Properties.Resources.status_good;
            }
            else
            {
                picTempSta.Image = Properties.Resources.status_bad;
            }
            txtVout.Text = VoutActual.ToString("F2");
            txtVin.Text = VinActual.ToString("F2");
            txtIout.Text = IoutActual.ToString("F2");
            txtIin.Text = IinActual.ToString("F2");
            txtTemp.Text = temperature.ToString("F2");
        }
        
        protected void RefreshDirection()
        {
            switch (sysDirection)
            {
                case 0:
                    picSysDirection.Image = Properties.Resources.current_initial;
                    lblModeSta.Visible = lblMode.Visible = false;
                    break;
                case 1:
                    switch (direction_count)
                    {
                        case 0:
                            picSysDirection.Image = Properties.Resources.current_forward1;
                            break;
                        case 1:
                            picSysDirection.Image = Properties.Resources.current_forward2;
                            break;
                        case 2:
                            picSysDirection.Image = Properties.Resources.current_forward3;
                            break;
                    }
                    direction_count++;
                    if (direction_count >= 3) direction_count = 0;

                    lblModeSta.Visible = lblMode.Visible = false;
                    break;
                case 2:
                    switch (direction_count)
                    {
                        case 0:
                            picSysDirection.Image = Properties.Resources.current_backward1;
                            break;
                        case 1:
                            picSysDirection.Image = Properties.Resources.current_backward2;
                            break;
                        case 2:
                            picSysDirection.Image = Properties.Resources.current_backward3;
                            break;
                    }
                    direction_count++;
                    if (direction_count >= 3) direction_count = 0;

                    lblModeSta.Visible = lblMode.Visible = true;
                    break;
            }
        }
        #endregion

        #region file
        protected void FileGenerate()
        {

            StreamWriter writer = null;
            string fileName = mainName + DateTime.Now.ToString("yyyyMMddHHmmss") + ".csv";
            try
            {
                if (dataSet1 == null || dataSet1.Tables.Count == 0)
                {
                    throw new Exception("dataset is null or has not table in dataset");
                }

                for (int i = 0; i < dataSet1.Tables.Count; i++)
                {
                    if (i > 0)
                    {
                        fileName = fileName.Substring(0, fileName.IndexOf('.')) + i + fileName.Substring(fileName.IndexOf("."));
                    }

                    writer = new StreamWriter(fileName);
                    DataTable dt = dataSet1.Tables[i];
                    StringBuilder sb = new StringBuilder();
                    for (int j = 0; j < dt.Columns.Count; j++)
                    {
                        string colName = dt.Columns[j].ColumnName;
                        if (colName.IndexOf(',') > -1)
                        {
                            colName = colName.Insert(0, "/").Insert(colName.Length + 1, "/");
                        }
                        sb.Append(colName);
                        if (!colName.Equals(""))
                        {
                            if (j != dt.Columns.Count - 1)
                            {
                                sb.Append(",");
                            }
                        }
                    }
                    writer.WriteLine(sb.ToString());
                    sb = new StringBuilder();
                    string temp = "";
                    for (int j = 0; j < dt.Rows.Count; j++)
                    {
                        DataRow dr = dt.Rows[j];
                        for (int k = 0; k < dt.Columns.Count; k++)
                        {
                            object o = dr[k];
                            if (o != null)
                            {
                                temp = o.ToString();
                            }
                            if (temp.IndexOf(',') > -1)
                            {
                                temp = temp.Insert(0, "/").Insert(temp.Length + 1, "/");
                            }
                            sb.Append(temp);
                            if (k != dt.Columns.Count - 1)
                            {
                                sb.Append(",");
                            }
                        }
                        writer.WriteLine(sb.ToString());
                        sb = new StringBuilder();
                    }
                    writer.Close();
                }
            }
            catch (Exception ex)
            {
                throw new Exception("save csv error", ex);
            }
            finally
            {
                if (writer != null)
                    writer.Close();
            }

            dataSet1.Clear();
        }

        public static DataTable OpenCSV(string filePath)
        {
            System.Text.Encoding encoding = GetType(filePath);
            DataTable dt = new DataTable();
            System.IO.FileStream fs = new System.IO.FileStream(filePath, System.IO.FileMode.Open, System.IO.FileAccess.Read);
            System.IO.StreamReader sr = new System.IO.StreamReader(fs, encoding);

            string strLine = "";
            string[] aryLine = null;
            string[] tableHead = null;
            int columnCount = 0;
            bool IsFirst = true;
            while ((strLine = sr.ReadLine()) != null)
            {
                if (IsFirst == true)
                {
                    tableHead = strLine.Split(',');

                    IsFirst = false;

                    columnCount = tableHead.Length;
                    for (int i = 0; i < columnCount; i++)
                    {
                        DataColumn dc = new DataColumn(tableHead[i]);

                        dt.Columns.Add(dc);
                    }
                }
                else
                {
                    aryLine = strLine.Split(',');

                    DataRow dr = dt.NewRow();

                    for (int j = 0; j < columnCount; j++)
                    {
                        dr[j] = aryLine[j];
                    }
                    dt.Rows.Add(dr);
                }
            }
            if (aryLine != null && aryLine.Length > 0)
            {
                dt.DefaultView.Sort = tableHead[0] + " " + "asc";
            }

            sr.Close();
            fs.Close();
            return dt;
        }
        /// 给定文件的路径，读取文件的二进制数据，判断文件的编码类型
        /// <param name="FILE_NAME">文件路径</param>
        /// <returns>文件的编码类型</returns>
        public static System.Text.Encoding GetType(string FILE_NAME)
        {
            System.IO.FileStream fs = new System.IO.FileStream(FILE_NAME, System.IO.FileMode.Open, System.IO.FileAccess.Read);
            System.Text.Encoding r = GetType(fs);
            fs.Close();
            return r;
        }
        /// 通过给定的文件流，判断文件的编码类型
        /// <param name="fs">文件流</param>
        /// <returns>文件的编码类型</returns>
        public static System.Text.Encoding GetType(System.IO.FileStream fs)
        {
            byte[] Unicode = new byte[] { 0xFF, 0xFE, 0x41 };

            byte[] UnicodeBIG = new byte[] { 0xFE, 0xFF, 0x00 };

            byte[] UTF8 = new byte[] { 0xEF, 0xBB, 0xBF };

            System.Text.Encoding reVal = System.Text.Encoding.Default;

            System.IO.BinaryReader r = new System.IO.BinaryReader(fs, System.Text.Encoding.Default);

            int i;

            int.TryParse(fs.Length.ToString(), out i);

            byte[] ss = r.ReadBytes(i);

            if (IsUTF8Bytes(ss) || (ss[0] == 0xEF && ss[1] == 0xBB && ss[2] == 0xBF))
            {
                reVal = System.Text.Encoding.UTF8;
            }
            else if (ss[0] == 0xFE && ss[1] == 0xFF && ss[2] == 0x00)
            {
                reVal = System.Text.Encoding.BigEndianUnicode;
            }
            else if (ss[0] == 0xFF && ss[1] == 0xFE && ss[2] == 0x41)
            {
                reVal = System.Text.Encoding.Unicode;
            }
            r.Close();
            return reVal;
        }
        /// 判断是否是不带 BOM 的 UTF8 格式
        /// <param name="data"></param>
        /// <returns></returns>
        private static bool IsUTF8Bytes(byte[] data)
        {
            int charByteCounter = 1;

            byte curByte;

            for (int i = 0; i < data.Length; i++)
            {
                curByte = data[i];

                if (charByteCounter == 1)
                {
                    if (curByte >= 0x80)
                    {
                        while (((curByte <<= 1) & 0x80) != 0)
                        {
                            charByteCounter++;
                        }
                        if (charByteCounter == 1 || charByteCounter > 6)
                        {
                            return false;
                        }
                    }
                }
                else
                {
                    if ((curByte & 0xC0) != 0x80)
                    {
                        return false;
                    }
                    charByteCounter--;
                }
            }
            if (charByteCounter > 1)
            {
                throw new Exception("非预期的byte格式");
            }
            return true;
        }
        #endregion

        #region serial
        public bool Send(Packet pkt)
        {
            bool result = false;
            if (pkt.length < 251)
            {
                byte size = (byte)(pkt.length + 4);
                byte[] buffer = new byte[size];

                buffer[0] = pkt.head;
                buffer[1] = pkt.command;
                buffer[2] = pkt.length;
                buffer[3] = pkt.sum;
                for (int i = 0; i < pkt.length; i++)
                {
                    buffer[i + 4] = pkt.data[i];
                }

                if (Send(buffer, size))
                {
                    result = true;
                }
            }
            return result;
        }

        public bool Send(byte[] value, int size)
        {
            bool result = false;
            if (value != null)
            {
                if (size != 0)
                {
                    if (serialPort1.IsOpen)
                    {
                        try
                        {
                            serialPort1.Write(value, 0, size);
                            result = true;
                        }
                        catch (System.Exception ex)
                        {
                            //MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        }
                    }
                }
            }
            return result;
        }

        public bool Receive(ref Packet pkt)
        {
            bool result = false;
            byte[] buffer = new byte[4];
            if (Receive(ref buffer, 4))
            {
                pkt.head = buffer[0];
                pkt.command = buffer[1];
                pkt.length = buffer[2];
                pkt.sum = buffer[3];

                if (pkt.length > 0)
                {
                    if (Receive(ref pkt.data, pkt.length))
                    {
                        result = true;
                    }
                }
                else
                {
                    result = true;
                }
            }
            return result;
        }

        public bool Receive(ref byte[] value, int size)
        {
            bool result = false;
            if (serialPort1.IsOpen)
            {
                try
                {
                    int readsize = 0;
                    byte[] buffer = new byte[size];

                    while (readsize < size)
                    {
                        readsize += serialPort1.Read(buffer, readsize, size);
                        System.Threading.Thread.Sleep(1);
                    }

                    if (readsize == size)
                    {
                        value = buffer;
                        result = true;
                    }
                }
                catch (System.Exception ex)
                {
                    //MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
            return result;
        }

        private byte GetCheckSum(Packet pkt)
        {
            int result = pkt.head + pkt.command + pkt.length;
            for (int i = 0; i < pkt.length; i++)
            {
                result += pkt.data[i];
            }
            return (byte)result;
        }

        private bool IsCorrectCheckSum(Packet pkt)
        {
            bool result = false;
            if (pkt.sum == GetCheckSum(pkt))
            {
                result = true;
            }
            return result;
        }
        #endregion

        #region explanation
        private void lblVoutSet_MouseEnter(object sender, EventArgs e)
        {
            Font boldFont = new Font(lblVoutSet.Font, FontStyle.Bold);
            lblVoutSet.Font = boldFont;
            txtVoutIns.Visible = true;
        }

        private void lblVoutSet_MouseLeave(object sender, EventArgs e)
        {
            Font regularFont = new Font(lblVoutSet.Font, FontStyle.Regular);
            lblVoutSet.Font = regularFont;
            txtVoutIns.Visible = false;
        }

        private void lblIoutSet_MouseEnter(object sender, EventArgs e)
        {
            Font boldFont = new Font(lblIoutSet.Font, FontStyle.Bold);
            lblIoutSet.Font = boldFont;
            txtIoutIns.Visible = true;
        }

        private void lblIoutSet_MouseLeave(object sender, EventArgs e)
        {
            Font regularFont = new Font(lblIoutSet.Font, FontStyle.Regular);
            lblIoutSet.Font = regularFont;
            txtIoutIns.Visible = false;
        }

        private void lblVinSet_MouseEnter(object sender, EventArgs e)
        {
            Font boldFont = new Font(lblVinSet.Font, FontStyle.Bold);
            lblVinSet.Font = boldFont;
            txtVinIns.Visible = true;
        }

        private void lblVinSet_MouseLeave(object sender, EventArgs e)
        {
            Font regularFont = new Font(lblVinSet.Font, FontStyle.Regular);
            lblVinSet.Font = regularFont;
            txtVinIns.Visible = false;
        }

        private void lblIinSet_MouseEnter(object sender, EventArgs e)
        {
            Font boldFont = new Font(lblIinSet.Font, FontStyle.Bold);
            lblIinSet.Font = boldFont;
            txtIinIns.Visible = true;
        }

        private void lblIinSet_MouseLeave(object sender, EventArgs e)
        {
            Font regularFont = new Font(lblIinSet.Font, FontStyle.Regular);
            lblIinSet.Font = regularFont;
            txtIinIns.Visible = false;
        }
        #endregion

        #region PWM duty setting for debug
        private void Duty_TextChanged(object sender, EventArgs e)
        {
            double duty_vout, duty_iout, duty_vin, duty_iin;

            if (!string.IsNullOrEmpty(txtVoutDuty.Text))
            {
                duty_vout = System.Convert.ToDouble(txtVoutDuty.Text) / 100;
                if (chkPWMduty.Checked)
                {
                    this.txtVoutSet.Text = (duty_vout * Define.vcc * this.mvo + this.vom).ToString("F1");
                }
                this.txtVoutVol.Text = (duty_vout * Define.vcc).ToString("F3");
            }
            if (!string.IsNullOrEmpty(txtVinDuty.Text))
            {
                duty_vin = System.Convert.ToDouble(txtVinDuty.Text) / 100;
                if (chkPWMduty.Checked)
                {
                    this.txtVinSet.Text = (duty_vin * Define.vcc * this.mvi + this.vim).ToString("F1");
                }
                this.txtVinVol.Text = (duty_vin * Define.vcc).ToString("F3");
            }
            if (!string.IsNullOrEmpty(txtIoutDuty.Text))
            {
                duty_iout = System.Convert.ToDouble(txtIoutDuty.Text) / 100;
                if (chkPWMduty.Checked)
                {
                    this.txtIoutSet.Text = (duty_iout * Define.vcc * this.mio + this.iom).ToString("F1");
                }
                this.txtIoutVol.Text = (duty_iout * Define.vcc).ToString("F3");
            }
            if (!string.IsNullOrEmpty(txtIinDuty.Text))
            {
                duty_iin = System.Convert.ToDouble(txtIinDuty.Text) /100;
                if (chkPWMduty.Checked)
                {
                    this.txtIinSet.Text = (duty_iin * Define.vcc * this.mii + this.iim).ToString("F1");
                }
                this.txtIinVol.Text = (duty_iin * Define.vcc).ToString("F3");
            }
        }

        private void chkPWMduty_CheckedChanged(object sender, EventArgs e)
        {
            if (chkPWMduty.Checked)
            {
                lbPWMduty.Enabled = lbPWMvol.Enabled = lbVoutPWM.Enabled = lbIoutPWM.Enabled =
                                    lbIoPerSca.Enabled = lbVoPerSca.Enabled =
                                    lbVoVolSca.Enabled = lbIoVolSca.Enabled =
                                    txtVoutDuty.Enabled = txtVinDuty.Enabled = txtIoutDuty.Enabled = txtIinDuty.Enabled =
                                    txtIinVol.Enabled = txtIoutVol.Enabled = txtVinVol.Enabled = txtVoutVol.Enabled = true;
                lblVinSet.Enabled = txtVinSet.Enabled = lblVinSetScale.Enabled = lblIinSet.Enabled = txtIinSet.Enabled = lblIinSetScale.Enabled =
                                    lblVoutSet.Enabled = txtVoutSet.Enabled = lblVoutSetScale.Enabled = lblIoutSet.Enabled = txtIoutSet.Enabled = lblIoutSetScale.Enabled = 
                                    btnVoutDe.Enabled = btnVoutIn.Enabled = btnIoutDe.Enabled = btnIoutIn.Enabled = 
                                    btnVinDe.Enabled = btnVinIn.Enabled = btnIinDe.Enabled = btnIinIn.Enabled = false;
                if (!chkReverse.Checked)
                {
                    lbVinPWM.Enabled = lbViPerSca.Enabled = lbViVolSca.Enabled = lbIinPWM.Enabled = lbIiPerSca.Enabled = lbIiVolSca.Enabled = true;
                }
            }
            else
            {
                lbPWMduty.Enabled = lbPWMvol.Enabled = lbVoutPWM.Enabled = lbVinPWM.Enabled = lbIoutPWM.Enabled = lbIinPWM.Enabled =
                                    lbIiPerSca.Enabled = lbIoPerSca.Enabled = lbViPerSca.Enabled = lbVoPerSca.Enabled =
                                    lbViVolSca.Enabled = lbIiVolSca.Enabled = lbVoVolSca.Enabled = lbIoVolSca.Enabled =
                                    txtVoutDuty.Enabled = txtVinDuty.Enabled = txtIoutDuty.Enabled = txtIinDuty.Enabled =
                                    txtIinVol.Enabled = txtIoutVol.Enabled = txtVinVol.Enabled = txtVoutVol.Enabled = false;
                lblVinSet.Enabled = txtVinSet.Enabled = lblVinSetScale.Enabled = lblIinSet.Enabled = txtIinSet.Enabled = lblIinSetScale.Enabled =
                                    lblVoutSet.Enabled = txtVoutSet.Enabled = lblVoutSetScale.Enabled = lblIoutSet.Enabled = txtIoutSet.Enabled = lblIoutSetScale.Enabled =
                                    btnVoutDe.Enabled = btnVoutIn.Enabled = btnIoutDe.Enabled = btnIoutIn.Enabled = true;
                if (!chkReverse.Checked)
                {
                    btnIinIn.Enabled = btnIinDe.Enabled = true;
                    btnVinDe.Enabled = btnVinIn.Enabled = true;
                }
            }
        }
        #endregion

        private void timer2_Tick(object sender, EventArgs e)
        {
            double duty_vin;
            bool err1 = true;
            if (!chkReverse.Checked)
            {
                duty_vin = GetDutyValue(VinSet, Define.vin);

                Packet pkt1 = new Packet(3);
                pkt1.data[0] = (byte)(duty_vin * 100 % 100);
                pkt1.data[1] = (byte)(duty_vin * 10000 % 100);
                pkt1.data[2] = (byte)(duty_vin * 100000 % 10);
                pkt1.command = CommandCategory.VINCTRL;
                pkt1.sum = GetCheckSum(pkt1);

                serialPort1.DiscardInBuffer();
                if (Send(pkt1))
                {
                    System.Threading.Thread.Sleep(20);
                    Packet pktReturn1 = new Packet();
                    if (Receive(ref pktReturn1))
                    {
                        if (IsCorrectCheckSum(pktReturn1))
                        {
                            if (pktReturn1.command == CommandCategory.OK)
                            {
                                err1 = false;
                            }
                        }
                    }
                }

                if (err1)
                {
                    btnOFF_Click(sender, e);
                }
            }

            timer2.Enabled = false;
        }
    }

    public struct Packet
    {
        public byte head;
        public byte command;
        public byte length;
        public byte sum;
        public byte[] data;

        public Packet(byte size)
        {
            head = 0xAA;
            command = 0;
            length = size;
            sum = 0;
            data = new byte[size];
        }
    }
}
