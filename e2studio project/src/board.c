/***********************************************************************************************************************
* DISCLAIMER
* This software is supplied by Renesas Electronics Corporation and is only intended for use with Renesas products.
* No other uses are authorized. This software is owned by Renesas Electronics Corporation and is protected under all
* applicable laws, including copyright laws. 
* THIS SOFTWARE IS PROVIDED "AS IS" AND RENESAS MAKES NO WARRANTIESREGARDING THIS SOFTWARE, WHETHER EXPRESS, IMPLIED
* OR STATUTORY, INCLUDING BUT NOT LIMITED TO WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
* NON-INFRINGEMENT.  ALL SUCH WARRANTIES ARE EXPRESSLY DISCLAIMED.TO THE MAXIMUM EXTENT PERMITTED NOT PROHIBITED BY
* LAW, NEITHER RENESAS ELECTRONICS CORPORATION NOR ANY OF ITS AFFILIATED COMPANIES SHALL BE LIABLE FOR ANY DIRECT,
* INDIRECT, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES FOR ANY REASON RELATED TO THIS SOFTWARE, EVEN IF RENESAS OR
* ITS AFFILIATES HAVE BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
* Renesas reserves the right, without notice, to make changes to this software and to discontinue the availability 
* of this software. By using this software, you agree to the additional terms and conditions found by accessing the 
* following link:
* http://www.renesas.com/disclaimer
*
* Copyright (C) 2018 Renesas Electronics Corporation. All rights reserved.
***********************************************************************************************************************/

/***********************************************************************************************************************
* File Name    : board.c
***********************************************************************************************************************/

/***********************************************************************************************************************
Includes
***********************************************************************************************************************/

#include <math.h>
#include "r_cg_macrodriver.h"
#include "board.h"
#include "r_cg_adc.h"

/***********************************************************************************************************************
Pragma directive
***********************************************************************************************************************/


/***********************************************************************************************************************
Global variables and functions
***********************************************************************************************************************/

#define ADC_MASK 0xffff

double vcc = 3.3;
double vom = 81.828;
double voo = 0.008;
double vio = -0.017;
double vim = 80.916;
double mvo = -25.03;
double mvi = -25.059;
double kvo = 24.45;
double kvi = 24.38;

double k1 = 82.3; //75.712;
double mio = -9.079;
double ix = 73.351; //68.844;
double iom = 26.304;

//double k2 = 86.828;
//double mii = -9.814;
//double iim = 7.716;
//double iy = 61.231;
double k2 = 86.51; //70.145;
double mii = -9.814;
double iy = 61.231;
double iim = 7.716;

uint16_t sample_count = 1;


double IinCon = 0.0;
double IoutCon = 0.0;

uint16_t vin_raw_adc;
uint16_t vout_raw_adc;
uint16_t iin_raw_adc;
uint16_t iout_raw_adc;


#ifdef SIMULATION
double simulated_iout = 12.34;
double simulated_iout_step = 0.0;

double solve_voltage(double current,
		 	 	 	double Isc,
					double Voc,
					double Impp,
					double Vmpp)
{
	double v,vt;
	double temperature = 298.0;

	vt = ((2*Vmpp - Voc)/(log((Isc-Impp)/Isc)+Impp/(Isc-Impp))) * temperature/298;
	v = vt* log(1+(Isc-current)/((Isc - Impp) / (exp(Vmpp/vt) - 1)));
	return v;
}

#endif

volatile uint16_t adc_vin, adc_iin, adc_vout, adc_iout;

double sense_vin(void)
{
#ifdef SIMULATION
	return solve_voltage(simulated_iout,
			 	 	 	 solar_panels[SOLAR_PANEL_IDX].Isc,
						 solar_panels[SOLAR_PANEL_IDX].Voc,
						 solar_panels[SOLAR_PANEL_IDX].Impp,
						 solar_panels[SOLAR_PANEL_IDX].Vmpp);
#else
	uint32_t i, sum = 0;

	for(i=0; i<sample_count; i++)
		sum += (vin_raw_adc = get_vin()) & ADC_MASK;

	adc_vin = sum / i;
	return adc_vin * kvi / 1000 + vio;
#endif
}

double sense_iin(void)
{
#ifdef SIMULATION
	return simulated_iout;
#else
	uint32_t i, sum = 0;

	for(i=0; i<sample_count; i++)
		sum += (iin_raw_adc = get_iin()) & ADC_MASK;

	adc_iin = sum / i;

	IinCon = (-1.0 - iim) / mii;

	return adc_iin * k2 / 1000 - iy + IinCon * mii;
#endif
}

double sense_vout(void)
{
	uint32_t i, sum = 0;

	for(i=0; i<sample_count; i++)
		sum += (vout_raw_adc = get_vout()) & ADC_MASK;

	adc_vout = sum / i;
	return adc_vout * kvo / 1000 + voo;
}

double sense_iout(void)
{
	uint32_t i, sum = 0;

	for(i=0; i<sample_count; i++)
		sum += (iout_raw_adc = get_iout()) & ADC_MASK;

	adc_iout = sum / i;

	IoutCon  = (3.0 - iom) / mio;

	return adc_iout * k1 / 1000 - ix + IoutCon * mio;
}

uint32_t vout_dutycycle;
uint32_t get_vout_dutycycle(double vout)
{

	vout_dutycycle = (vout - vom) / mvo / vcc * 100000;
	return vout_dutycycle;
}

uint32_t iout_dutycycle;
uint32_t get_iout_dutycycle(double iout)
{

	iout_dutycycle = (iout - iom) / mio / vcc * 100000;
	return iout_dutycycle;
}

uint32_t vin_dutycycle;
uint32_t get_vin_dutycycle(double vin)
{

	vin_dutycycle = (vin - vim) / mvi / vcc * 100000;
	return vin_dutycycle;
}

uint32_t iin_dutycycle;
uint32_t get_iin_dutycycle(double iin)
{

	iin_dutycycle = (iin - iim) / mii / vcc * 100000;
	return iin_dutycycle;
}




