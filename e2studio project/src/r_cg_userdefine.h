/***********************************************************************************************************************
* DISCLAIMER
* This software is supplied by Renesas Electronics Corporation and is only intended for use with Renesas products.
* No other uses are authorized. This software is owned by Renesas Electronics Corporation and is protected under all
* applicable laws, including copyright laws. 
* THIS SOFTWARE IS PROVIDED "AS IS" AND RENESAS MAKES NO WARRANTIES REGARDING THIS SOFTWARE, WHETHER EXPRESS, IMPLIED
* OR STATUTORY, INCLUDING BUT NOT LIMITED TO WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
* NON-INFRINGEMENT.  ALL SUCH WARRANTIES ARE EXPRESSLY DISCLAIMED.TO THE MAXIMUM EXTENT PERMITTED NOT PROHIBITED BY
* LAW, NEITHER RENESAS ELECTRONICS CORPORATION NOR ANY OF ITS AFFILIATED COMPANIES SHALL BE LIABLE FOR ANY DIRECT,
* INDIRECT, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES FOR ANY REASON RELATED TO THIS SOFTWARE, EVEN IF RENESAS OR
* ITS AFFILIATES HAVE BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
* Renesas reserves the right, without notice, to make changes to this software and to discontinue the availability 
* of this software. By using this software, you agree to the additional terms and conditions found by accessing the 
* following link:
* http://www.renesas.com/disclaimer
*
* Copyright (C) 2011, 2018 Renesas Electronics Corporation. All rights reserved.
***********************************************************************************************************************/

/***********************************************************************************************************************
* File Name    : r_cg_userdefine.h
* Version      : CodeGenerator for RL78/G14 V2.05.02.05 [30 May 2018]
* Device(s)    : R5F104AG
* Tool-Chain   : CCRL
* Description  : This file includes user definition.
* Creation Date: 12/14/2018
***********************************************************************************************************************/

#ifndef _USER_DEF_H
#define _USER_DEF_H

/***********************************************************************************************************************
User definitions
***********************************************************************************************************************/

/* Start user code for function. Do not edit comment generated here */
/***********************************************************************************************************************
Macro definitions
***********************************************************************************************************************/
#define TRUE				1
#define FALSE				0
#define PORT_HIGH			1
#define PORT_LOW			0
#define INPUT				1
#define OUTPUT				0
#define EN_PORT				P3_bit.no0
#define PG_PORT				P3_bit.no1
#define DITHER_PORT			P0_bit.no0

#define II_PWM				TRDGRC1
#define VI_PWM				TRDGRC0
#define VO_PWM				TRDGRB0
#define IO_PWM				TRDGRB1
#define PWM_PERIOD0			TRDGRA0
#define PWM_PERIOD1			TRDGRA1
#define PM_VO				PM1_bit.no5
#define PM_IO 				PM1_bit.no2
#define PM_VI 				PM1_bit.no6
#define PM_II 				PM1_bit.no1

//#define ADC_SAMPLE_TIMES	5
#define VCC					3300			/* VREF(+): VDD, 3300mV */
#define SERIES_R			100000			/* series resistance: 100K */

#define TIMER0_NUM			10

#define KVI					24
#define TIMER1_NUM			3
#define VIN_LIMIT			7500

/***********************************************************************************************************************
Typedef definitions
***********************************************************************************************************************/
typedef enum
{
	HEAD = 0,
	CMD,
	LEN,
	SUM,
	HEADER_SIZE,
	TX_NUM = 15,
	RX_NUM = 19
}protocol_t;

typedef enum
{
	ICSTART = 1,
	ICSTOP,
	VOUTCTRL,
	IOUTCTRL,
	VINCTRL,
	IINCTRL,
	MCUSENSE,
	EN,
	DITHEREN,
	DITHERDIS
}command_t;

/***********************************************************************************************************************
Global variables and functions
***********************************************************************************************************************/
_Bool r_serial_checksum(uint8_t * const rx_buf, uint8_t rx_num);
uint8_t r_serial_getsum(uint8_t * const rx_buf, uint8_t rx_num);
void r_command_process(uint8_t command);
/* End user code. Do not edit comment generated here */
#endif
