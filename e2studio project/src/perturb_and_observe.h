/***********************************************************************************************************************
* DISCLAIMER
* This software is supplied by Renesas Electronics Corporation and is only intended for use with Renesas products.
* No other uses are authorized. This software is owned by Renesas Electronics Corporation and is protected under all
* applicable laws, including copyright laws. 
* THIS SOFTWARE IS PROVIDED "AS IS" AND RENESAS MAKES NO WARRANTIESREGARDING THIS SOFTWARE, WHETHER EXPRESS, IMPLIED
* OR STATUTORY, INCLUDING BUT NOT LIMITED TO WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
* NON-INFRINGEMENT.  ALL SUCH WARRANTIES ARE EXPRESSLY DISCLAIMED.TO THE MAXIMUM EXTENT PERMITTED NOT PROHIBITED BY
* LAW, NEITHER RENESAS ELECTRONICS CORPORATION NOR ANY OF ITS AFFILIATED COMPANIES SHALL BE LIABLE FOR ANY DIRECT,
* INDIRECT, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES FOR ANY REASON RELATED TO THIS SOFTWARE, EVEN IF RENESAS OR
* ITS AFFILIATES HAVE BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
* Renesas reserves the right, without notice, to make changes to this software and to discontinue the availability 
* of this software. By using this software, you agree to the additional terms and conditions found by accessing the 
* following link:
* http://www.renesas.com/disclaimer
*
* Copyright (C) 2018 Renesas Electronics Corporation. All rights reserved.
***********************************************************************************************************************/

/***********************************************************************************************************************
* File Name    : perturb_and_observe.h
***********************************************************************************************************************/


typedef struct
{
	uint16_t sample_count;
	double Vin;
	double Iin;
	double Vout;
	double Iout;
	double TargetVout;
	uint16_t increase_flag;
}PO_TELEMETRY_TYPE;

#define PO_TELEMETRY_COUNT 10
extern PO_TELEMETRY_TYPE po_telemetry[PO_TELEMETRY_COUNT];
extern uint32_t po_telemetry_idx;

void dump_panel_spec(void);

void perturb_and_observe_telemetry_dump(void);

void perturb_and_observe_telemetry(
		uint32_t sample_count,
		double Vin,
		double Iin,
		double Vout,
		double Iout,
		double previous_power,
		double TargetVout,
		uint16_t increase_flag);
