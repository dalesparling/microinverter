/***********************************************************************************************************************
* DISCLAIMER
* This software is supplied by Renesas Electronics Corporation and is only intended for use with Renesas products.
* No other uses are authorized. This software is owned by Renesas Electronics Corporation and is protected under all
* applicable laws, including copyright laws. 
* THIS SOFTWARE IS PROVIDED "AS IS" AND RENESAS MAKES NO WARRANTIESREGARDING THIS SOFTWARE, WHETHER EXPRESS, IMPLIED
* OR STATUTORY, INCLUDING BUT NOT LIMITED TO WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
* NON-INFRINGEMENT.  ALL SUCH WARRANTIES ARE EXPRESSLY DISCLAIMED.TO THE MAXIMUM EXTENT PERMITTED NOT PROHIBITED BY
* LAW, NEITHER RENESAS ELECTRONICS CORPORATION NOR ANY OF ITS AFFILIATED COMPANIES SHALL BE LIABLE FOR ANY DIRECT,
* INDIRECT, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES FOR ANY REASON RELATED TO THIS SOFTWARE, EVEN IF RENESAS OR
* ITS AFFILIATES HAVE BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
* Renesas reserves the right, without notice, to make changes to this software and to discontinue the availability 
* of this software. By using this software, you agree to the additional terms and conditions found by accessing the 
* following link:
* http://www.renesas.com/disclaimer
*
* Copyright (C) 2018 Renesas Electronics Corporation. All rights reserved.
***********************************************************************************************************************/

/***********************************************************************************************************************
* File Name    : perturb_and_observe.c
***********************************************************************************************************************/

/***********************************************************************************************************************
Includes
***********************************************************************************************************************/
#include <stdio.h>
#include <string.h>
#include <math.h>
#include "r_cg_macrodriver.h"
#include "r_cg_adc.h"
#include "perturb_and_observe.h"
#include "r_cg_serial.h"
#include "solar_panel.h"
#include "board.h"

/***********************************************************************************************************************
Pragma directive
***********************************************************************************************************************/


/***********************************************************************************************************************
Global variables and functions
***********************************************************************************************************************/

void command_voutctrl(uint32_t vout_duty);
void command_iinctrl(uint32_t vout_duty);

PO_TELEMETRY_TYPE po_telemetry[PO_TELEMETRY_COUNT];
uint32_t po_telemetry_idx = 0;

#define SOLAR_PANEL_IDX 0

extern volatile uint8_t r_uart0_callback_sendend_flag;
void UART0_SendText(uint8_t * const tx_buf, uint16_t tx_num)
{
	r_uart0_callback_sendend_flag = 1;
	R_UART0_Send(tx_buf, tx_num);
	while(r_uart0_callback_sendend_flag);
}


void perturb_and_observe_telemetry(
		uint32_t sample_count,
		double Vin,
		double Iin,
		double Vout,
		double Iout,
		double previous_power,
		double vout_target_mppt,
		uint16_t increase_flag)
{
	if (PO_TELEMETRY_COUNT > po_telemetry_idx)
	{
		po_telemetry[po_telemetry_idx].sample_count = sample_count;
		po_telemetry[po_telemetry_idx].Vin = Vin;
		po_telemetry[po_telemetry_idx].Iin = Iin;
		po_telemetry[po_telemetry_idx].Vout = Vout;
		po_telemetry[po_telemetry_idx].Iout = Iout;
		po_telemetry[po_telemetry_idx].TargetVout = vout_target_mppt;
		po_telemetry[po_telemetry_idx].increase_flag = increase_flag;
		po_telemetry_idx++;
	}
}

char buf[256];
int buflen=0;

void perturb_and_observe_telemetry_dump(void)
{
	int i;

	strcpy(buf, "\nsample count   Vin      Iin     Power        Vout     Iout    Power    Target Vout\n");
	buflen = strlen(buf);
	UART0_SendText(buf, buflen);

	for(i = 0; i < po_telemetry_idx; i++)
	{
		char *flagstr;

		switch(po_telemetry[i].increase_flag)
		{
		case 1:
			flagstr = "up";
			break;
		case 2:
			flagstr = "down";
			break;
		default:
			flagstr = "";
			break;
		}

		sprintf(buf, "%04d,        %6.2f,  %6.2f,  %6.2f,     %6.2f,  %6.2f,  %6.2f      %6.2f  %s\n",
				po_telemetry[i].sample_count,
				po_telemetry[i].Vin,
				po_telemetry[i].Iin,
				po_telemetry[i].Vin * po_telemetry[i].Iin,
				po_telemetry[i].Vout,
				po_telemetry[i].Iout,
				po_telemetry[i].Vout * po_telemetry[i].Iout,
				po_telemetry[i].TargetVout,
				flagstr);
		buflen = strlen(buf);
		UART0_SendText(buf, buflen);
	}
}


void dump_panel_spec(void)
{
#ifdef SIMULATION

	double current, v;
	current = solar_panels[SOLAR_PANEL_IDX].Impp;
	v = solve_voltage(current,
			solar_panels[SOLAR_PANEL_IDX].Isc,
			solar_panels[SOLAR_PANEL_IDX].Voc,
			solar_panels[SOLAR_PANEL_IDX].Impp,
			solar_panels[SOLAR_PANEL_IDX].Vmpp);

	sprintf(buf, "V = %6.2f  current = %6.2f  Power = %6.2f\n",	v, current, v * current);
	buflen = strlen(buf);
	UART0_SendText(buf, buflen);

	current = 0.0;
	v = solve_voltage(current,
			solar_panels[SOLAR_PANEL_IDX].Isc,
			solar_panels[SOLAR_PANEL_IDX].Voc,
			solar_panels[SOLAR_PANEL_IDX].Impp,
			solar_panels[SOLAR_PANEL_IDX].Vmpp);

	sprintf(buf, "V = %6.2f  current = %6.2f  Power = %6.2f\n",	v, current, v * current);
	buflen = strlen(buf);
	UART0_SendText(buf, buflen);

	current = solar_panels[SOLAR_PANEL_IDX].Isc;
	v = solve_voltage(current,
			solar_panels[SOLAR_PANEL_IDX].Isc,
			solar_panels[SOLAR_PANEL_IDX].Voc,
			solar_panels[SOLAR_PANEL_IDX].Impp,
			solar_panels[SOLAR_PANEL_IDX].Vmpp);

	sprintf(buf, "V = %6.2f  current = %6.2f  Power = %6.2f\n",	v, current, v * current);
	buflen = strlen(buf);
	UART0_SendText(buf, buflen);

#endif
}

double Vin;
double Iin;
double Vout;
double Iout;
double Power;
double vout_target_slider = 12.0;
double vout_target_mppt;
double iin_target_slider = -1.1;
double iin_target_mppt;
double step_rate = 0.1;
uint8_t MPPT_enable_flag = 0;

void decrease_vin(void);
void decrease_vin(void)
{
#ifdef SIMULATION
	simulated_iout -= simulated_iout_step;
	if (simulated_iout > solar_panels[SOLAR_PANEL_IDX].Isc)
		simulated_iout = solar_panels[SOLAR_PANEL_IDX].Isc;
#else

	vout_target_mppt = vout_target_mppt - step_rate;
	if (vout_target_mppt < 0.0)
		vout_target_mppt = 0.0;

	command_voutctrl(get_vout_dutycycle(vout_target_mppt));
#endif
}

void increase_vin(void);
void increase_vin(void)
{
#ifdef SIMULATION
	simulated_iout += simulated_iout_step;
	if (simulated_iout < 0.0)
		simulated_iout = 0.0;
#else

	vout_target_mppt = vout_target_mppt + step_rate;
	if (vout_target_mppt > 40.0)
		vout_target_mppt = 40.0;

	command_voutctrl(get_vout_dutycycle(vout_target_mppt));
#endif
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void perturb_and_observe(uint32_t sample_count)
{
	static double previous_vin = 0;
	static double previous_iin = 0;
	static double previous_power = .00001;
	uint16_t increase_flag;

#ifdef SIMULATION
	if (simulated_iout_step == 0.0)
	{
		double current, v;

		simulated_iout = 0.0;
		simulated_iout_step = solar_panels[SOLAR_PANEL_IDX].Isc / (PO_TELEMETRY_COUNT * 1.0) * 1.2;
	}
#endif

	Vin = sense_vin();
	Iin = sense_iin();
	Vout = sense_vout();
	Iout = sense_iout();
	Power = Vin * Iin;


	{
		static double iin_target_slider_old;

		if (iin_target_slider_old != iin_target_slider)
			command_iinctrl(get_iin_dutycycle(iin_target_slider));
		iin_target_mppt = iin_target_slider_old = iin_target_slider;
	}


	if (!MPPT_enable_flag)
	{
		static double vout_target_slider_old;

		if (vout_target_slider_old != vout_target_slider)
			command_voutctrl(get_vout_dutycycle(vout_target_slider));
		vout_target_mppt = vout_target_slider_old = vout_target_slider;
	}
	else
	{

		increase_flag = 0;

		if (Power != previous_power)
		{

			if (Power - previous_power > 0)
			{
				if (Vin - previous_vin > 0)
				{
					decrease_vin();
					increase_flag = 2;
				}
				else
				{
					increase_vin();
					increase_flag = 1;
				}
			}
			else
			{
				if (Vin - previous_vin > 0)
				{
					increase_vin();
					increase_flag = 1;
				}
				else
				{
					decrease_vin();
					increase_flag = 2;
				}
			}
		}

		perturb_and_observe_telemetry(sample_count, Vin, Iin, Vout, Iout, previous_power, vout_target_mppt, increase_flag);


		previous_vin = Vin;
		previous_iin = Iin;
		previous_power = Power;
	}
}
