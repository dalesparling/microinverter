/***********************************************************************************************************************
* DISCLAIMER
* This software is supplied by Renesas Electronics Corporation and is only intended for use with Renesas products.
* No other uses are authorized. This software is owned by Renesas Electronics Corporation and is protected under all
* applicable laws, including copyright laws. 
* THIS SOFTWARE IS PROVIDED "AS IS" AND RENESAS MAKES NO WARRANTIES REGARDING THIS SOFTWARE, WHETHER EXPRESS, IMPLIED
* OR STATUTORY, INCLUDING BUT NOT LIMITED TO WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
* NON-INFRINGEMENT.  ALL SUCH WARRANTIES ARE EXPRESSLY DISCLAIMED.TO THE MAXIMUM EXTENT PERMITTED NOT PROHIBITED BY
* LAW, NEITHER RENESAS ELECTRONICS CORPORATION NOR ANY OF ITS AFFILIATED COMPANIES SHALL BE LIABLE FOR ANY DIRECT,
* INDIRECT, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES FOR ANY REASON RELATED TO THIS SOFTWARE, EVEN IF RENESAS OR
* ITS AFFILIATES HAVE BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
* Renesas reserves the right, without notice, to make changes to this software and to discontinue the availability 
* of this software. By using this software, you agree to the additional terms and conditions found by accessing the 
* following link:
* http://www.renesas.com/disclaimer
*
* Copyright (C) 2011, 2018 Renesas Electronics Corporation. All rights reserved.
***********************************************************************************************************************/

/***********************************************************************************************************************
* File Name    : r_main.c
* Version      : CodeGenerator for RL78/G14 V2.05.02.05 [30 May 2018]
* Device(s)    : R5F104AG
* Tool-Chain   : CCRL
* Description  : This file implements main function.
* Creation Date: 12/14/2018
***********************************************************************************************************************/

/***********************************************************************************************************************
Includes
***********************************************************************************************************************/
#include <stdio.h>
#include <string.h>
#include "r_cg_macrodriver.h"
#include "r_cg_cgc.h"
#include "r_cg_port.h"
#include "r_cg_serial.h"
#include "r_cg_adc.h"
#include "r_cg_timer.h"
/* Start user code for include. Do not edit comment generated here */
#include "perturb_and_observe.h"
#include "board.h"
/* End user code. Do not edit comment generated here */
#include "r_cg_userdefine.h"

/***********************************************************************************************************************
Pragma directive
***********************************************************************************************************************/
/* Start user code for pragma. Do not edit comment generated here */
/* End user code. Do not edit comment generated here */

/***********************************************************************************************************************
Global variables and functions
***********************************************************************************************************************/
/* Start user code for global. Do not edit comment generated here */
uint8_t rx_buffer[RX_NUM];
uint8_t tx_OK[HEADER_SIZE] = {0x55, 0x10, 0x00, 0x65};
uint8_t tx_ERROR[HEADER_SIZE] = {0x55, 0xF1, 0x00, 0x46};
uint8_t tx_buffer[TX_NUM];
_Bool rx_done;

uint32_t vout_duty, vout_pwm, vin_pwm, iin_pwm;
uint32_t iout_duty;
uint32_t vin_duty;
uint32_t iin_duty;
uint16_t vout_sen;
uint16_t vin_sen;
uint16_t iout_sen;
uint16_t iin_sen;
uint16_t temperature;
uint32_t PWM_period = 319;

uint16_t vout_ad[TIMER0_NUM];
uint16_t vin_ad[TIMER0_NUM];
uint16_t iout_ad[TIMER0_NUM];
uint16_t iin_ad[TIMER0_NUM];
uint16_t vout_sum = 0, vin_sum = 0, iout_sum = 0, iin_sum = 0;
uint16_t vout_adc = 0, vin_adc = 0, iout_adc = 0, iin_adc = 0;
uint8_t timer0_count = 0, timer1_count = 0;
uint8_t fM_EN = FALSE, fS_SP = FALSE;

void command_icstart(uint32_t vout_duty, uint32_t iout_duty, uint32_t vin_duty, uint32_t iin_duty, uint32_t PWM_period);
void command_en(void);
void command_vinctrl(uint32_t vin_duty);

/* End user code. Do not edit comment generated here */
void R_MAIN_UserInit(void);

/***********************************************************************************************************************
* Function Name: main
* Description  : This function implements main function.
* Arguments    : None
* Return Value : None
***********************************************************************************************************************/
void main(void)
{
    R_MAIN_UserInit();
    /* Start user code. Do not edit comment generated here */
	R_UART0_Receive(rx_buffer, RX_NUM);
	R_UART0_Start();

	R_TAU0_Channel0_Start();
	R_TAU0_Channel1_Start();


#if 1
	vout_duty = get_vout_dutycycle(19.0);
	iout_duty = get_iout_dutycycle(3.0);
	vin_duty = get_vin_dutycycle(17.2);
	iin_duty = get_iin_dutycycle(-1.1);
	command_icstart(vout_duty,
					iout_duty,
					vin_duty,
					iin_duty,
					PWM_period);
	command_en();
	R_TAU0_Channel3_Start();
	dump_panel_spec();

	while(1)
	{
		if (PO_TELEMETRY_COUNT == po_telemetry_idx)
		{
			perturb_and_observe_telemetry_dump();
//			while(1);
			po_telemetry_idx = 0;
		}
	}
#endif
//	R_TAU0_Channel3_Start();



	while (1U)
    {
        if (rx_done)
		{
			if (r_serial_checksum(rx_buffer, rx_buffer[LEN] + HEADER_SIZE))
			{
				if (rx_buffer[CMD] == MCUSENSE)
				{
					if (fS_SP == TRUE)
					{
						R_UART0_Send(tx_OK, HEADER_SIZE);
						fS_SP = FALSE;
					}
					else
					{
						R_UART0_Send(tx_buffer, TX_NUM);
					}
				}
				else if (rx_buffer[CMD] == ICSTART)
				{
					R_UART0_Send(tx_buffer, TX_NUM);
					r_command_process(ICSTART);
				}
				else
				{
					R_UART0_Send(tx_OK, HEADER_SIZE);
					r_command_process(rx_buffer[CMD]);
				}
			}
			else
			{
				R_UART0_Send(tx_ERROR, HEADER_SIZE);
			}
			rx_done = FALSE;
		}
		
		if (1U == TMIF02)
		{
			fM_EN = TRUE;
			R_TAU0_Channel2_Stop();
			TMIF02 = 0;
		}
		
		if (1U == TMIF01)
		{
			TMIF01 = 0;
			vin_adc = R_ADC_Sample(ADCHANNEL0);
			vin_sum += vin_adc;
			timer1_count++;
			if (timer1_count >= TIMER1_NUM) /* 900us */
			{
				timer1_count = 0;
				vin_sen = vin_sum / TIMER1_NUM;
				vin_sum = 0;
				if (fM_EN == TRUE)
				{
					if (vin_sen * KVI <= VIN_LIMIT)
					{
						r_command_process(ICSTOP);
						fS_SP = TRUE;
					}
				}
			}
		}
		
		if (1U == TMIF00)
    	{
			TMIF00 = 0;    /* clear RIFG */

			vout_adc = R_ADC_Sample(ADCHANNEL1);
			iout_adc = R_ADC_Sample(ADCHANNEL3);
			iin_adc = R_ADC_Sample(ADCHANNEL2);
			vout_sum += vout_adc;
			iout_sum += iout_adc;
			iin_sum += iin_adc;
			timer0_count++;
			
			if (timer0_count >= TIMER0_NUM)	/* 1s */
			{
				timer0_count = 0;
				vout_sen = vout_sum / TIMER0_NUM;
				iout_sen = iout_sum / TIMER0_NUM;
				iin_sen = iin_sum / TIMER0_NUM;
				vout_sum = 0;
				iout_sum = 0;
				iin_sum = 0;
				temperature = R_ADC_Sample(ADCHANNEL16);
	        	tx_buffer[4] = (uint8_t)(vout_sen >> 8);
				tx_buffer[5] = (uint8_t)vout_sen;
				tx_buffer[6] = (uint8_t)(vin_sen >> 8);
				tx_buffer[7] = (uint8_t)vin_sen;
				tx_buffer[8] = (uint8_t)(iout_sen >> 8);
				tx_buffer[9] = (uint8_t)iout_sen;
				tx_buffer[10] = (uint8_t)(iin_sen >> 8);
				tx_buffer[11] = (uint8_t)iin_sen;
				tx_buffer[12] = (uint8_t)(temperature >> 8);
				tx_buffer[13] = (uint8_t)temperature;
				tx_buffer[14] = PG_PORT;
				tx_buffer[SUM] = r_serial_getsum(tx_buffer, TX_NUM);
			}
    	}
    }
    /* End user code. Do not edit comment generated here */
}

/***********************************************************************************************************************
* Function Name: R_MAIN_UserInit
* Description  : This function adds user code before implementing main function.
* Arguments    : None
* Return Value : None
***********************************************************************************************************************/
void R_MAIN_UserInit(void)
{
    /* Start user code. Do not edit comment generated here */
    EI();
	tx_buffer[HEAD] = 0x55;
	tx_buffer[CMD] = 0x10;
	tx_buffer[LEN] = 0x0B;
    /* End user code. Do not edit comment generated here */
}

/* Start user code for adding. Do not edit comment generated here */
void command_icstart(uint32_t vout_duty, uint32_t iout_duty, uint32_t vin_duty, uint32_t iin_duty, uint32_t PWM_period)
{
	R_TMR_RD0_Stop();
	PM_II = INPUT;
	PM_VI = INPUT;
	PM_IO = INPUT;
	PM_VO = INPUT;

	PWM_PERIOD0 = PWM_period;
	PWM_PERIOD1 = PWM_period;

	II_PWM = iin_duty * (PWM_period + 1) / 100000.0;
	VI_PWM = vin_duty * (PWM_period + 1) / 100000.0;
	VO_PWM = vout_duty * (PWM_period + 1) / 100000.0;
	IO_PWM = iout_duty * (PWM_period + 1) / 100000.0;

	PM_II = OUTPUT;
	PM_VI = OUTPUT;
	PM_IO = OUTPUT;
	PM_VO = OUTPUT;
	R_TMR_RD0_Start();

}

void command_en(void)
{
	EN_PORT = PORT_HIGH;
	TMIF02 = 0;
	R_TAU0_Channel2_Start();
}

void command_icstop(void)
{
	EN_PORT = PORT_LOW;
	fM_EN = FALSE;
	R_TMR_RD0_Stop();
	PM_II = INPUT;
	PM_VI = INPUT;
	PM_IO = INPUT;
	PM_VO = INPUT;
}

void command_voutctrl(uint32_t vout_duty)
{
	vout_pwm = vout_duty * (PWM_period + 1) / 100000.0;
	if (vout_pwm > 171 && vout_pwm < 330)
		VO_PWM = vout_pwm;
}

void command_vinctrl(uint32_t vin_duty)
{
	vin_pwm = vin_duty * (PWM_period + 1) / 100000.0;
	VI_PWM = vin_pwm;
}

void command_iinctrl(uint32_t iin_duty)
{
	iin_pwm = iin_duty * (PWM_period + 1) / 100000.0;
	II_PWM = iin_pwm;
}

uint16_t g_command;
void r_command_process(uint8_t command)
{
	g_command = command;
	switch(command)
	{
		case ICSTART:
			vout_duty = rx_buffer[4];
			vout_duty = vout_duty * 100 + rx_buffer[5];
			vout_duty = vout_duty * 10 + rx_buffer[6];
			iout_duty = rx_buffer[7];
			iout_duty = iout_duty * 100 + rx_buffer[8];
			iout_duty = iout_duty * 10 + rx_buffer[9];
			vin_duty = rx_buffer[10];
			vin_duty = vin_duty * 100 + rx_buffer[11];
			vin_duty = vin_duty * 10 + rx_buffer[12];
			iin_duty = rx_buffer[13];
			iin_duty = iin_duty * 100 + rx_buffer[14];
			iin_duty = iin_duty * 10 + rx_buffer[15];
			
			PWM_period = rx_buffer[16];
			PWM_period = PWM_period * 100 + rx_buffer[17];
			PWM_period = PWM_period * 10 + rx_buffer[18];
			
			command_icstart(vout_duty, iout_duty, vin_duty, iin_duty, PWM_period);
			break;
		case EN:
			command_en();
			break;
		case ICSTOP:
			command_icstop();
			break;
		case VOUTCTRL:
			vout_duty = rx_buffer[4];
			vout_duty = vout_duty * 100 + rx_buffer[5];
			vout_duty = vout_duty * 10 + rx_buffer[6];
			command_voutctrl(vout_duty);
			break;
		case IOUTCTRL:
			iout_duty = rx_buffer[4];
			iout_duty = iout_duty * 100 + rx_buffer[5];
			iout_duty = iout_duty * 10 + rx_buffer[6];
			IO_PWM = iout_duty * (PWM_period + 1) / 100000;
			break;
		case VINCTRL:
			vin_duty = rx_buffer[4];
			vin_duty = vin_duty * 100 + rx_buffer[5];
			vin_duty = vin_duty * 10 + rx_buffer[6];
			command_vinctrl(vin_duty);
			break;
		case IINCTRL:
			iin_duty = rx_buffer[4];
			iin_duty = iin_duty * 100 + rx_buffer[5];
			iin_duty = iin_duty * 10 + rx_buffer[6];
			command_iinctrl(iin_duty);
			break;
		case DITHEREN:
			DITHER_PORT = PORT_HIGH;
			break;
		case DITHERDIS:
			DITHER_PORT = PORT_LOW;
			break;
		default:
			break;
	}
}
/* End user code. Do not edit comment generated here */
