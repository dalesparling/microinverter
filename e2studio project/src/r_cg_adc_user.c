/***********************************************************************************************************************
* DISCLAIMER
* This software is supplied by Renesas Electronics Corporation and is only intended for use with Renesas products.
* No other uses are authorized. This software is owned by Renesas Electronics Corporation and is protected under all
* applicable laws, including copyright laws. 
* THIS SOFTWARE IS PROVIDED "AS IS" AND RENESAS MAKES NO WARRANTIES REGARDING THIS SOFTWARE, WHETHER EXPRESS, IMPLIED
* OR STATUTORY, INCLUDING BUT NOT LIMITED TO WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
* NON-INFRINGEMENT.  ALL SUCH WARRANTIES ARE EXPRESSLY DISCLAIMED.TO THE MAXIMUM EXTENT PERMITTED NOT PROHIBITED BY
* LAW, NEITHER RENESAS ELECTRONICS CORPORATION NOR ANY OF ITS AFFILIATED COMPANIES SHALL BE LIABLE FOR ANY DIRECT,
* INDIRECT, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES FOR ANY REASON RELATED TO THIS SOFTWARE, EVEN IF RENESAS OR
* ITS AFFILIATES HAVE BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
* Renesas reserves the right, without notice, to make changes to this software and to discontinue the availability 
* of this software. By using this software, you agree to the additional terms and conditions found by accessing the 
* following link:
* http://www.renesas.com/disclaimer
*
* Copyright (C) 2011, 2018 Renesas Electronics Corporation. All rights reserved.
***********************************************************************************************************************/

/***********************************************************************************************************************
* File Name    : r_cg_adc_user.c
* Version      : CodeGenerator for RL78/G14 V2.05.02.05 [30 May 2018]
* Device(s)    : R5F104AG
* Tool-Chain   : CCRL
* Description  : This file implements device driver for ADC module.
* Creation Date: 12/14/2018
***********************************************************************************************************************/

/***********************************************************************************************************************
Includes
***********************************************************************************************************************/
#include "r_cg_macrodriver.h"
#include "r_cg_adc.h"
/* Start user code for include. Do not edit comment generated here */
#include "math.h"
/* End user code. Do not edit comment generated here */
#include "r_cg_userdefine.h"

/***********************************************************************************************************************
Pragma directive
***********************************************************************************************************************/
/* Start user code for pragma. Do not edit comment generated here */
/* End user code. Do not edit comment generated here */

/***********************************************************************************************************************
Global variables and functions
***********************************************************************************************************************/
/* Start user code for global. Do not edit comment generated here */

uint16_t ADC_SAMPLE_TIMES = 5;


//B value=3380, R25=10000, seriesR=10000, resolution=10-bit					
//Vref_AD=VCC_Sensor					
const int temp_ntc[128] =					
{					
	//0  1    2    3    4    5    6    7    8    9				
	247, 247, 196, 171, 154,142, 133, 125, 118, 113,	//0			
	108, 103, 99,  96,  93,  90,  87,  84,  82,  79,	//1			
	77,  75,  73,  71,  69,  67,  66,  64,  63,  61,	//2			
	60,  58,  57,  56,  54,  53,  52,  51,  49,  48,	//3			
	47,  46,  45,  44,  43,  42,  41,  40,  39,  38,	//4			
	37,  36,  35,  34,  33,  33,  32,  31,  30,  29,	//5			
	28,  27,  27,  26,  25,  24,  23,  23,  22,  21,	//6			
	20,  19,  18,  18,  17,  16,  15,  14,  14,  13,	//7			
	12,  11,  10,  10,  9,   8,   7,   6,   6,   5,		//8		
	4,   3,   2,   1,   0,   0,   -1,  -2,  -3,  -4,	//9			
	-5,  -6,  -7,  -8,  -9,  -10, -11, -13, -14, -15,	//10			
	-16, -17, -19, -20, -22, -23, -25, -27, -28, -31,	//11			
	-33, -35, -38, -41, -45, -49, -56, -66				//12
};					

/* End user code. Do not edit comment generated here */

/* Start user code for adding. Do not edit comment generated here */
uint16_t adc_result[20];
uint16_t R_ADC_Sample(ad_channel_t channel)
{
//	uint16_t adc_result[ADC_SAMPLE_TIMES];
	uint16_t i, j, min, tmp, sum, average;
	static uint32_t result;
	//static uint32_t voltage_value;
	//static double sensor_Rt;
	
	ADS = (uint8_t)channel;
	
	/* Sampling 5 times */
	for (i = 0; i < ADC_SAMPLE_TIMES; i++)
	{
		R_ADC_Start();
		while (ADIF == 0);
		ADIF = 0;
		R_ADC_Get_Result(&adc_result[i]);
	}
	/* Sorting the array */
	for (i = 0; i < ADC_SAMPLE_TIMES; i++)
	{
		min = adc_result[i];
		for (j = i; j < ADC_SAMPLE_TIMES; j++)
		{
			if (adc_result[i] < min)
			{
				tmp = min;
				min = adc_result[j];
				adc_result[j] = tmp;
			}
		}
		adc_result[i] = min;
	}
	/* Get average value */
	for (i = 0; i < (ADC_SAMPLE_TIMES - 2); i++)
	{
		sum += adc_result[i+1];
	}
	average = sum / (ADC_SAMPLE_TIMES - 2);

	//if (average > 1)	/* filters the useless data */
	//{
		switch(channel)
		{
			case ADINTERREFVOLT:
				result = (uint32_t)145 * 1023 / average / 100;
				break;
			case ADCHANNEL16:
				//voltage_value = (uint32_t)average * VCC / 1023;
				//sensor_Rt = voltage_value * SERIES_R / (VCC - voltage_value);
				//result = (uint8_t)(1.0/(((double)log(sensor_Rt) - (double)log(10000))/3380.0 + 1.0/298) - 273);
				result = temp_ntc[average >> 3];
				break;
			default:
				result = (uint32_t)average * VCC / 1023; /* mV */
				break;
		}
	//}
	
	return (uint16_t)result;
}
/* End user code. Do not edit comment generated here */
